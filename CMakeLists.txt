cmake_minimum_required(VERSION 3.6)
project(cyberpunk)

set(CMAKE_CXX_STANDARD 14)

if (UNIX)
    # Add -O1 to -O3 flag for optimaziation
    set(CMAKE_CXX_FLAGS "-fno-exceptions -Wall")
endif ()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/../cyberpunk)

file(GLOB SOURCE_FILES
        "src/*.hpp"
        "src/*/*.hpp"
        "src/*.cpp"
        "src/*/*.cpp"
        "src/tinyc2.h"
        )

add_executable(cyberpunk ${SOURCE_FILES} src/CSkillImplosionCircle.cpp src/CSkillImplosionCircle.hpp src/CFlatCircle.cpp src/CFlatCircle.hpp src/CHealthGem.cpp src/CHealthGem.hpp src/CSkillSourceHealthGems.cpp src/CSkillSourceHealthGems.hpp src/CGui.cpp src/CGui.hpp src/CIconTextBox.cpp src/CIconTextBox.hpp src/CGuiTextElement.cpp src/CGuiTextElement.hpp src/CSkillPowerBeacon.cpp src/CSkillPowerBeacon.cpp src/CSkillPowerBeacon.hpp src/CPowerBeacon.cpp src/CPowerBeacon.hpp src/CSkillHackSignal.cpp src/CSkillHackSignal.hpp src/CProjectileHackSignal.cpp src/CProjectileHackSignal.hpp src/CSkillSniperStance.cpp src/CSkillSniperStance.hpp src/CSkillCreateMapBlock.cpp src/CSkillCreateMapBlock.hpp src/CHoloMapBlock.cpp src/CHoloMapBlock.hpp src/CDifficultyManager.cpp src/CDifficultyManager.hpp)

# Detect and add SFML
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake-build-debug" ${CMAKE_MODULE_PATH})
#Find any version 2.X of SFML
#See the FindSFML.cmake file for additional details and instructions
find_package(SFML 2 REQUIRED system window graphics network audio)
if(SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(cyberpunk ${SFML_LIBRARIES})
endif()
