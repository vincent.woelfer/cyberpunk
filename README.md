# Cyberpunk

Cyberpunk is an isometric top down shooter where you have to defeat all enemies in the current level to advance. 

## Installation

- 1 Verify that you are on the *release-branch*.
- 2 Download the repository by clicking the download button on the right hand side.
- 3 Unpack the downloaded archive.
- 4 Start cyberpunk.exe.

The archive should contain all necessary files. Currently a precompiled version is only available for Windows 64bit.

## Controls
- Movement: WASD
- Dodging: Shift
- Shooting: Left mouse button
- Reloading: R (reloads automatically if necessary)
- Drawing: Hold right mouse button
- Cast Spell: Space

While you are able to fire at the enemies you greatest weapon are a set of spells. You can cast these spells by drawing their *symbol* using the right mouse button.
After casting a symbol it will appear in your skill queue in the top right hand corner. The skill queue can hold up to two skills which can then be cast using space.
Each skill consumes energy (yellow bar) and can not be cast if you dont have enough energy.

## Skills
### Push
Casts a moving wall in the direction of your mouse cursor which pushes away enemies and blocks projectiles.

![Push](IconHLINE.png)

### Teleport
Teleports you towards your mouse cursor.

![Teleport](IconWEDGE_UP.png)

### Alpha Blast
Fires three homing missiles at the mouse cursor which lock on to nearby enemies.
Applies a flash mark for each missile.

![Alpha](IconALPHA.png)

### Chain Flash
Damages and stuns nearby enemies which have a flash mark attached to them. These flash marks are applied by shooting them or casting alpha blast.

![Flash](IconFLASH.png)

