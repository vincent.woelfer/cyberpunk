uniform sampler2D texture;
uniform float frameTime;

void main() {
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
    
    if(pixel.a > 0.8) {
        //pixel.a -= 0.2;
    }
    
    if(pixel.r + pixel.g + pixel.b <= 0.4) {
        pixel.a = 0;
        gl_FragColor = pixel;
        return;
    }

    pixel.a -= 2 * frameTime;
    
    gl_FragColor = pixel;
}


/*void main() {
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
    
    pixel = pixel * vec4(1, 1, 1, 0.997);
    
    if(pixel.a < 0.1) {
        gl_FragColor = vec4(0, 0, 0, 0);
        return;
    } else {
        gl_FragColor = pixel;
    }
}*/


/*void main() {
float blur_radius = 0.001;
    vec2 offx = vec2(blur_radius, 0.0);
    vec2 offy = vec2(0.0, blur_radius);

    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy)               * 4.0 +
                 texture2D(texture, gl_TexCoord[0].xy - offx)        * 2.0 +
                 texture2D(texture, gl_TexCoord[0].xy + offx)        * 2.0 +
                 texture2D(texture, gl_TexCoord[0].xy - offy)        * 2.0 +
                 texture2D(texture, gl_TexCoord[0].xy + offy)        * 2.0 +
                 texture2D(texture, gl_TexCoord[0].xy - offx - offy) * 1.0 +
                 texture2D(texture, gl_TexCoord[0].xy - offx + offy) * 1.0 +
                 texture2D(texture, gl_TexCoord[0].xy + offx - offy) * 1.0 +
                 texture2D(texture, gl_TexCoord[0].xy + offx + offy) * 1.0;

    gl_FragColor =  gl_Color * (pixel / 16.0);
}*/