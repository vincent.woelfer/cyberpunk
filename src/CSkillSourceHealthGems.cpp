#include "CSkillSourceHealthGems.hpp"
#include "CClassShotgunner.hpp"
#include "CSceneManager.hpp"
#include "CLineEffect.hpp"

void CSkillSourceHealthGems::onButtonPress() {
	CClassShotgunner *gunner = dynamic_cast<CClassShotgunner*>(owner->getCharacterClass());


	for (CHealthGem *h : gunner->getHealthGemList()) {
		if (CSceneManager::doesObjectExist(h)) {
			connectedHealthGems.push_back(h);
			h->setAsBeingDrained();
			CLineEffect *line = new CLineEffect(owner, h, gunner->getAccentColor());
			line->setLifetime(duration);
			CSceneManager::safeAddObject(line);

		}
	}
	gunner->clearHealthGemList();

	//prevent useless application
	if(connectedHealthGems.size() == 0) {
		owner->restoreEnergy(energyConsumption);
		return;
	}

	//begin draining
	isDrainingHealthGems = true;
	clockDraining.restart();
}

void CSkillSourceHealthGems::tick(float frameTime) {
	if(isDrainingHealthGems) {
		if(clockDraining.getElapsedTime().asMilliseconds() < duration) {
			float healingPerSecond = connectedHealthGems.size() * connectedHealthGems[0]->getHealthBonus() / (duration /1000);
			owner->healHealth(healingPerSecond * frameTime);
		} else {
			isDrainingHealthGems = false;
			for(CHealthGem *h : connectedHealthGems) {
				CSceneManager::safeRemoveObject(h);
			}
			connectedHealthGems.clear();
		}
	}
}
