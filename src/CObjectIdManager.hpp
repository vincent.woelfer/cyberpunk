#ifndef CYBERPUNK_COBJECTIDMANAGER_HPP
#define CYBERPUNK_COBJECTIDMANAGER_HPP

#include <SFML/Config.hpp>

typedef sf::Uint32 CObjectId;

class CObjectIdManager {
	CObjectIdManager() = delete;
	
private:
	static CObjectId objectIdGenerator;
	
public:
	static void init(CObjectId initialId);
	static CObjectId getUniqueId();

};


#endif //CYBERPUNK_COBJECTIDMANAGER_HPP
