#include <iostream>
#include "CMapFloorBlock.hpp"

CMapFloorBlock::CMapFloorBlock(const Vector3f pos, const Vector3f dim, const Color colorBase, Color colorAccent) :
		CMapBlock(pos, dim, colorBase, colorAccent) {
	
	lineCountX = ((int)(dim.x / 0.5f) + 1);
	lineCountY = ((int)(dim.y / 0.5f) + 1);
	lines = VertexArray(sf::Lines, (size_t) (lineCountX + lineCountY) * 2);
	
	for(unsigned int i = 0; i < (unsigned int) (lineCountX + lineCountY) * 2; i++) {
		lines[i].color = colorAccent;
	}
	
	update();
}

CMapFloorBlock::CMapFloorBlock(CRectangleArea area, float posZ, float dimZ, Color colorBase, Color colorAccent) :
		CMapBlock(Vector3f(area.offset.x + area.size.x/2.f - 0.5f, area.offset.y + area.size.y/2.f - 0.5f, posZ),
		          Vector3f(area.size.x/2.f, area.size.y/2.f, dimZ),
		          colorBase,
		          colorAccent) {

	lineCountX = ((int)( dim.x / 0.5f) + 1);
	lineCountY = ((int)(dim.y / 0.5f) + 1);
	lines = VertexArray(sf::Lines, (size_t) (lineCountX + lineCountY) * 2);

	for(unsigned int i = 0; i < (unsigned int) (lineCountX + lineCountY) * 2; i++) {
		lines[i].color = colorAccent;
	}

	update();
}

void CMapFloorBlock::draw(RenderTarget* target) {
	CObject::draw(target);
	target->draw(lines);
}

void CMapFloorBlock::update() {
	unsigned int lineIndex = 0;
	
	// Along X-Axis
	float x = pos.x - graphicBox.getDimension().x;
	float y1 = pos.y + graphicBox.getDimension().y;
	float y2 = pos.y - graphicBox.getDimension().y;
	
	float z = pos.z + graphicBox.getDimension().z;
	
	for(int step = 0; step < lineCountX; step++) {
		lines[lineIndex    ].position = CEngine::toIso(Vector3f(x, y1, z));
		lines[lineIndex + 1].position = CEngine::toIso(Vector3f(x, y2, z));
		
		x++;
		
		lineIndex += 2;
	}

	// Along Y-Axis
	float x1 = pos.x + graphicBox.getDimension().x;
	float x2 = pos.x - graphicBox.getDimension().x;
	float y = pos.y - graphicBox.getDimension().y;
	
	for(int step = 0; step < lineCountY; step++) {
		lines[lineIndex    ].position = CEngine::toIso(Vector3f(x1, y, z));
		lines[lineIndex + 1].position = CEngine::toIso(Vector3f(x2, y, z));
		
		y++;

		lineIndex += 2;
	}
}
