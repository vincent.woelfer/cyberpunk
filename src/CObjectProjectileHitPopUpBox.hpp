#ifndef CYBERPUNK_CEXPLOSION_HPP
#define CYBERPUNK_CEXPLOSION_HPP

#include "CObject.hpp"
#include "CGraphicBox.hpp"
#include "CBoundingBox.hpp"

using namespace sf;

class CObjectProjectileHitPopUpBox : public CObject {
private:
	CGraphicBox graphicBox;
	Clock clock;
	float lifetime;

public:
	CObjectProjectileHitPopUpBox(Vector3f pos, Vector3f dim, Color color, float lifetime) :
			CObject(pos),
			graphicBox(pos, dim, color, color),
	        lifetime(lifetime) {}

	CObjectProjectileHitPopUpBox(Vector3f pos, Vector3f dim, Color colorBase, Color colorAccent, float lifetime) :
			CObject(pos),
			graphicBox(pos, dim, colorBase, colorAccent),
			lifetime(lifetime) {}
	
	CObjectProjectileHitPopUpBox(Vector3f pos, Color color) :
			CObject(pos),
			graphicBox(pos, Vector3f(0.1f,0.1f,0.1f), color, color),
			lifetime(50.f) {}


	void tick(float frameTime);

	CCollisionContainer* getCollisionContainer();
	CGraphicContainer* getGraphicContainer();
};

#endif //CYBERPUNK_CEXPLOSION_HPP
