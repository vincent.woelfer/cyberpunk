#ifndef CYBERPUNK_CPROJECTILEALPHABLAST_HPP
#define CYBERPUNK_CPROJECTILEALPHABLAST_HPP


#include "CProjectile.hpp"

class CProjectileAlphaBlast : public CProjectile {
private:
	CObject* target;
	Vector3f targetPos;
	Vector3f startPos;
	
	bool hasPassedApex = false;
	
	float damage = 16;
	
	float apexZ = 2.8f;
	float apexDistance = 0.25f;
	float preApexSpeed = 0.35f;
	CClock apexClock;
	
	float postApexBaseSpeed = 8.f;
	float postApexSpeedIncrease = 20;
	
	float targetLostMaxDistance = 10.f;
	Vector2f lastTargetPosition;

public:
	CProjectileAlphaBlast(CCharacter* initiator, const Vector3f pos, Vector3f targetPos) :
			CProjectile(initiator, pos, Vector3f(0.15,0.15,0.15), COLOR_PINK_ALPHA, CEngine::normalize(CEngine::to2D(targetPos - pos))),
			target(nullptr),
			targetPos(targetPos),
			startPos(pos),
	        lastTargetPosition(CEngine::to2D(targetPos)) {
		
		if(CEngine::getDistance(CEngine::to2D(startPos), CEngine::to2D(targetPos)) < apexDistance * 2.f) {
			apexDistance = CEngine::getDistance(CEngine::to2D(startPos), CEngine::to2D(targetPos)) * 0.5f;
		}
	}

	CProjectileAlphaBlast(CCharacter* initiator, const Vector3f pos, CObject* target) :
			CProjectile(initiator, pos, Vector3f(0.15,0.15,0.15), COLOR_PINK_ALPHA, CEngine::normalize(CEngine::to2D(target->getPosition() - pos))),
			target(target),
			targetPos(target->getPosition()),
			startPos(pos),
			lastTargetPosition(CEngine::to2D(target->getPosition())) {
		
		if(CEngine::getDistance(CEngine::to2D(startPos), CEngine::to2D(targetPos)) < apexDistance * 2.f) {
			apexDistance = CEngine::getDistance(CEngine::to2D(startPos), CEngine::to2D(targetPos)) * 0.5f;
		}
	}

	virtual void tick(float frameTime);

	virtual void onHitEnemy(CEnemy* target, float frameTime);
	virtual void onHitWall(float frameTime);

private:
	void updatePosition();
	
	void checkTargetLost(float frameTime);
};


#endif //CYBERPUNK_CPROJECTILEALPHABLAST_HPP
