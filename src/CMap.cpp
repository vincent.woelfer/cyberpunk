#include "CMap.hpp"
#include "CMapRoomGeneratorDigOut.hpp"
#include "CNavMesh.hpp"
#include "CMapRoomGeneratorStartRoom.hpp"
	
void CMap::initMap() {
	mapRoomGenerator = new CMapRoomGeneratorStartRoom();
	
	// Init first room
	rooms.push_back(mapRoomGenerator->generateRoom(CRectangleArea(Vector2f(0, 0), Vector2f(14, 14)), Doorway{}, CDirection::PX, 4));

	lastRequestedRoom = rooms.back();
	
	int roomsToGenerate = 10;
	while(roomsToGenerate-- > 0) {
		if(!generateNextRoom()) {
			rooms.pop_back();
			rooms.pop_back();
			roomsToGenerate++;
		} else {
			rooms.back()->sealOfExit();
		}
	}
	
	for(CMapRoom* room : rooms) {
		room->registerToSceneManager();
	}
}

bool CMap::generateNextRoom() {
	delete mapRoomGenerator;
	mapRoomGenerator = new CMapRoomGeneratorDigOut((int) (rooms.size()));
	
	CMapRoom* previousRoom = rooms.back();
	Doorway prevExit = previousRoom->getExit();
	
	// Find entrance and corridor
	Doorway entrance;
	entrance.direction = prevExit.direction;
	
	float corridorLength = getRandomCorridorLength(previousRoom->getMinimumExitCorridorLength());
	entrance.left = prevExit.left + prevExit.direction.toVector() * corridorLength;
	entrance.right = prevExit.right + prevExit.direction.toVector() * corridorLength;
	
	// Find size and offset. New roomArea will be put in front of entrance so the entrance is on the half of the orthogonal side
	Vector2f size = getRandomRoomSize();
	Vector2f offset;
	
	if(entrance.direction == CDirection::PX) {
		offset = entrance.left;
		offset.y -= floor(size.y/2.f);
	} else if(entrance.direction == CDirection::NX) {
		offset = entrance.right;
		offset.x -= size.x - 1;
		offset.y -= floor(size.y/2.f);
	} else if(entrance.direction == CDirection::PY) {
		offset = entrance.right;
		offset.x -= floor(size.x/2.f);
	} else if(entrance.direction == CDirection::NY) {
		offset = entrance.right;
		offset.x -= floor(size.x/2.f);
		offset.y -= size.y - 1;
	}
	
	// Find exit
	CRectangleArea roomArea(offset, size);
	vector<CDirection> possibleExitDirections = getPossibleExitDirections(roomArea, entrance.direction);
	
	if(possibleExitDirections.empty()) {
		cout << "No direction possible!" << endl;
		return false;
	}

	CDirection exitDirection = possibleExitDirections.at((unsigned long) CRandom::getInRangeI(0, (int) (possibleExitDirections.size() - 1)));
	
	// Generate room
	rooms.push_back(new CMapCorridor(entrance, prevExit, (int) (rooms.size())));
	rooms.push_back(mapRoomGenerator->generateRoom(CRectangleArea(offset, size), entrance, exitDirection, getRandomDoorwaySize()));
	
	return true;
}

bool CMap::isAreaFree(CRectangleArea roomArea) {
	for(CMapRoom* otherRoom : rooms) {
		CRectangleArea otherRoomArea = otherRoom->getRoomArea();

		if(roomArea.doesIntersect(otherRoomArea)) {
			return false;
		}
	}
	
	return true;
}

vector<CDirection> CMap::getPossibleExitDirections(CRectangleArea room, CDirection entranceDirection) {
	vector<CDirection> possibleExits;
	vector<CDirection> possibleDirections;
	possibleDirections.push_back(CDirection(CDirection::PX));
	possibleDirections.push_back(CDirection(CDirection::NX));
	possibleDirections.push_back(CDirection(CDirection::PY));
	possibleDirections.push_back(CDirection(CDirection::NY));

	possibleDirections.erase(std::remove(possibleDirections.begin(), possibleDirections.end(), entranceDirection.getOpposite()), possibleDirections.end());
	
	for(CDirection direction : possibleDirections) {
		CRectangleArea nextRoomPotentialArea;
		
		if(direction == CDirection::PX || direction == CDirection::NX) {
			nextRoomPotentialArea.size = Vector2f(maxRoomSize + maxCorridorLength, room.size.y + maxRoomSize);

			if(direction == CDirection::PX) {
				nextRoomPotentialArea.offset = Vector2f(room.offset.x + room.size.x + 1, room.offset.y - maxRoomSize/2.f);
			} else {
				nextRoomPotentialArea.offset = Vector2f(room.offset.x - nextRoomPotentialArea.size.x - 1, room.offset.y - maxRoomSize/2.f);
			}
		} else {
			nextRoomPotentialArea.size = Vector2f(room.size.x + maxRoomSize, maxRoomSize + maxCorridorLength);

			if(direction == CDirection::PY) {
				nextRoomPotentialArea.offset = Vector2f(room.offset.x - maxRoomSize/2.f, room.offset.y + room.size.y + 1);
			} else {
				nextRoomPotentialArea.offset = Vector2f(room.offset.x - maxRoomSize/2.f, room.offset.y - nextRoomPotentialArea.size.y - 1);
			}
		}
		
		if(isAreaFree(nextRoomPotentialArea)) {
			possibleExits.push_back(direction);
		}
	}
	
	return possibleExits;
}

int CMap::getRandomCorridorLength(int minimumCorridorLength) {
	return CRandom::getInRangeI(max(minCorridorLength, minimumCorridorLength), max(maxCorridorLength, minimumCorridorLength), rollsCorridorLength);
}

Vector2f CMap::getRandomRoomSize() {
	return Vector2f(CRandom::getInRangeI(minRoomSize, maxRoomSize, rollsRoomSize), CRandom::getInRangeI(minRoomSize, maxRoomSize, rollsRoomSize));
}

int CMap::getRandomDoorwaySize() {
	return CRandom::getInRangeI(minDoorwaySize, maxDoorwaySize, rollsDoorwaySize);
}

const vector<CMapBlock*>* CMap::getWallsForRoom(Vector2f pos) {
	CMapRoom* room = getRoomAt(pos);

	if(room != nullptr) {
		return room->getWallBlocks();
	}

	// Return non null vector
	return rooms.back()->getWallBlocks();
}

const vector<CMapBlock*>* CMap::getWallsForRoom(Vector3f pos) {
	return getWallsForRoom(CEngine::to2D(pos));
}

CMapRoom* CMap::getRoomAt(Vector2f pos) const {
	if(lastRequestedRoom->getRoomArea().isPointInside(pos)) {
		return lastRequestedRoom;
	}

	for(CMapRoom* room : rooms) {
		CRectangleArea roomArea = room->getRoomArea();

		if(roomArea.isPointInside(pos)) {
			lastRequestedRoom = room;
			return room;
		}
	}

	cout << "Error: No room found for point " << pos.x << "/" << pos.y << endl;
	return nullptr;
}

CMapRoom* CMap::getRoomAt(Vector3f pos) const {
	return getRoomAt(CEngine::to2D(pos));
}

CMapRoom* CMap::getRoom(int roomNumber) const {
	return rooms.at(static_cast<unsigned long>(roomNumber * 2)); // To get only rooms, not corridors
}

void CMap::addDebugVisuals() {
	for(CMapRoom* room : rooms) {
		room->getNavMesh()->addDebugVisuals();
	}
}

void CMap::removeDebugVisuals() {
	for(CMapRoom* room : rooms) {
		room->getNavMesh()->removeDebugVisuals();
	}
}

CNavPath* CMap::findPath(Vector2f start, Vector2f target, float agentHalfWidth) {
	CNavPath* path;
	CMapRoom* startRoom = getRoomAt(start);
	CMapRoom* targetRoom = getRoomAt(target);

	if(startRoom == nullptr || targetRoom == nullptr) {
		cout << "No path found: start or target not within rooms" << endl;
		return new CNavPath(nullptr);
	}

	if(startRoom == targetRoom) {
		path = startRoom->getNavMesh()->findPath(start, target, agentHalfWidth);		
	} else {
		cout << "Path between rooms not implemented yet!" << endl;
		return new CNavPath(nullptr);
	}	
	
	return path;
}

CNavMesh* CMap::getNavMeshForPosition(Vector2f position) const {
	CMapRoom* room = getRoomAt(position);

	if(room != nullptr) {
		return room->getNavMesh();
	}

	return nullptr;
}

std::vector<Vector2f> CMap::getStartingPositions(int playerCount) const {
	std::vector<Vector2f> positions;

	Vector2f center = rooms.front()->getRoomArea().getCenter();
	// TODO remove when area.getCenter is fixed
	center -= Vector2f(0.5f, 0.5f);
	CDirection exitDirection = rooms.front()->getExit().direction;

	if(playerCount == 1) {
		positions.push_back(center);
	} else if(playerCount == 2) {
		positions.push_back(center + exitDirection.getLeft().toVector());
		positions.push_back(center + exitDirection.getRight().toVector());
	} else if(playerCount == 3) {
		positions.push_back(center);
		positions.push_back(center + exitDirection.getLeft().toVector() * 1.5f);
		positions.push_back(center + exitDirection.getRight().toVector() * 1.5f);
	} else {
		positions.push_back(center + Vector2f(-1, -1));		
		positions.push_back(center + Vector2f(-1,  1));		
		positions.push_back(center + Vector2f( 1, -1));		
		positions.push_back(center + Vector2f( 1,  1));		
	}

	return positions;
}

// TODO implement
/*vector<Vector2f> CMap::getFreeFieldsForRoom(Vector2f objectPosition) {
	
}

vector<Vector2f> CMap::getFreeFieldsForRoom(Vector3f objectPosition) {
	return getFreeFieldsForRoom(CEngine::to2D(objectPosition));
}*/
