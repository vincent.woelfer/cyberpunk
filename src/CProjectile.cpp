#include "CProjectile.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"

CProjectile::CProjectile(CCharacter* initiator, Vector3f pos, Vector3f dim, Color color, Vector2f dir) :
		CObject(pos),
		initiator(initiator),
		boundingBox(pos, dim),
		graphicBox(pos, dim, color),
		dir(CEngine::normalize(dir)),
		speed(1) {

	setRotation(CEngine::getRotation(dir));
}

CProjectile::CProjectile(CCharacter* initiator, Vector3f pos, Vector3f dim, Color color, Vector2f dir, float speed) :
	CObject(pos),
	initiator(initiator),
	boundingBox(pos, dim),
	graphicBox(pos, dim, color),
	dir(CEngine::normalize(dir)),
	speed(speed) {

	setRotation(CEngine::getRotation(dir));
}

void CProjectile::tick(float frameTime) {
	CObject::tick(frameTime);

	if(!isMovementReplicated) {
		move(dir * speed * frameTime);
	}

	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		if(enemy->getCollisionContainer()->doesIntersect(*this->getCollisionContainer())) {
			onHitEnemy(enemy, frameTime);
		}
	}

	for(CMapBlock* mapBlock : *CSceneManager::getMap()->getWallsForRoom(getPosition())) {
		if(mapBlock->getCollisionContainer()->doesIntersect(*this->getCollisionContainer())) {
			onHitWall(frameTime);
		}
	}

	for(CPlayer* player : CSceneManager::getPlayers()) {
		if(player->getCollisionContainer()->doesIntersect(*this->getCollisionContainer())) {
			onHitPlayer(player, frameTime);
		}
	}

	for(CProjectile* projectile : CSceneManager::getProjectiles()) {
		if(projectile->getCollisionContainer()->doesIntersect(*this->getCollisionContainer())) {
			onHitProjectile(projectile, frameTime);
		}
	}

	if(clockLifetime.getElapsedTime().asMilliseconds() >= lifetime) {
		CSceneManager::safeRemoveObject(this);
	}
}

CCollisionContainer* CProjectile::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer* CProjectile::getGraphicContainer() {
	return &graphicBox;
}

Vector2f CProjectile::getDirection() {
	return dir;
}

float CProjectile::getSpeed() {
	return speed;
}

void CProjectile::setSpeed(float speed) {
	this->speed = speed;
}

void CProjectile::setDirection(Vector2f dir) {
    this->dir = dir;
}

void CProjectile::setLifetime(float lifetime) {
	this->lifetime = lifetime;

}

