
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "CEngine.hpp"
#include "CPlayer.hpp"
#include "CInputManager.hpp"
#include "CSceneManager.hpp"
#include "CMap.hpp"
#include "CRenderManager.hpp"
#include "CStatisticsManager.hpp"
#include "CGuiPopupText.hpp"
#include "CGui.hpp"
#include "CDebugHelper.hpp"
#include "CNetworkManager.hpp"
#include "CMessageObjTranslationUpdate.hpp"
#include "CMessageObjCreatePlayer.hpp"
#include "CGameClock.hpp"
#include "CEnemyGhost.hpp"
#include "CRpcHandler.hpp"
#include "CNavMesh.hpp"
#include "CLobby.hpp"
#include "CGameStateManager.hpp"
#include "CMessageEnemySpawn.hpp"

using namespace sf;
using namespace std;

int main(int argc, char* argv[]) {
	std::string version = "Cyberpunk v0.15";
	cout << version << endl;
	CDebugHelper::print(version);

	CRandom::initialize();
	CGuiManager::loadFonts();

	VideoMode desktopVideoMode = VideoMode().getDesktopMode();
	sf::Uint32 style = Style::Fullscreen;
	Vector2i windowPosition = Vector2i(0, 0);
	
	// Multiplayer
	//bool isMultiplayer = true;
	//bool isHost = true;
	bool isMultiplayer = argc > 1;
	bool isHost = false;
	std::string name = "";
	if(isMultiplayer) {
		if(*argv[1] == 'h') {
			isHost = true;
		}

		if(argc > 2) {
			desktopVideoMode.width *= 0.5f;
			desktopVideoMode.height *= 0.5f;
			style = Style::None;
			
			if(*argv[2] == 'l') {
				windowPosition = Vector2i(0, 0);
				name = "left";
			} else if(*argv[2] == 'r') {
				windowPosition = Vector2i(VideoMode().getDesktopMode().width - desktopVideoMode.width, VideoMode().getDesktopMode().height - desktopVideoMode.height);
				name = "right";
			} else if(*argv[2] == 'b') {
				windowPosition = Vector2i(0, VideoMode().getDesktopMode().height - desktopVideoMode.height);
				name = "bot";
			} else if(*argv[2] == 't') {
				windowPosition = Vector2i(VideoMode().getDesktopMode().width - desktopVideoMode.width, 0);
				name = "top";
			}			
		}
	}

	RenderWindow window(desktopVideoMode, "Cyberpunk", style);
	window.setPosition(windowPosition);
	//window.setVerticalSyncEnabled(true);
	//window.setFramerateLimit(60);

	CInputManager::setRenderWindow(&window);
	CRenderManager::initialize(desktopVideoMode);
	CDebugHelper::initDebugHelper();
	/* Window initialization complete */

	CClassName localPlayerClass;
	CDifficulty difficulty;
	if(isMultiplayer) {
		CLobby lobby(isHost, &window);
		if(name != "") {
			lobby.setName(name);
		}
		lobby.enterLobby();
		
		localPlayerClass = lobby.getClassName();
		difficulty = lobby.getDifficulty();
	} else {
		localPlayerClass = CClassName::MEDIATOR;
		difficulty = CDifficulty::MEDIUM;
	}

	// Init Game
	CMap map;
	map.initMap();
	CSceneManager::setMap(&map);
	
	/* Game initialization starting */
	//CSpawningManager* spawningManager = new CSpawningManager(difficulty, CEnemyTypeName::HOLOGRAMS);
	CTempSpawningManager* spawningManager = new CTempSpawningManager(difficulty, static_cast<int>(CNetworkManager::getConnections().size() + 1));

	CPatternRecognitionMngr* patternRecognitionMngr = new CPatternRecognitionMngr(); //TODO make static
	CInputManager::setPatternRecognitionMngr(patternRecognitionMngr);
	
	Vector2f startingPosition = map.getStartingPositions(static_cast<int>(CNetworkManager::getConnections().size() + 1)).at(static_cast<unsigned long>(CNetworkManager::getPlayerNumber()));
	CPlayer* localPlayer = new CPlayer(Vector3f(startingPosition.x, startingPosition.y, 0.5f + 0.6f + 0.01f), Vector3f(0.35f, 0.35f, 0.6f), localPlayerClass);
	
	CSceneManager::safeAddObject(localPlayer);
	CInputManager::setLocalPlayer(localPlayer);
	CSceneManager::safeAddPendingObjects();
	CGui::setupLocalPlayerGui(localPlayer);
	CNetworkManager::addReplicatingObject(localPlayer);
	CGameStateManager::init(localPlayer, spawningManager);

	CMessageObjCreatePlayer* messageObjCreatePlayer = (CMessageObjCreatePlayer*) CNetworkManager::createMessage(CMessageType::OBJ_CREATE_PLAYER);
	messageObjCreatePlayer->objectId = localPlayer->getId();
	messageObjCreatePlayer->className = localPlayerClass;
	messageObjCreatePlayer->pos = localPlayer->getPosition();
	messageObjCreatePlayer->rot = localPlayer->getRotation();
	CNetworkManager::queueForBroadcast(messageObjCreatePlayer);
	
	Text textFps;
	textFps.setFont(CGuiManager::getFontDebug());
	textFps.setCharacterSize((unsigned int) (22 * CGuiManager::getGuiFontSizeFactor()));
	textFps.setColor(Color::Red);

	// Debug TODO refactor
	bool isDebugNavigationVisualsOn = false;
	double avg = 500;
	
	//TODO put this in the right place -- make multiplayer compatible
	CDifficultyManager::setDifficulty(difficulty);
	CDifficultyManager::setNumberOfPlayers(static_cast<int>(CNetworkManager::getConnections().size() + 1));

	Clock clockFps;
	Clock clockDebug;
	while(window.isOpen()) {
		float frameTime = clockFps.restart().asSeconds();

		Event event;
		while(window.pollEvent(event)) {
			if(event.type == Event::Closed || (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)) {
				window.close();
			}
		}
		
		if(CNetworkManager::isActive()) {
			CNetworkManager::receiveMessages();
			CNetworkManager::processReceivedMessages();
			CNetworkManager::tick();
		}
		
		CGameClock::tick(frameTime);
		spawningManager->tick(frameTime);
		CGameStateManager::tick();

		if(CGameStateManager::isLocalPlayerAlive()) {
			CInputManager::handleKeyboardInput(frameTime);
			CInputManager::handleMouseInput(frameTime);
		}

		if(!CGameStateManager::isGameOver()) {
			//clockDebug.restart();
			CSceneManager::tickAll(frameTime);
			//float ttick = clockDebug.restart().asMicroseconds();

			CRenderManager::getViewScene()->setCenter(CEngine::toIso(CGameStateManager::getViewCenter()));
			CRenderManager::updateView();
			CRenderManager::clear();

			//float tstuff = clockDebug.restart().asMicroseconds();
			deque<CObject*> objectHierarchy = CSceneManager::getObjectHierarchy();
			//float thist = clockDebug.restart().asMicroseconds();

			Vector3f pos = CGameStateManager::getViewCenter();
			float viewDistance = (CEngine::tile_size >= 63) ? 550 : 850;
			for(CObject* object : objectHierarchy) {
				if(CEngine::getDistanceSquared(object->getPosition(), pos) < viewDistance) {
					object->draw(CRenderManager::getTextureScene());
				}
			}

			/*
			float tdraw = clockDebug.restart().asMicroseconds();
			float tframe = ttick + tstuff + tdraw + thist;
			if(1.f / frameTime >= 5 && 1.f / frameTime <= 2000) {
				avg -= avg / 60.f;
				avg += (1.f / frameTime) / 60.f;
			}
			textFps.setString(to_string(1.f / frameTime).substr(0, 5) + string(" | avg: ") + to_string(avg).substr(0, 5)
			                  + string(" | tick: " + to_string(ttick).substr(0, 5))
			                  + string(" | stuff: ") + to_string(tstuff).substr(0, 5)
			                  + string(" | hist: ") + to_string(thist).substr(0, 5)
			                  + string(" | draw: ") + to_string(tdraw).substr(0, 5)
			                  + string(" | frame(added): ") + to_string(tframe).substr(0, 5)
			                  + string(" | frame(complete): ") + to_string(frameTime * 1000000.f).substr(0, 5)
			                  + string("\n")
			                  + string(" [Score: ") + to_string(CStatisticsManager::getScore()) + string("]")
			                  + string(" | pattern: ") + patternRecognitionMngr->prompt
			                  + string(" | Intensity: ") + to_string(spawningManager->getIntensity()).substr(0, 4)
			                  + string(" | SpawnInt: ") + to_string(spawningManager->getSpawnInterval() /1000.f).substr(0, 3));
			View* view = CRenderManager::getViewGui();
			textFps.setPosition(view->getCenter() - view->getSize() * 0.5f);
			//CRenderManager::getTextureGui()->draw(textFps);*/
			// Debug
			/*if(CInputManager::isDebugKeyToggledOn(CInputManager::keyDebugNavigation)) {
				isDebugNavigationVisualsOn = !isDebugNavigationVisualsOn;
				if(isDebugNavigationVisualsOn) {
					map.addDebugVisuals();
				} else {
					map.removeDebugVisuals();
				}
			}*/
		}

		CDebugHelper::updateDebugContent();
		CGuiManager::tickAll(frameTime);
		CRenderManager::drawGui();

		CRenderManager::tick(frameTime);
		CRenderManager::render(&window);

		CSceneManager::safeAddPendingObjects();
		CSceneManager::safeRemovePendingObjects();
		CSceneManager::safeUnregisterPendingObjects();
	}

	CNetworkManager::closeNetworkSocket();
	return 0;
}

/*
 * Socken waschen ist wichtig
 * Baue häuser für die welterrschaft. Wir brauchen stammhalter
 * Dicke euter sind erstrebenswert
 * 
 * Ich will dass das für immer im code bleibt und ich somit in cyberpunk verewigt bin
 *  - Peter 20 cm
 *  
 *  Du ich mag marc 
 *  Veni vidi vinni
 *  Löt mir einen! 
 */
