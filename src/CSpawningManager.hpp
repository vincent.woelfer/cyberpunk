#ifndef CYBERPUNK_CSPAWNINGMANAGER_HPP
#define CYBERPUNK_CSPAWNINGMANAGER_HPP

#include <vector>
#include "CEnemyType.hpp"
#include "CSpawnPoint.hpp"
#include "CIntensityManager.hpp"
#include "CDifficultyManager.hpp"

class CSpawnPoint;

enum class CEnemyTypeName {
	HOLOGRAMS,
	CYBER_VAMPS
};

class CSpawningManager {
private:
	// General Budget Fields
	CDifficulty difficulty;
	float budget;
	float initialBudget;
	float budgetIncreasePerRoom; //absolut
	int numberOfWaves;
	float waveDivisor; //the minimum budget required to justify another wave
	unsigned int numberActiveSpawners;

	CEnemyType* enemyType;
	
	float spawnerDeployChance = 0.8f; //per second if less active spawner

	std::vector<int> enemyPoolList;

	std::vector<vector<int>> enemyWaveTable;
	std::vector<CSpawnPoint*> activeSpawnPoints;

	CIntensityManager* intensityManager;

	CNavMesh* currentNavMesh = nullptr;
	bool isFloor = true;
	bool hasOpenedRoom = true;
	bool hasEnteredNewRoom = false;
	CClock newRoomEnteredClock;

public:
	CSpawningManager(CDifficulty difficulty, CEnemyTypeName enemyTypeName);

	//Running obsolete
	~CSpawningManager();

	virtual void tick(float frameTime);
	float getSpawnInterval();
	float getIntensity();
	CEnemyType* getEnemyType();
	
private:
	void deployNewSpawner();
	void createSpawnList(std::vector<float> enemyTypeDistribution);
	void acquireEnemyInstance(CEnemyType::ENEMY_INSTANCE enemy_instance_type);
	void resetEnemyPoolList(int size);
};


#endif //CYBERPUNK_CSPAWNINGMANAGER_HPP
