#include "CProjectileSpecterPull.hpp"
#include "CProjectileSweepingWall.hpp"
#include "CEffectSpecterPull.hpp"

CProjectileSpecterPull::CProjectileSpecterPull(CCharacter* initiator, const Vector3f pos, const Vector2f dir, CEnemySpecter* owner,
                                               float projectileSpeed, float maxRange, float stunDuration,
                                               float pullDuration, float pullStrength) :
		CProjectile(initiator, pos, Vector3f(0.15f, 0.15f, 0.15f), Color(60, 60, 180, 240), dir, projectileSpeed),
		owner(owner),
		lineEffect(nullptr),
		maxRange(maxRange),
		stunDuration(stunDuration),
		pullDuration(pullDuration),
		pullStrength(pullStrength) {

	lineEffect = new CLineEffect(this, owner, Color(40, 40, 120, 230));
	lineEffect->setLifetime(10000);
	CSceneManager::safeAddObject(lineEffect);
}

void CProjectileSpecterPull::tick(float frameTime) {
	if(!CSceneManager::doesObjectExist(owner)) {
		CSceneManager::safeRemoveObject(this);
		return;
	}
	
	CProjectile::tick(frameTime);

	if(CEngine::getDistance(pos, owner->getPosition()) >= maxRange) {
		CSceneManager::safeRemoveObject(this);
	}
}

void CProjectileSpecterPull::onHitPlayer(CPlayer* player, float frameTime) {
	if(!CSceneManager::doesObjectExist(owner)) {
		CSceneManager::safeRemoveObject(this);
		return;
	}
	
	player->addEffect(new CEffectSpecterPull(owner, stunDuration, pullDuration, pullStrength));
	owner->notifyPullProjectileHit();

	CSceneManager::safeRemoveObject(this);
}

void CProjectileSpecterPull::onHitWall(float frameTime) {
	CSceneManager::safeRemoveObject(this);
}

void CProjectileSpecterPull::onHitProjectile(CProjectile* projectile, float frameTime) {
	CProjectileSweepingWall* projectileSweepingWall = dynamic_cast<CProjectileSweepingWall*>(projectile);

	if(projectileSweepingWall != nullptr) {
		CSceneManager::safeRemoveObject(this);
	}
}

CProjectileSpecterPull::~CProjectileSpecterPull() {
	if(lineEffect != nullptr) {
		CSceneManager::safeRemoveObject(lineEffect);
	}
}
