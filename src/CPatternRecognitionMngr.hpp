#ifndef CYBERPUNK_CPATTERNRECOGNITIONMNGR_HPP
#define CYBERPUNK_CPATTERNRECOGNITIONMNGR_HPP

#include "CNeuralNet.hpp"
#include "CGuiDrawing.hpp"
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;


enum SIGN {
    ALPHA, HLINE_FROMLEFT, HLINE_FROMRIGHT, VLINE_DOWN, VLINE_UP, WEDGE_UP, WEDGE_DOWN, GAMMA, CIRCLE, FLASH, EPSILON, BETA, NONE //TODO fill
};

class CPatternRecognitionMngr {
public:
    CPatternRecognitionMngr();


    SIGN determineSign(vector<Vector2f> &pointsDrawn, float *quality_score, vector<double> &netOutput); //TODO Rationalize netOutput

    int netResolution = 12; //number of vectors taken as net input to determine the sign, should correspond to the actual network layout
    string prompt = ".. waiting on input";
	vector<string> sign_names = {"alpha", "hline", "hline", "vline", "vline", "wedge up", "wedge down", "gamma", "circle", "flash", "epsilon", "beta"};
	bool debugMode = false;
	CGuiDrawing * debugDrawing;
private:


    Net * net;
    bool loadNeuralNetFromSource(string path); //TODO pre-process

    bool fetchSelectPoints(vector<Vector2f> &pointsDrawn,vector<Vector2f> &pointsSelected,vector<Vector2f> &pointDirectionVectors);
    void convertVectorToNetInput(vector<Vector2f> &pointDirectionVectors, vector<double> &netInput);
    SIGN validateSign(vector<double> &confidenceArray, float *quality_score);

	float kernelDensity(Vector2f smoothingPoint, float bandwidth, vector<Vector2f> dataPoints);
	float kernel(Vector2f u);
};


#endif //PATTERN_RECOGNITION_PATTERNRECOGNTIONMNGR_H