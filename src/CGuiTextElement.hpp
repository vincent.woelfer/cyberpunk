#ifndef CYBERPUNK_CGUITEXTELEMENT_HPP
#define CYBERPUNK_CGUITEXTELEMENT_HPP

#include <SFML/Graphics/Text.hpp>
#include "CGuiElement.hpp"
#include "CClock.hpp"
#include "CEngine.hpp"

class CGuiTextElement : public CGuiElement {
private:
	sf::Text text;
	sf::Vector2f pos;
	float lifetime;
	bool isFading = false;
	bool isLifetimeLimited = false;
	CClock clockLifetime;

	float fadeAfter = 0.8f;

public:
	CGuiTextElement(std::string text, unsigned int size, sf::Color color, sf::Vector2f pos, float lifetime = 0);

	virtual void draw(sf::RenderTarget* renderTarget);

	virtual void tick(float frameTime);

	void setColor(sf::Color color);
	void setText(std::string text);
	Vector2f getTextBounds();
	void setLifetime(float lifetime);
	void setPosition(Vector2f position);
};


#endif //CYBERPUNK_CGUITEXTELEMENT_HPP
