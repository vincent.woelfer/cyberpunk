#include "CClassShotgunner.hpp"
#include "CSkillDash.hpp"
#include "CSkillDecoy.hpp"
#include "CSkillShotGunFire.hpp"
#include "CSkillForceSweep.hpp"
#include "CSkillImplosionCircle.hpp"
#include "CSceneManager.hpp"
#include "CSkillSourceHealthGems.hpp"
#include "CSkillCreateMapBlock.hpp"


CClassShotgunner::CClassShotgunner(CPlayer *player) :
		CCharacterClass(CClassName::SHOTGUNNER) {
	
	primarySkill = new CSkillShotGunFire(player);
	movementSkill = new CSkillDash(player);
	activeSkills.push_back(new CSkillImplosionCircle(player));
	activeSkills.push_back(new CSkillSourceHealthGems(player));
	activeSkills.push_back(new CSkillCreateMapBlock(player));

	skillDisplayProfiles = {make_pair(SIGN::CIRCLE, "Implosion"), make_pair(SIGN::WEDGE_DOWN, "Source Health Gems"), make_pair(SIGN::BETA, "Berta Block")};
	healthGemList = {};


}

CSkill *CClassShotgunner::getSkill(SIGN sign) {
	if(sign == SIGN::CIRCLE) {
		return activeSkills[0];
	} else if(sign == SIGN::WEDGE_DOWN) {
		return activeSkills[1];
	} else if(sign == SIGN::BETA) {
		return activeSkills[2];
	}
	else {
		return nullptr;
	}
}

Color CClassShotgunner::getBaseColor() {
	return COLOR_DARK_RED;
}

Color CClassShotgunner::getAccentColor() {
	return COLOR_ORANGE;
}

void CClassShotgunner::spawnGem(Vector3f position) {

	//Catch overlapping and multiple gem spawns
	for (CHealthGem * h : healthGemList) {
		if(CSceneManager::doesObjectExist(h)) {
			if(CEngine::getDistance(h->getPosition(), position) < 0.25) {
				return;
			}
		}
	}
	if(CEngine::getDistance(position, recentGemSpawningPosition) < 0.25) {
		return;
	}

	//spawn health gem
	CHealthGem *healthGem = new CHealthGem(position, Vector3f(0.25, 0.25, 0.25), getAccentColor(), healthGemBonus);
	CSceneManager::safeAddObject(healthGem);
	healthGemList.push_back(healthGem);
	recentGemSpawningPosition = position;
}

vector<CHealthGem *> CClassShotgunner::getHealthGemList() {
	return healthGemList;
}

void CClassShotgunner::clearHealthGemList() {
	healthGemList.clear();
}
