#include <iostream>
#include "CCharacter.hpp"
#include "CSceneManager.hpp"
#include "CMessageObjTranslationUpdate.hpp"
#include "CNetworkManager.hpp"
#include "CGameClock.hpp"
#include "CRpcHandler.hpp"

CCharacter::CCharacter(Vector3f pos, Vector3f dim, float rot, Color color, float maxHealth, float movementSpeed)  :
		CObject(pos),
		maxHealth(maxHealth),
		health(maxHealth),
		movementSpeed(movementSpeed),
		boundingBox(pos, dim),
		graphicBox(pos, dim, color),
		effectManager(this) {
	boundingBox.getCollisionChannel()->set(CCollisionChannel::Channel::CHARACTER);
}

void CCharacter::tick(float frameTime) {
	CObject::tick(frameTime);
	
	effectManager.setPosition(getPosition());
	effectManager.tickEffects(frameTime);

	if(isLocal) {
		if(effectManager.hasForcePush()) {
			push(effectManager.getForcePush() * frameTime);
		}
	}
}

CCharacter::PossibleMovement CCharacter::getPossibleMovement(const c2Poly* p, Vector3f movement, vector<const c2Poly*> obstacles) {
	float minDistance = 0.05f;

	PossibleMovement possMovement;
	possMovement.hit = false;
	possMovement.length = 999.f;

	c2Ray ray;
	ray.d = CEngine::toC2v(CEngine::normalize(movement));
	ray.t = CEngine::getLength(movement);

	c2Raycast raycast;

	// p to obstacles
	for(int i = 0; i < p->count; i++) {
		ray.p = p->verts[i];

		for(const c2Poly* obstacle : obstacles) {
			if(p != obstacle) {
				if(c2RaytoPoly(ray, obstacle, nullptr, &raycast)) {
					if(raycast.t < possMovement.length) {
						possMovement.length = raycast.t;
						possMovement.surfaceNormal = CEngine::toSFVec(raycast.n);


					}
				}
			}
		}
	}

	if(possMovement.length <= minDistance) {
		possMovement.length = 0;
		possMovement.hit = true;
		return possMovement;
	}

	// Obstacles to p
	ray.d = CEngine::toC2v(CEngine::normalize(CEngine::rotateZ(movement, 180)));

	for(const c2Poly* wall : obstacles) {
		for(int i = 0; i < wall->count; i++) {
			ray.p = wall->verts[i];
			if(p != wall) {
				if(c2RaytoPoly(ray, p, nullptr, &raycast)) {
					if(raycast.t < possMovement.length) {
						possMovement.length = raycast.t;
						possMovement.surfaceNormal = - CEngine::toSFVec(raycast.n);
					}
				}
			}
		}
	}

	if(possMovement.length <= minDistance) {
		possMovement.length = 0;
		possMovement.hit = true;
		return possMovement;
	}

	if(possMovement.length != 999.f) {
		possMovement.hit = true;
	}

	// Don't get stuck in walls
	possMovement.length -= minDistance;

	return possMovement;
}

bool CCharacter::move(Vector2f desiredMovement, bool restrictMovement = false, bool wasPushed = false) {
	bool wasBlocked = false;

	if(effectManager.hasStun() && !wasPushed) {
		return false;
	}
	
	if(restrictMovement && CEngine::getLength(restrictedMovement) > 0) {
		// get dot product
		float dot = CEngine::getDotProduct(desiredMovement, restrictedMovement);
		desiredMovement -= restrictedMovement * dot;

		restrictedMovement = Vector2f(0, 0);
		wasBlocked = true;
	}

	float minSlidingAngle = 1.f;
	bool reduceSlidingMovement = true;

	Vector3f finalMovement;
	Vector3f movement = CEngine::toPlanar3D(desiredMovement);

	boundingBox.move(movement);

	// Get intersecting polygons
	vector<const c2Poly*> intersecting;
	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});
	for(CObject* object : CSceneManager::getIntersecting(this, collisionChannel)) {
		intersecting.push_back(object->getCollisionContainer()->getPoly());
	}

	if(intersecting.empty()) {
		finalMovement = movement;
	} else {
		wasBlocked = true;
		boundingBox.setPosition(pos);

		PossibleMovement possibleMovement = getPossibleMovement(boundingBox.getPoly(), movement, intersecting);

		if(!possibleMovement.hit) {
			finalMovement = movement;
		} else {
			// Move till impact
			finalMovement = CEngine::normalize(movement) * possibleMovement.length;
			Vector3f remainingMovement = movement - finalMovement;
			boundingBox.move(finalMovement);

			// Slide along surface
			float invNormAngle = CEngine::rotate(CEngine::getRotation(possibleMovement.surfaceNormal), 180);

			Vector2f movement2D = CEngine::to2D(movement);
			float angle = CEngine::getAngleDifference(invNormAngle, CEngine::getRotation(movement2D));
			if(abs(angle) > minSlidingAngle) {
				angle = (angle < 0 ? -1.f : +1.f) * (90 - abs(angle));

				remainingMovement = CEngine::rotateZ(remainingMovement, angle);

				if(reduceSlidingMovement) {
					remainingMovement *= cos(angle * 0.0174533f);
				}

				// Check if sliding movement is possible
				possibleMovement = getPossibleMovement(boundingBox.getPoly(), remainingMovement, intersecting);

				if(!possibleMovement.hit) {
					finalMovement += remainingMovement;
				} else {
					finalMovement += CEngine::normalize(remainingMovement) * possibleMovement.length;
				}
			}
		}
	}

	boundingBox.setPosition(pos);

	CObject::move(finalMovement);

	return !wasBlocked;
}

void CCharacter::rotate(float angle) {
	Vector3f resolution;

	boundingBox.rotate(angle);

	// Get polygons near
	Vector3f dim = boundingBox.getDimension();
	boundingBox.setDimension(Vector3f(dim.x * 2.f, dim.y * 2.f, dim.z));
	vector<const c2Poly*> intersecting;
	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});
	for(CObject* object : CSceneManager::getIntersecting(this, collisionChannel)) {
		intersecting.push_back(object->getCollisionContainer()->getPoly());
	}
	boundingBox.setDimension(dim);

	c2Manifold m;
	for(const c2Poly* p : intersecting) {
		m.count = 0;
		c2PolytoPolyManifold(boundingBox.getPoly(), nullptr, p, nullptr, &m);

		if(m.count > 0) {
			// Use larger depth
			Vector2f contactPoint;
			float depth;

			if(m.count == 2) {
				if(m.depths[0] > m.depths[1]) {
					contactPoint = CEngine::toSFVec(m.contact_points[0]);
					depth = m.depths[0];
				} else {
					contactPoint = CEngine::toSFVec(m.contact_points[1]);
					depth = m.depths[1];
				}
			} else {
				contactPoint = CEngine::toSFVec(m.contact_points[0]);
				depth = m.depths[0];
			}

			// Solve
			resolution = CEngine::toPlanar3D(CEngine::toSFVec(m.normal) * depth);

			// Check if normal points toward wall
			Vector2f wallCenter = 0.5f * (Vector2f(p->verts[0].x, p->verts[0].y) + Vector2f(p->verts[2].x, p->verts[2].y));
			if(CEngine::getDistance(wallCenter, contactPoint + CEngine::to2D(resolution)) < CEngine::getDistance(wallCenter, contactPoint)) {
				resolution = CEngine::toPlanar3D(CEngine::toSFVec(m.normal) * -depth);
			}

			boundingBox.move(CEngine::normalize(resolution) * (CEngine::getLength(resolution) + 0.05f));
		}
	}

	// Get polygons near
	intersecting.clear();
	for(CObject* object : CSceneManager::getIntersecting(this, collisionChannel)) {
		intersecting.push_back(object->getCollisionContainer()->getPoly());
	}

	if(!intersecting.empty()) {
		boundingBox.setPosition(pos);
		boundingBox.setRotation(rot);
	} else {
		float deltaRot = boundingBox.getRotation() - rot;
		Vector3f deltaPos = boundingBox.getPosition() - pos;

		boundingBox.setRotation(rot);
		boundingBox.setPosition(pos);

		CObject::move(deltaPos);
		CObject::rotate(deltaRot);
	}
}

void CCharacter::push(Vector2f desiredMovement) {
	Vector3f movement = CEngine::toPlanar3D(desiredMovement);

	isPushed = true;

	boundingBox.move(movement);

	// Get intersectingCharacters
	vector<CCharacter*> intersectingCharacters;
	CCollisionChannel collisionChannel({CCollisionChannel::Channel::CHARACTER});
	for(CObject* object : CSceneManager::getIntersecting(this, collisionChannel)) {
		CCharacter* character = dynamic_cast<CCharacter*>(object);
		if(character != nullptr) {
			intersectingCharacters.push_back(character);
		}
	}

	boundingBox.setPosition(pos);

	for(CCharacter* character : intersectingCharacters) {
		if(!character->isPushed) {
			Vector2f angleToObject = CEngine::to2D(character->getPosition() - pos);
			float angleDiff = CEngine::getAngleDifference(CEngine::getRotation(desiredMovement),
			                                              CEngine::getRotation(angleToObject));
			Vector2f pushMovement = CEngine::rotateZ(desiredMovement, angleDiff / 3.f);
	
			character->push(pushMovement);
		}
	}

	isPushed = false;
	restrictedMovement = CEngine::normalize(desiredMovement) * -1.f;

	move(desiredMovement, false, true);

	onPushed();
}

CCollisionContainer* CCharacter::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer* CCharacter::getGraphicContainer() {
	return &graphicBox;
}

float CCharacter::getMovementSpeed() const {
	return movementSpeed;
}

float CCharacter::getHealth() const {
	return health;
}

float CCharacter::getMaxHealth() const {
	return maxHealth;
}

void CCharacter::setMovementSpeed(float movementSpeed) {
	this->movementSpeed = movementSpeed;
}

void CCharacter::setHealth(float health) {
	this->health = CEngine::clamp(health, 0, maxHealth);

	if(this->health <= 0) {
		die();
	}
}

void CCharacter::reduceHealth(float damage) {
	if(dynamic_cast<CPlayer*>(this) != nullptr) {
		dynamic_cast<CPlayer*>(this)->damagedRecently += damage;
	}
	
	health = CEngine::clamp(health - damage, 0, maxHealth);

	if(health <= 0) {
		die();
	}
}

void CCharacter::healHealth(float heal) {
	health = CEngine::clamp(health + heal, 0, maxHealth);
}

void CCharacter::die() {
	effectManager.ownerDeath();	
	CSceneManager::safeRemoveObject(this);
}

void CCharacter::addEffect(CEffect* effect) {
	effectManager.addEffect(effect);
}

CEffectManager* CCharacter::getEffectManager() {
	return &effectManager;
}

void CCharacter::teleport(Vector3f targetPos) {
	float time = CGameClock::getGameTime();

	createTranslationSnapshotMessage(time);	
	setPosition(targetPos);
	createTranslationSnapshotMessage(time + 1);
}

bool CCharacter::isGoingToDie() {
	return health <= 0;
}
