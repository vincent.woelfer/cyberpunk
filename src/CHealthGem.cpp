#include "CHealthGem.hpp"
#include "CSceneManager.hpp"


CCollisionContainer *CHealthGem::getCollisionContainer() {
	return &boundingBox;
}

CGraphicContainer *CHealthGem::getGraphicContainer() {
	return &graphicBox;
}

void CHealthGem::tick(float frameTime) {


	rotate(rotationSpeed * frameTime);

	for(CPlayer* player : CSceneManager::getPlayers()) {
		if(player->getCollisionContainer()->doesIntersect(*this->getCollisionContainer())) {
			onHitPlayer(player, frameTime);
		}
	}

	if(clockLifetime.getElapsedTime().asMilliseconds() >= lifetime && !isBeingDrained) {
		CSceneManager::safeRemoveObject(this);
	}
}

void CHealthGem::onHitPlayer(CPlayer *player, float frameTime) {
	//TODO implement to multiplayer !! works though
	if(!isBeingDrained) {
		player->healHealth(healthBonus);
		CSceneManager::safeRemoveObject(this);
	}
}

float CHealthGem::getHealthBonus() {
	return healthBonus;
}

void CHealthGem::setAsBeingDrained() {
	isBeingDrained = true;
}
