#include "CGraphicArea.hpp"
#include "CGraphicBox.hpp"

CGraphicArea::CGraphicArea(Vector3f pos, float radius, unsigned int vertexCount, Color colorBase, Color colorAccent) :
		CGraphicContainer(pos, colorBase, colorAccent),
        radius(radius),
        vertexCount(vertexCount) {
	
	triangles = VertexArray(sf::TrianglesFan, vertexCount + 2);
	lines = VertexArray(sf::LinesStrip, vertexCount + 1);
	
	update();
}

void CGraphicArea::update() {
	doesNeedUpdate = false;
	
	Vector3f center = pos + posOffset;
	triangles[0] = CEngine::toIso(center);
	triangles[0].color = colorBase;
	
	float rotOffset = rot + 45; // So rectangles are aligned with the grid
	float angleStep = 360.f/(vertexCount);
	for(unsigned int i = 0; i < vertexCount; i++) {
		Vector2f vertexPos = CEngine::toIso(center + CEngine::getUnitInDirection3D(rotOffset + i * angleStep) * radius * scaleOffset);
		
		triangles[i + 1].position = vertexPos;
		triangles[i + 1].color = colorBase;
		lines[i].position = vertexPos;
		lines[i].color = colorAccent;
	}
	
	Vector2f lastPos = CEngine::toIso(center + CEngine::getUnitInDirection3D(rotOffset) * radius * scaleOffset);
	triangles[vertexCount + 1].position = lastPos;
	triangles[vertexCount + 1].color = colorBase;
	lines[vertexCount].position = lastPos;
	lines[vertexCount].color = colorAccent;

	// Assign overlap
	left = CEngine::toIso(center + CEngine::getUnitInDirection3D(45) * radius * scaleOffset).x;
	right = CEngine::toIso(center + CEngine::getUnitInDirection3D(225) * radius * scaleOffset).x;
	top = CEngine::toIso(center + CEngine::getUnitInDirection3D(135) * radius * scaleOffset).y;
	bot = CEngine::toIso(center + CEngine::getUnitInDirection3D(-45) * radius * scaleOffset).y;
}

bool CGraphicArea::doesOverlap(const CGraphicContainer& other) const {
	return CEngine::doesOverlap(*this, other);
}

bool CGraphicArea::isInFrontOf(const CGraphicContainer& other) const {
	if(const CGraphicBox* graphicBox = dynamic_cast<const CGraphicBox*>(&other)) {
		return CEngine::isInFrontOf(*this, *graphicBox);
	} else if(const CGraphicArea* graphicArea = dynamic_cast<const CGraphicArea*>(&other)) {
		return CEngine::isInFrontOf(*this, *graphicArea);
	}
	
	return false;
}

void CGraphicArea::draw(RenderTarget* target) {
	if(doesNeedUpdate) {
		update();
	}
	target->draw(triangles);
	target->draw(lines);
}

void CGraphicArea::drawOutline(RenderTarget* target) {
	if(doesNeedUpdate) {
		update();
	}
	target->draw(lines);
}

void CGraphicArea::drawOverlap(RenderTarget* target) const {
	VertexArray rect = VertexArray(Quads, 4);
	rect[0] = Vector2f(left, top);
	rect[1] = Vector2f(right, top);
	rect[2] = Vector2f(right, bot);
	rect[3] = Vector2f(left, bot);

	for(int i = 0; i < 4; i++) {
		rect[i].color = colorBase;
	}

	target->draw(rect);
}

void CGraphicArea::setPosition(Vector3f pos) {
	needsUpdate();
	move(pos - this->pos);
}

void CGraphicArea::setRotation(float rot) {
	needsUpdate();
	this->rot = rot;
}

void CGraphicArea::setPosOffset(Vector3f offset) {
	needsUpdate();
	CGraphicContainer::setPosOffset(offset);
}

void CGraphicArea::setScaleOffset(float offset) {
	needsUpdate();
	CGraphicContainer::setScaleOffset(offset);
}

void CGraphicArea::setColorBase(Color color) {
	needsUpdate();
	this->colorBase = color;
}

void CGraphicArea::setColorAccent(Color color) {
	needsUpdate();
	this->colorAccent = color;
}

void CGraphicArea::move(Vector3f movement) {
	needsUpdate();
	pos += movement;
}

void CGraphicArea::move(Vector2f movement) {
	needsUpdate();
	Vector3f planarMovement = CEngine::toPlanar3D(movement);
	pos += planarMovement;
}

void CGraphicArea::rotate(float angle) {
	needsUpdate();
	rot = CEngine::rotate(rot, angle);
}

void CGraphicArea::scale(float factor) {
	needsUpdate();
	radius *= fmax(0.01f, factor);
}

Vector3f CGraphicArea::getPosition() const {
	return pos;
}

Vector3f CGraphicArea::getDimension() const {
	return Vector3f(radius, radius, 0.01f);
}

float CGraphicArea::getRotation() const {
	return rot;
}

void CGraphicArea::setRadius(float radius) {
	needsUpdate();
	this->radius = radius;
}

float CGraphicArea::getRadius() const {
	return radius;
}
