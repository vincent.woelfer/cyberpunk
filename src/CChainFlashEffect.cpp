#include "CChainFlashEffect.hpp"
#include "CSceneManager.hpp"

CChainFlashEffect::CChainFlashEffect(Vector3f p1, Vector3f p2) :
	CObject(p1, 0),
    graphicBox(p1, Vector3f(0.075f, 1, 0.075f), COLOR_PINK_ALPHA) {
	
	Vector3f connectVec = p2 - p1;
	Vector3f origin = p1 + connectVec * 0.5f;
	Vector2f dir = CEngine::to2D(connectVec);
	float rotation = CEngine::getRotation(dir);
	
	graphicBox.setPosition(origin);
	graphicBox.setRotation(rotation);
	graphicBox.setDimension(Vector3f(0.075f, CEngine::getLength(connectVec)/2.f, 0.075f));
}

void CChainFlashEffect::tick(float frameTime) {
	if(clock.getElapsedTime().asMilliseconds() >= lifetime) {
		CSceneManager::safeRemoveObject(this);
	}
}

CCollisionContainer* CChainFlashEffect::getCollisionContainer() {
	return nullptr;
}

CGraphicContainer* CChainFlashEffect::getGraphicContainer() {
	return &graphicBox;
}
