#ifndef CYBERPUNK_CSKILLSHOTGUNFIRE_HPP
#define CYBERPUNK_CSKILLSHOTGUNFIRE_HPP


#include <SFML/Window.hpp>
#include "CSkill.hpp"
#include "CObjectIdManager.hpp"
#include "CCharacter.hpp"

class CProjectile;

struct CSkillShotGunFireRaycastResult {
	bool isCharacter;
	CCharacter* target;
	Vector2f dir;
	Vector3f hitPosition; 
};

struct CSkillShotGunFirePopupBoxNetwork {
	CObjectId target;
	Vector3f relativePos;
};

struct CSkillShotGunFireHitResult {
	CCharacter* target;
	CObjectId targetId;
	Vector2f dir;
	sf::Uint8 bulletCount;
};

class CSkillShotGunFire : public CSkill {
private:
	const float fireCooldown = 1100;
	const float reloadCooldown = 2000;
	const int maxBullets = 6;
	int remainingBullets = maxBullets;
	float bulletSpreadAngle = 27.f;
	int bulletCountPerShot = 10;
	
	float bulletRange = 5.f;
	float firePositionOffsetLength = 0.3f;
	float firePositionLateralOffsetRange = 0.5f;
	
	float damagePerBullet = 15.f;
	float pushStrengthBasis = 22.f;
	float pushStrengthPerAdditionalBullet = 12.f;
	float pushDurationBasis = 60.f;
	float pushDurationPerAdditionalBullet = 10.f;

public:
	CSkillShotGunFire(CPlayer* owner) :
			CSkill(owner, 1000, 0) {}

	virtual void tick(float frameTime);

	virtual void quickDraw();
	virtual void quickDrawRPC();
	virtual void reload();
	virtual void reloadRPC(float gameTime);

	void fire(Vector2f dir);
	void fireRPC(Vector2f dir, std::vector<CSkillShotGunFireHitResult> hitResults, std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxes);
	
protected:
	void onButtonPress();

private:
	void stopReloading(bool wasQuickdraw);
	
	std::vector<CSkillShotGunFireRaycastResult> performRaycast(Vector2f dir);
	std::vector<CSkillShotGunFirePopupBoxNetwork> getPopupBoxesNetwork(std::vector<CSkillShotGunFireRaycastResult> raycastResults);
	std::vector<CSkillShotGunFireHitResult> getHitResults(std::vector<CSkillShotGunFireRaycastResult> raycastResults);
	
	void hitEnemies(std::vector<CSkillShotGunFireHitResult> hitResults);
	
	void spawnPopupBoxesLocal(std::vector<CSkillShotGunFireRaycastResult> raycastResults);
	void spawnPopupBoxesNetwork(std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxesNetwork);
	void spawnFireLinesNetwork(Vector2f dir);
	
	void spawnPopupBox(Vector3f pos) const;
	void spawnFireLine(Vector3f startPos, Vector3f endPos) const;
	void spawnFirePopupBox(Vector2f dir) const;
	
	void convertIdsToPointers(std::vector<CSkillShotGunFireHitResult>& hitResults);
};


#endif //CYBERPUNK_CSKILLSHOTGUNFIRE_HPP
