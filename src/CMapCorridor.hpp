#ifndef CYBERPUNK_CMAPCORRIDOR_HPP
#define CYBERPUNK_CMAPCORRIDOR_HPP


#include "CMapRoom.hpp"
#include "CColorDefines.hpp"

class CMapCorridor : public CMapRoom {
private:
	int length;

	Color colorWallBase = COLOR_NRW_DARK_CORRIDOR_BASE;
	Color colorWallAccent = COLOR_NRW_DARK_CORRIDOR_ACCENT;
	Color colorFloorBase = COLOR_NRW_DARK_FLOOR_BASE;
	Color colorFloorAccent = COLOR_NRW_DARK_FLOOR_ACCENT;		

public:
	CMapCorridor(Doorway entrance, Doorway exit, int level);
	
	int getLength() const;

private:
	void generateStraightCorridor();

};


#endif //CYBERPUNK_CMAPCORRIDOR_HPP
