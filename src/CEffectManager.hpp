#ifndef CYBERPUNK_CEFFECTMANAGER_HPP
#define CYBERPUNK_CEFFECTMANAGER_HPP

#include <vector>
#include <queue>
#include <SFML/System.hpp>
#include "CGuiElement.hpp"
#include "CGuiManager.hpp"
#include "CEffectMark.hpp"

class CCharacter;
class CEffect;

using namespace std;
using namespace sf;

struct CEffectAggregate {
	float movementSpeedModifier = 1.f;
	
	bool hasForcePush = false;
	Vector2f forcePush = Vector2f(0, 0);
	
	bool hasStun = false;
	
	std::map<CEffectMark::EffectMarkType, int> marks;	
	
	bool hasMinAlpha = false;
	float minAlpha = 0;
};

/* ToDo:
 * removeEffect() (specific class, id, removeAllSlows???)
 */
struct CompareTextures {
	bool operator()(pair<int, std::vector<const sf::Texture*>> lhs, pair<int, std::vector<const sf::Texture*>> rhs) const {
		return lhs.first > rhs.first;
	}
};

typedef std::priority_queue<std::pair<int, std::vector<const sf::Texture*>>, std::vector<std::pair<int, std::vector<const sf::Texture*>>>, CompareTextures> TexturesPriorityQueue;

class CEffectManager : public CGuiElement {
private:
	CCharacter* owner;
	CEffectAggregate effectAggregate;
	vector<CEffect*> effects;

	Vector2f screenPos;
	
	float effectIconSize = 25;
	float effectIconSpacing = 1;
	float yOffset = 70;

	mutable bool triggeredOwnerDeathThisFrame = false; 

public:
	CEffectManager(CCharacter* owner) : 
			owner(owner) {
		CGuiManager::addGuiElement(this);
	};
	
	~CEffectManager();
	
	void addEffect(CEffect* effect);
	void tickEffects(float frameTime);
	
	void removeEffectMark(CEffectMark::EffectMarkType, int markCount);
	
	CEffect* getEffectWithId(std::string id);
	CEffect* getEffectWithId(long id);
	void removeEffectWithId(std::string id);
	void removeEffectWithId(long id);
	
	virtual void draw(sf::RenderTarget* renderTarget);
	
	/* Overwrite from GuiElement */
	virtual void tick(float frameTime);
	
	void setPosition(Vector3f pos);
	void ownerDeath();

private:
	void resetEffectAggregate();

public:
	// TODO extract to own class
	/* Effect aggregate accessors */
	float getMovementSpeedModifier() const;
	
	bool hasForcePush() const;
	Vector2f getForcePush() const;
	
	bool hasStun() const;
	
	bool hasMark(CEffectMark::EffectMarkType markType) const;
	int getMarkCount(CEffectMark::EffectMarkType markType) const;
	
	bool hasMinAlpha() const;
	Uint8 getMinAlpha() const;
};


#endif //CYBERPUNK_CEFFECTMANAGER_HPP
