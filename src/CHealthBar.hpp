#ifndef CYBERPUNK_CHEALTHBAR_HPP
#define CYBERPUNK_CHEALTHBAR_HPP

#include <SFML/Graphics.hpp>
#include "CGuiElement.hpp"

using namespace sf;

class CHealthBar : public CGuiElement {
private:
	float maxHealth;
	float currentHealth;
	float previewHealth;

	Vector2f screenPos;

	float width = 80;
	float height = 7;
	Color color = Color(255, 0, 0);
	Color backgroundColor = Color(20, 20, 20, 80);
	Color previewColor = Color(150, 50, 50, 200);
	float previewUpdateSpeedPercentage = 0.04;
	float yOffset = 80;
	
	float separatorHealthSize = 25.f;
	float separatorPixelSize = 3.f;

	VertexArray vertexArray;
	VertexArray vertexArraySeparators;

public:
	CHealthBar(float maxHealth);

	virtual void draw(RenderTarget* renderTarget);
	virtual void tick(float frameTime);
	
	void setCurrentHealth(float currentHealth);
	void setPosition(Vector3f position);
	void setYOffset(float yOffset);
	void setMaxHealth(float maxHealth);

	void update(float frameTime);

private:
	int getHealthSeparatorCount();
};


#endif //CYBERPUNK_CHEALTHBAR_HPP