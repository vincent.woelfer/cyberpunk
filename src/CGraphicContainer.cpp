#include "CGraphicContainer.hpp"
#include "CEngine.hpp"

void CGraphicContainer::setPosition(Vector3f pos) {
	needsUpdate();
	this->pos = pos;
}

void CGraphicContainer::setPosOffset(Vector3f offset) {
	needsUpdate();
	this->posOffset = offset;
}

void CGraphicContainer::setRotation(float rot) {
	needsUpdate();
	this->rot = rot;
}

void CGraphicContainer::move(Vector3f movement) {
	needsUpdate();
    pos += movement;
}

void CGraphicContainer::move(Vector2f movement) {
	needsUpdate();
	pos += CEngine::toPlanar3D(movement);
}

void CGraphicContainer::rotate(float angle) {
	needsUpdate();
	rot = CEngine::rotate(rot, angle);
}

Vector3f CGraphicContainer::getPosition() const {
	return pos;
}

Vector3f CGraphicContainer::getPosOffset() const {
	return posOffset;
}

float CGraphicContainer::getRotation() const {
	return rot;
}

float CGraphicContainer::getDepth() const {
	return CEngine::getDepth(pos);
}

void CGraphicContainer::setVisible(bool visible) {
	needsUpdate();
	this->visible = visible;
}

bool CGraphicContainer::isVisible() const {
	return visible;
}

void CGraphicContainer::setColorBase(Color color) {
	needsUpdate();
	this->colorBase = color;
}

void CGraphicContainer::setColorAccent(Color color) {
	needsUpdate();
	this->colorAccent = color;
}

Color CGraphicContainer::getColorBase() const {
	return colorBase;
}

Color CGraphicContainer::getColorAccent() const {
	return colorAccent;
}

float CGraphicContainer::getLeft() const {
	return left;
}

float CGraphicContainer::getRight() const {
	return right;
}

float CGraphicContainer::getTop() const {
	return top;
}

float CGraphicContainer::getBot() const {
	return bot;
}

void CGraphicContainer::setScaleOffset(float offset) {
	needsUpdate();
	scaleOffset = offset;
}

float CGraphicContainer::getScaleOffset() const {
	return scaleOffset;
}

void CGraphicContainer::needsUpdate() {
	doesNeedUpdate = true;
}
