#ifndef CYBERPUNK_COBJECT_H
#define CYBERPUNK_COBJECT_H

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window.hpp>
#include "CTranslationSnapshot.hpp"
#include "CTranslationSnapshotBuffer.hpp"
#include "CObjectIdManager.hpp"

using namespace sf;

class CCollisionContainer;
class CGraphicContainer;

class CObject {
private:
	CObjectId id;
	
protected:
	Vector3f pos;
	float rot;
	bool isStatic;
	
	bool isLocal = true;
	bool isMovementReplicated = false;
	CTranslationSnapshotBuffer translationSnapshotBuffer;

public:
	CObject(Vector3f pos) :
			id(CObjectIdManager::getUniqueId()),
			pos(pos),
			rot(0),
			isStatic(false) {}

	CObject(Vector3f pos, bool isStatic) :
			id(CObjectIdManager::getUniqueId()),
			pos(pos),
			rot(0),
			isStatic(isStatic) {}

	virtual ~CObject() {};

    // Tick
    virtual void tick(float frameTime);
    virtual void tickAuthority(float frameTime) {};

    // Getter & Setter
	virtual CCollisionContainer* getCollisionContainer() = 0;
	virtual CGraphicContainer* getGraphicContainer() = 0;
	
	CObjectId getId() const;
	void setId(CObjectId id); // Only use if you know what you are doing

	/* Transformable Accessors */
	virtual void setPosition(Vector3f pos);
	virtual void setRotation(float rot);

	virtual bool move(Vector3f movement);
	virtual bool move(Vector2f movement);
	virtual void rotate(float angle);

	virtual Vector3f getPosition() const;
	virtual float getRotation() const;
	
	bool getIsStatic() const;
	void updateIsStatic(bool isStatic);
	
	// TODO WIP
	virtual void setNotLocal(CObjectId id);
	bool getIsLocal() const;
	void setIsMovementReplicated(bool isMovementReplicated);
	bool getIsMovementReplicated() const;
	void updateTranslationSnapshotBuffer(CTranslationSnapshot translationSnapshot);
	void applyReplicatedMovement();
	void createTranslationSnapshotMessage() const;
	void createTranslationSnapshotMessage(float gameTime) const;

    // Display
    virtual void draw(sf::RenderTarget* target);
};

#endif //CYBERPUNK_COBJECT_H