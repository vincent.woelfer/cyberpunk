#ifndef CYBERPUNK_CEFFECTSTUN_HPP
#define CYBERPUNK_CEFFECTSTUN_HPP

#include "CEffect.hpp"
#include "CEnemy.hpp"

class CEffectStun : public CEffect {
private:
	float movementSpeedModifier;

public:
	CEffectStun(CCharacter* initiator, float lifetime) :
	CEffect(initiator, lifetime) {}

	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate);
	
	virtual std::vector<const sf::Texture*> getVisuals();
	virtual int getVisualPriority();

};

#endif //CYBERPUNK_CEFFECTSTUN_HPP
