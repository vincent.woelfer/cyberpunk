#include <iostream>
#include "CGuiManager.hpp"
#include "CRenderManager.hpp"

vector<CGuiElement*> CGuiManager::guiElements;
Font CGuiManager::fontDisplay;
Font CGuiManager::fontDebug;
std::string CGuiManager::fontNameDisplay = "Gravedigger";
std::string CGuiManager::fontNameDebug = "UbuntuMono";
sf::Vector2f CGuiManager::guiSize;

void CGuiManager::addGuiElement(CGuiElement* guiElement) {
	guiElements.push_back(guiElement);
}

void CGuiManager::removeGuiElement(CGuiElement* guiElement) {
	guiElements.erase(remove(guiElements.begin(), guiElements.end(), guiElement), guiElements.end());
}

void CGuiManager::drawGui(RenderTarget* renderTarget) {
	for(CGuiElement* guiElement : guiElements) {
		guiElement->draw(renderTarget);
	}
}

void CGuiManager::tickAll(float frameTime) {
	for(CGuiElement* guiElement : guiElements) {
		guiElement->tick(frameTime);
	}
}

void CGuiManager::loadFonts() {
	if(!fontDisplay.loadFromFile("data/" + fontNameDisplay + ".ttf")) {
		cout << "Display font not found!" << endl;
	}

	if(!fontDebug.loadFromFile("data/" + fontNameDebug + ".ttf")) {
		cout << "Debug font not found!" << endl;
	}
}

Font& CGuiManager::getFontDisplay() {
	return fontDisplay;
}

Font& CGuiManager::getFontDebug() {
	return fontDebug;
}

Vector2f CGuiManager::getGuiSize() {
	return guiSize;
}

float CGuiManager::getRelativeToGuiSizeX(float percentageX) {
	return percentageX * guiSize.x;
}

float CGuiManager::getRelativeToGuiSizeY(float percentageY) {
	return percentageY * guiSize.y;
}

Vector2f CGuiManager::getRelativeToGuiSize(float percentage) {
	return percentage * guiSize;
}

Vector2f CGuiManager::getRelativeToGuiSize(Vector2f percentage) {
	return Vector2f(percentage.x * guiSize.x, percentage.y * guiSize.y);
}

void CGuiManager::setGuiSize(Vector2f size) {
	guiSize = size;
}

float CGuiManager::getGuiFontSizeFactor() {
	return 1.f + (guiSize.x - 1920)/2000.f;
}
