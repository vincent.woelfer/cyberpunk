#include "CSkillTeleport.hpp"
#include "CInputManager.hpp"
#include "CSceneManager.hpp"
#include "CUtilityDefines.hpp"
#include "CMap.hpp"
#include "CNavMesh.hpp"
#include "CFlatCircle.hpp"

void CSkillTeleport::onButtonPress() {
	owner->getEffectManager()->removeEffectWithId("CEffectSpecterPull");
	
	Vector3f oriPos = owner->getPosition();
	Vector2f dir = CInputManager::getDirToMouseInScene(owner->getPosition());

	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});
	Vector3f targetPos = owner->getPosition() + CEngine::toPlanar3D(dir * distance);
	Vector3f dim = dynamic_cast<CBoundingBox*>(owner->getCollisionContainer())->getDimension();
	targetPos = CSceneManager::findSpawnPoint(targetPos, dim, collisionChannel, owner->getRotation(), 0, 6, 0.25f, owner);

	if(targetPos != VECTOR3D_INVALID) {
		// Only teleportation within same nav mesh is ok
		if(CSceneManager::getMap()->getNavMeshForPosition(CEngine::to2D(owner->getPosition()))->isInside(targetPos)) {
			owner->teleport(targetPos);
			
			// Visuals
			float circleRadius = 0.25f;
			float dotRadius = 0.1f;
			Color color = owner->getCharacterClass()->getBaseColor();
			Color frameColor = owner->getCharacterClass()->getAccentColor();
			float lifetime = 250;
			CSceneManager::safeAddObject(new CFlatCircle(oriPos, circleRadius, color, frameColor, lifetime));
			CSceneManager::safeAddObject(new CFlatCircle(targetPos, circleRadius, color, frameColor, lifetime));
			for(int i = 1; i < 5; ++i) {
				Vector3f dist = (targetPos - oriPos)/5.f;
				CSceneManager::safeAddObject(new CFlatCircle(oriPos + float(i) * dist, dotRadius, color, frameColor, lifetime));
			}
		}
	}

}
