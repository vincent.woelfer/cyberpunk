#include "CProjectileHackSignal.hpp"
#include "CEffectStun.hpp"
#include "CSceneManager.hpp"
#include "CRpcHandler.hpp"
#include "CGui.hpp"

CProjectileHackSignal::CProjectileHackSignal(CCharacter* initiator, const Vector3f &pos, const Vector2f &dir, Color colorBase, Color colorAccent) :
		CProjectile(initiator, pos, Vector3f(0.25f, 0.25f, 0.25f), colorBase, dir, 10), colorBase(colorBase), colorAccent(colorAccent) {
	
	setLifetime(2000);
	getCollisionContainer()->setCollisionChannel(CCollisionChannel::Channel::PROJECTILE);
	CFlatCircle *startCircle = new CFlatCircle(pos, circleRadius, colorBase, colorAccent, 10000);
	trailVisuals.push_back(startCircle);
	CSceneManager::safeAddObject(startCircle);

}

void CProjectileHackSignal::tick(float frameTime) {
	CProjectile::tick(frameTime);
	CFlatCircle *prevDot = trailVisuals[trailVisuals.size() - 1];
	if (CEngine::getDistance(prevDot->getPosition(), getPosition()) > dotDistance + dotDistanceOffset) {
		Vector2f direction = CEngine::normalize(dir);
		CFlatCircle *newDot = new CFlatCircle(prevDot->getPosition() + CEngine::toPlanar3D(direction) * dotDistance, dotRadius, colorBase, colorAccent, 10000);
		trailVisuals.push_back(newDot);
		CSceneManager::safeAddObject(newDot);
	}
}

CProjectileHackSignal::~CProjectileHackSignal() {
	for (CFlatCircle *c : trailVisuals) {
		if(c != nullptr) {
			CSceneManager::safeRemoveObject(c);
		}
	}
}

void CProjectileHackSignal::onHitEnemy(CEnemy *target, float frameTime) {
	if(!isLocal) {
		return;
	}
	target->addEffect(new CEffectStun(nullptr, hackingDuration));
	if(dynamic_cast<CPlayer *>(initiator)!= nullptr) {
		dynamic_cast<CPlayer *>(initiator)->addAssist();
		CGui::updateKDA();
	}

	CSceneManager::safeRemoveObject(this);

	CRpcHandler::sendProjectileHackSignalOnHitEnemy(this, target->getId(), dir);
}

void CProjectileHackSignal::onHitEnemyRPC(CEnemy *target) {
	target->addEffect(new CEffectStun(nullptr, hackingDuration));
	CSceneManager::safeRemoveObject(this);
}
