#ifndef CYBERPUNK_CNAVPOLYGON_HPP
#define CYBERPUNK_CNAVPOLYGON_HPP

#include <vector>
#include <SFML/System.hpp>
#include "CNavPolygonBorder.hpp"
#include "CNavPolygonEdge.hpp"
#include "CObject.hpp"
#include "CRectangleArea.hpp"


using namespace sf;
using namespace std;

struct CNavPolygonExtend {
	float minX;
	float maxX;
	float minY;
	float maxY;
};

class CNavPolygon {
protected:
	CNavPolygon() {}
	
	vector<CNavPolygonEdge*> adjacent;
	vector<CObject*> debugVisuals;

public:
	virtual ~CNavPolygon();

	void connectTo(CNavPolygon* target, bool connectViceVersa);
	void disconnectFrom(CNavPolygon* target);
	
	vector<CNavPolygonEdge*> getAdjacent() const;
	
	virtual bool isInside(Vector2f pos) const = 0;
	virtual bool isInside(Vector3f pos) const = 0;

	virtual bool isInsideExclusiveBorder(Vector2f pos) const = 0;
	virtual bool isInsideExclusiveBorder(Vector3f pos) const = 0;

	virtual Vector2f getRandomPointInside() const = 0;
	
	virtual vector<CNavPolygonBorder> getBorders() const = 0;
	
	virtual CNavPolygonExtend getExtend() const = 0;
	virtual float getAreaSize() const = 0;
	virtual Vector2f getCenter() const = 0;

	virtual bool intersectsRectangleArea(CRectangleArea rectangleArea) const = 0;
	virtual bool intersectsRectangleAreaExclusiveBorders(CRectangleArea rectangleArea) const = 0;

	virtual vector<CNavPolygon*> splitUp(CRectangleArea rectangleArea) = 0;
	
	virtual void addDebugVisuals() = 0;
	virtual void removeDebugVisuals() = 0;
};

#endif //CYBERPUNK_CNAVPOLYGON_HPP