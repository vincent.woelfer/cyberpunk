#ifndef CYBERPUNK_CTEMPSPAWNPOINT_HPP
#define CYBERPUNK_CTEMPSPAWNPOINT_HPP


#include "CObject.hpp"
#include "CRectangleArea.hpp"
#include "CColorDefines.hpp"
#include "CGraphicBox.hpp"
#include "CBoundingBox.hpp"

class CTempSpawnPoint : public CObject {
private:
	CBoundingBox boundingBox;
	CGraphicBox graphicBox;
	
	Color colorBaseNormal = COLOR_SPAWNPOINT_BASE;
	Color colorAccentNormal = COLOR_SPAWNPOINT_ACCENT;
	
	float glowingTimeOffset = 0;
	float glowingAmplitude = 45;
	float glowingFrequency = 1.2f;
	float glowingScaleAmplitude = 0.04f;
	
	bool isRising = true;
	Clock clockRise;
	float riseDuration = 800;
	float riseOffsetX;
	float riseOffsetY;
	
	bool isDespawning = false;
	Clock clockDespawning;
	float despawnTime = 2000;
	
	CRectangleArea blockedArea;

public:
	explicit CTempSpawnPoint(Vector2f pos);
	~CTempSpawnPoint() override;

	void tick(float frameTime) override;

	// Getter & Setter
	CCollisionContainer* getCollisionContainer() override;
	CGraphicContainer* getGraphicContainer() override;

	void deletePoint();
	void deletePointRPC();

};


#endif //CYBERPUNK_CTEMPSPAWNPOINT_HPP
