#ifndef CYBERPUNK_CMESSAGEENEMYSPAWN_HPP
#define CYBERPUNK_CMESSAGEENEMYSPAWN_HPP


#include <SFML/System.hpp>
#include "CMessage.hpp"
#include "CObjectIdManager.hpp"
#include "CTempSpawningManager.hpp"

class CMessageEnemySpawn : public CMessage {
public:
	CEnemyTypeHologram enemyType;
	CObjectId objectId;
	sf::Vector3f pos;
	float rot;

	CMessageEnemySpawn() :
		CMessage(CMessageType::ENEMY_SPAWN) {}

	void serializeInto(sf::Packet& packet) override;
	void deserializeFrom(sf::Packet& packet) override;
	void process() override;

};


#endif //CYBERPUNK_CMESSAGEENEMYSPAWN_HPP
