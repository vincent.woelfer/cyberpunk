#include "CRenderManager.hpp"
#include "CEngine.hpp"
#include "CSceneManager.hpp"

View CRenderManager::viewScene;
View CRenderManager::viewGui;
RenderTexture CRenderManager::textureScene;
RenderTexture CRenderManager::textureGui;
Shader CRenderManager::shaderGlow;
Vector2f CRenderManager::ripplePos;
float CRenderManager::rippleFac;
Sprite CRenderManager::renderSprite;

void CRenderManager::addRippleEffect(Vector3f pos) {
	ripplePos = CEngine::toIso(pos);
	
	ripplePos.x -= viewScene.getCenter().x;
	ripplePos.y -= viewScene.getCenter().y;
	
	ripplePos.x += viewScene.getSize().x/2;
	ripplePos.y += viewScene.getSize().y/2;
	
	ripplePos.y = -(ripplePos.y - viewScene.getSize().y);
	
	ripplePos.x /= viewScene.getSize().x;
	ripplePos.y /= viewScene.getSize().y;
	
	rippleFac = 0.001f;
}

void CRenderManager::tick(float frameTime) {
	if(rippleFac != 0) {
		rippleFac += 9 * frameTime;		
	}
	if(rippleFac >= 1.15f) {
		rippleFac = 0;
	}

	//shaderGlow.setParameter("pos", ripplePos);
	//shaderGlow.setParameter("fac", rippleFac);
}

void CRenderManager::initialize(VideoMode videoMode) {
	float sceneSizeX = 1920;
	float sceneSizeY = 1080;

	float sceneRatio = sceneSizeX/sceneSizeY;
	float videoModeRatio = (float)videoMode.width / (float)videoMode.height;
	
	if(videoModeRatio != sceneRatio) {
		if(videoModeRatio < sceneRatio) {
			sceneSizeX = sceneSizeY * videoModeRatio;
		} else {
			sceneSizeY = sceneSizeX * 1.f/videoModeRatio;
		}		
	}
	
	viewScene = View(Vector2f(0, 0), Vector2f(sceneSizeX, sceneSizeY));
	viewGui = View(Vector2f(videoMode.width/2.f, videoMode.height/2.f), Vector2f(videoMode.width, videoMode.height));
	
	CGuiManager::setGuiSize(Vector2f(videoMode.width, videoMode.height));
	
	// Render textures
	textureScene.create(videoMode.width, videoMode.height);
	textureGui.create(videoMode.width, videoMode.height);

	// Shaders
	shaderGlow.loadFromFile("shaders/shader_glow.frag", Shader::Fragment);
	shaderGlow.setParameter("texture", Shader::CurrentTexture);
}

View* CRenderManager::getViewScene() {
	return &viewScene;
}

View* CRenderManager::getViewGui() {
	return &viewGui;
}

void CRenderManager::clear() {  
	textureScene.clear(Color::Black);
	textureGui.clear(Color::Transparent);
}

RenderTarget* CRenderManager::getTextureScene() {
	return &textureScene;
}

RenderTarget* CRenderManager::getTextureGui() {
	return &textureGui;
}

void CRenderManager::drawGui() {
	CGuiManager::drawGui(&textureGui);
}

void CRenderManager::render(RenderWindow* window) {
	textureScene.display();
	textureGui.display();
	
	renderSprite.setTexture(textureScene.getTexture());
	window->draw(renderSprite, &shaderGlow);
	renderSprite.setTexture(textureGui.getTexture());
	window->draw(renderSprite);
	
	window->display();
}

void CRenderManager::updateView() {
	textureScene.setView(viewScene);
	textureGui.setView(viewGui);
}

Vector2f CRenderManager::convertSceneToGui(Vector2f pos) {
	return Vector2f(textureScene.mapCoordsToPixel(pos));
}

void CRenderManager::setBlurStrength(float blurStrength) {
	shaderGlow.setParameter("blur_radius", blurStrength);
}

