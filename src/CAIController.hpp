#ifndef CYBERPUNK_CAICONTROLLER_HPP
#define CYBERPUNK_CAICONTROLLER_HPP


class CAIController {
public:
	CAIController() {}
	virtual ~CAIController() {};

	virtual void tick(float frameTime) = 0;
};

#endif //CYBERPUNK_CAICONTROLLER_HPP
