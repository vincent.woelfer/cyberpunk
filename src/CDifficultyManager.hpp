#ifndef CYBERPUNK_CDIFFICULTYMANAGER_HPP
#define CYBERPUNK_CDIFFICULTYMANAGER_HPP


#include <SFML/Config.hpp>

enum class CDifficulty : sf::Uint8 {
	DEBUG, EASY, MEDIUM, HARD
};

class CDifficultyManager {
private:
	// Private constructor
	CDifficultyManager() {}

	static CDifficulty difficulty;
	static int playerCount;
	static int additionalPlayerCount;


public:

	static void setDifficulty(CDifficulty difficultyUpdate);
	static void setNumberOfPlayers(int numOfPlayers);


	//Getter
	static float getEnemyHealthModifier();
	static float getSpawnBudgetModifier();
	static float getIntensityModifier();

private:

	static float linearScale(float intercept, float increase, float x);
	static float squaredScale(float intercept, float increase, float x);
	static float polynomScale(float intercept, float linearIncrease, float x, float squaredIncrease, float x_squared);
	//polynom currently reduced to 2 order terms

};


#endif //CYBERPUNK_CDIFFICULTYMANAGER_HPP
