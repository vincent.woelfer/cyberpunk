#ifndef CYBERPUNK_CSKILLHACKSIGNAL_HPP
#define CYBERPUNK_CSKILLHACKSIGNAL_HPP


#include <vector>
#include "CSkill.hpp"
#include "CObjectIdManager.hpp"
#include "CProjectileHackSignal.hpp"

class CSkillHackSignal : public CSkill {
private:

public:
	CSkillHackSignal(CPlayer* owner) :
	CSkill(owner, 250, 50) {}

	void fire(Vector2f dir);
	void fireRPC(float gameTime, Vector3f originalPos, Vector2f dir, CObjectId projectileIds);

protected:
	void onButtonPress();
	CProjectile *spawnHackProjectile(Vector2f dir);
};


#endif //CYBERPUNK_CSKILLHACKSIGNAL_HPP
