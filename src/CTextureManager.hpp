#ifndef CYBERPUNK_CTEXTUREMANAGER_HPP
#define CYBERPUNK_CTEXTUREMANAGER_HPP

#include <SFML/Graphics/Texture.hpp>
#include <map>


class CTextureManager {
private:
	static std::map<std::string, sf::Texture> textures;
	
	static std::string path;
	static std::string extension;
	
	static bool loadTexture(std::string name);

	CTextureManager() {};
	
public:
	static const sf::Texture& getTexture(std::string name);

};


#endif //CYBERPUNK_CTEXTUREMANAGER_HPP
