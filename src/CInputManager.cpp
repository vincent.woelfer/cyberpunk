#include "CInputManager.hpp"
#include "CRenderManager.hpp"
#include "CGui.hpp"
#include "CDebugHelper.hpp"

using namespace sf;

// Initialize static key variables
Keyboard::Key CInputManager::keyPlayerMoveUp = Keyboard::W;
Keyboard::Key CInputManager::keyPlayerMoveDown = Keyboard::S;
Keyboard::Key CInputManager::keyPlayerMoveLeft = Keyboard::A;
Keyboard::Key CInputManager::keyPlayerMoveRight = Keyboard::D;

Mouse::Button CInputManager::keyPlayerMousePrimary = Mouse::Button::Left;
Mouse::Button CInputManager::keyPlayerMouseSecondary = Mouse::Button::Right;

Keyboard::Key CInputManager::keyPlayerSignCast = Keyboard::Space;
Keyboard::Key CInputManager::keyPlayerReload = Keyboard::R;
Keyboard::Key CInputManager::keyPlayerSkill1 = Keyboard::LShift;
Keyboard::Key CInputManager::keyPlayerSwitchClasses = Keyboard::Tab; //Keyboard::Tab;

vector<CSkill*> CInputManager::playerSignSkillQueue;

bool CInputManager::isKeySignCastPressed = false;
bool CInputManager::keyMouseSecondaryPressedLastFrame = false;

CPlayer* CInputManager::player;
RenderWindow* CInputManager::window;
CPatternRecognitionMngr* CInputManager::patternRecognitionMngr;

std::vector<Vector2f> CInputManager::drawingPoints;
CGuiDrawing* CInputManager::guiDrawing;

Keyboard::Key CInputManager::keyDebugNavigation = Keyboard::Num0;
Keyboard::Key CInputManager::keyDebugExecutePrimaryAction = Keyboard::Num9;
Keyboard::Key CInputManager::keyDebugExecuteSecondaryAction = Keyboard::Num8;
bool CInputManager::isDebugNavigationKeyPressed = false;
bool CInputManager::isDebugExecutePrimaryActionKeyPressed = false;
bool CInputManager::isDebugExecuteSecondaryActionKeyPressed = false;

void CInputManager::handleKeyboardInput(float frameTime) {
	if(!window->hasFocus()) {
		return;
	}
	
	player->applyMovementDirection(getNormalizedPlayerMovementDir());

	if(Keyboard::isKeyPressed(keyPlayerReload)) {
		player->getCharacterClass()->getPrimarySkill()->reload();
	}

	if(Keyboard::isKeyPressed(keyPlayerSwitchClasses) && player->isClassSwitchAvailable()) {
		CClassName newClass = player->getCharacterClass()->getClassName();
		if(newClass == CClassName::MEDIATOR) {
			newClass = CClassName::SNIPER;
		} else if(newClass == CClassName::SNIPER) {
			newClass = CClassName::SHOTGUNNER;
		} else if(newClass == CClassName::SHOTGUNNER) {
			newClass = CClassName::MEDIATOR;
		}
		player->setCharacterClass(newClass);
	}

	// Player skills
	if(!playerSignSkillQueue.empty()) {
		bool isKeyPressed = Keyboard::isKeyPressed(keyPlayerSignCast);
		bool hasActivated = playerSignSkillQueue.back()->updateButtonStatus(isKeyPressed);

		if(!isKeyPressed && isKeySignCastPressed && hasActivated) {
			//playerSignSkillQueue.back()->updateButtonStatus(false);
			playerSignSkillQueue.pop_back();
			CGui::getSkillIconList()->popFront();
		}
		isKeySignCastPressed = isKeyPressed;
	}

	player->getCharacterClass()->getMovementSkill()->updateButtonStatus(Keyboard::isKeyPressed(keyPlayerSkill1));

}

void CInputManager::handleMouseInput(float frameTime) {
	if(!window->hasFocus()) {
		return;
	}
	
	player->getCharacterClass()->getPrimarySkill()->updateButtonStatus(Mouse::isButtonPressed(keyPlayerMousePrimary));

	if(Mouse::isButtonPressed(keyPlayerMouseSecondary)) {
		keyMouseSecondaryPressedLastFrame = true;
		
        Vector2f screenPos = getMousePositionGui();
		
		if(drawingPoints.empty()) {
			guiDrawing = new CGuiDrawing(screenPos, player->getCharacterClass()->getBaseColor());
			CGuiManager::addGuiElement(guiDrawing);
			drawingPoints.push_back(screenPos);
		} else if(CEngine::getDistance(drawingPoints.back(), screenPos) >= 1) {
			drawingPoints.push_back(screenPos);
			guiDrawing->addPoint(screenPos);
		}
		
	} else if(keyMouseSecondaryPressedLastFrame){
        keyMouseSecondaryPressedLastFrame = false;
		float qualityScore = 0;
        vector<double> netOutput;

		SIGN sign = patternRecognitionMngr->determineSign(drawingPoints, &qualityScore, netOutput);

		drawingPoints.clear();
		CGuiManager::removeGuiElement(guiDrawing);
		delete guiDrawing;


		CIcon* icon;
		CSkill* castSpell = player->getCharacterClass()->getSkill(sign);
		
		float iconSize = CGuiManager::getRelativeToGuiSizeX(0.15f);
		
		if(castSpell != nullptr) {
			playerSignSkillQueue.push_back(castSpell);
			CGui::getSkillIconList()->pushFront(CIcon::getIconPath(sign));
			
			icon = new CIcon(CIcon::getIconPath(sign), CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.3f)), Vector2f(iconSize, iconSize));
			icon->setColor(player->getCharacterClass()->getBaseColor());
			icon->setLifetime(1200);
			CGuiManager::addGuiElement(icon);
		} else {
			icon = new CIcon("IconMiss", CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.3f)), Vector2f(iconSize, iconSize));
			icon->setLifetime(1800);
			CGuiManager::addGuiElement(icon);
		}

		if(playerSignSkillQueue.size() > 2) {
			playerSignSkillQueue.erase(playerSignSkillQueue.begin());
			CGui::getSkillIconList()->popBack();
		}		
    }
}

void CInputManager::setLocalPlayer(CPlayer* localPlayer) {
    player = localPlayer;
}

void CInputManager::setRenderWindow(RenderWindow* renderWindow) {
	window = renderWindow;
}

void CInputManager::setPatternRecognitionMngr(CPatternRecognitionMngr *reconMngr) {
    patternRecognitionMngr = reconMngr;
    patternRecognitionMngr->prompt = "test";
}

Vector2f CInputManager::getMousePositionScene() {
	return window->mapPixelToCoords(Mouse::getPosition(*window), *CRenderManager::getViewScene());
}

Vector2f CInputManager::getMousePositionGui() {
	return window->mapPixelToCoords(Mouse::getPosition(*window), *CRenderManager::getViewGui());
}

Vector2f CInputManager::getNormalizedPlayerMovementDir() {
	Vector2f movement(0, 0);

	if(Keyboard::isKeyPressed(keyPlayerMoveUp)) {
		movement.x += -1;
		movement.y += -1;
	}
	if(Keyboard::isKeyPressed(keyPlayerMoveDown)) {
		movement.x +=  1;
		movement.y +=  1;
	}
	if(Keyboard::isKeyPressed(keyPlayerMoveLeft)) {
		movement.x += -1;
		movement.y +=  1;
	}
	if(Keyboard::isKeyPressed(keyPlayerMoveRight)) {
		movement.x +=  1;
		movement.y += -1;
	}
	
	return CEngine::normalize(movement);
}

bool CInputManager::isDebugKeyToggledOn(Keyboard::Key key) {
	bool isKeyPressed = Keyboard::isKeyPressed(key);

	if(key == keyDebugNavigation) {
		if(isDebugNavigationKeyPressed) {
			if(!isKeyPressed) {
				isDebugNavigationKeyPressed = false;
			}
		} else {
			if(isKeyPressed) {
				isDebugNavigationKeyPressed = true;
				return true;
			}
		}
		return false;
	}

	if(key == keyDebugExecutePrimaryAction) {
		if(isDebugExecutePrimaryActionKeyPressed) {
			if(!isKeyPressed) {
				isDebugExecutePrimaryActionKeyPressed = false;
			}
		} else {
			if(isKeyPressed) {
				isDebugExecutePrimaryActionKeyPressed = true;
				return true;
			}
		}
		return false;
	}

	if(key == keyDebugExecuteSecondaryAction) {
		if(isDebugExecuteSecondaryActionKeyPressed) {
			if(!isKeyPressed) {
				isDebugExecuteSecondaryActionKeyPressed = false;
			}
		} else {
			if(isKeyPressed) {
				isDebugExecuteSecondaryActionKeyPressed = true;
				return true;
			}
		}
		return false;
	}

	return false;
}

Vector2f CInputManager::getDirToMouseInScene(Vector3f reference) {
	Vector2f screenPos = getMousePositionScene();
	Vector3f worldPos = CEngine::to3DOnPlane(screenPos, reference.z);
	return CEngine::normalize(Vector2f(worldPos.x - reference.x, worldPos.y - reference.y));
}
