#include "CSkillHackSignal.hpp"
#include "CInputManager.hpp"
#include "CProjectileSweepingWall.hpp"
#include "CSceneManager.hpp"
#include "CProjectileHackSignal.hpp"
#include "CRpcHandler.hpp"

void CSkillHackSignal::onButtonPress() {
	Vector2f dir = CInputManager::getDirToMouseInScene(owner->getPosition());

	fire(dir);
}

void CSkillHackSignal::fire(Vector2f dir) {

	CProjectile *p = spawnHackProjectile(dir);
	CRpcHandler::sendPlayerSkillHackSignal(owner, dir, owner->getPosition(), p->getId());
}

void
CSkillHackSignal::fireRPC(float gameTime, Vector3f originalPos, Vector2f dir, CObjectId projectileId) {
	CProjectile* projectile = spawnHackProjectile(dir);
	projectile->setNotLocal(projectileId);
}

CProjectile *CSkillHackSignal::spawnHackProjectile(Vector2f dir) {
	CProjectileHackSignal* p = new CProjectileHackSignal(owner, owner->getPosition(), dir, owner->getCharacterClass()->getBaseColor(), owner->getCharacterClass()->getAccentColor());
	p->setRotation(CEngine::getRotation(dir));
	CSceneManager::safeAddObject(p);
	return p;
}
