#ifndef CYBERPUNK_CSCENEMANAGER_H
#define CYBERPUNK_CSCENEMANAGER_H

#include <vector>
#include <deque>

#include "CObject.hpp"
#include "CPlayer.hpp"
#include "CEnemy.hpp"
#include "CProjectile.hpp"
#include "CSpawnPoint.hpp"
#include "CTopologicalMap.hpp"
#include "CClock.hpp"

using namespace std;

struct CHitResult {
	CObject* target;
	Vector3f hitPos;
	Vector2f hitNormal;
	float hitDistance;
};

class CMap;

class CSceneManager {
private:
    static vector<CObject*> objects;
	static vector<CObject*> objectsPendingForAddition;
	static vector<CObject*> objectsPendingForRemoval;
	static vector<CObject*> objectsPendingForUnregistration;
	static vector<CPlayer*> players;
	static vector<CEnemy*> enemies;
	static vector<CProjectile*> projectiles;
	
	static vector<Vector2f> pointsDrawn;
	
	static CMap* map;
	static CTopologicalMap* topologicalMap;
	
	static int staticObjectsCount;
	
	static void addObject(CObject* object);
	static void removeObject(CObject* object, bool deleteObject = true);

public:
	static void safeAddObject(CObject* object);
	static void safeRemoveObject(CObject* object);
	static void safeUnregisterObject(CObject* object);

	static void safeAddPendingObjects();
	static void safeRemovePendingObjects();
	static void safeUnregisterPendingObjects();
	
	static CObject* getObject(CObjectId id);
	static std::vector<CObject*> getObjectsRemoveNull(std::vector<CObjectId> ids);
	static bool doesObjectExist(CObject* object);

    static std::deque<CObject*> getObjectHierarchy();
	static void requestReconstructionForTopologicalMap();

	static vector<CObject*> getObjects();
	static vector<CPlayer*> getPlayers();
	static vector<CPlayer*> getTargetablePlayers();
	static vector<CEnemy*> getEnemies();
	static vector<CProjectile*> getProjectiles();

    static void tickAll(float frameTime);

	static void setMap(CMap* map);
	static CMap* getMap();

	static void updateAllGraphicContainers();

	// Collision queries
	static bool doesIntersectAny(CObject* object, CCollisionChannel collisionChannel);
	static vector<CObject*> getIntersecting(CObject* object, CCollisionChannel collisionChannel);
	static vector<CObject*> getIntersecting(CCollisionContainer* collisionContainer, CCollisionChannel collisionChannel);

	static Vector3f findSpawnPoint(Vector3f origin, Vector3f dimension, CCollisionChannel collisionChannel, float rot, float radiusStart, float radiusStop, float radiusStep, CObject* self = nullptr);
	
	static CHitResult raycast(Vector3f start, Vector2f dir, CCollisionChannel collisionChannel);

    // Private Constructor
private:
    CSceneManager() {};
};


#endif //CYBERPUNK_CSCENEMANAGER_H
