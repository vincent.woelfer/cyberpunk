#include <iostream>
#include <vector>
#include <random>
#include <stdlib.h>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>
#include "CNeuralNet.hpp"

using namespace std;



TrainingData::TrainingData(const vector<string> filename_v) {
    total_inputNum = (int) filename_v.size() - 1;
    for (string path : filename_v) {
        m_trainingDataPaths.push_back(path);
        ifstream f(path, ios::in);
        matrix input_matrix;
        while (!f.eof()) {
            vector<double> input_line;

            string line;
            getline(f, line);

            stringstream ss(line);


            double oneValue;
            while(ss >> oneValue) {
                input_line.push_back(oneValue);
            }
            input_matrix.push_back(input_line);
        }
        input_matrix.pop_back(); //Remove empty line
        input_cube.push_back(input_matrix);
    }
}

void TrainingData::getTopology(vector<unsigned> &topology) {
    topology.push_back(18);
    topology.push_back(20);
    topology.push_back(10);
    topology.push_back(5);
    /*string line;
    string label;

    getline(m_trainingDataFile, line);
    stringstream ss(line);

    ss >> label;

    if(this->isEOF() || label.compare("topology:") != 0)
        abort(); //TODO actually abort()
    while(!ss.eof()) {
        unsigned n;
        ss >> n;
        topology.push_back(n);
    }

    return;
*/
}

unsigned TrainingData::getNextInputs(vector<double> &inputVals) {
    inputVals.clear();


    //cout << (double) m_trainingDataFiles.size() * rand() / (double) RAND_MAX << endl;
    c_inputNum = (double) (total_inputNum + 1) * rand() / (double) RAND_MAX; //+1 due to trash
    int c_inputLine = (double) input_cube[c_inputNum].size() * rand() / (double) RAND_MAX;



    inputVals = input_cube[c_inputNum][c_inputLine];

    return inputVals.size();
}


unsigned TrainingData::getTargetOutputs(vector<double> &targetOutputVals) {
    targetOutputVals.clear();

    for (int i = 0; i < total_inputNum; ++i) {
        double targetVal = i == c_inputNum ? 1 : 0;
        targetOutputVals.push_back(targetVal);
    }
    return (unsigned int) targetOutputVals.size();

}






//********************** Neuron *************************

double Neuron::eta = 0.1; //0.1 0.15
double Neuron::alpha = 0.6; //0.6 0.5

Neuron::Neuron(unsigned numOutputs, unsigned index) {
    for (unsigned c = 0; c < numOutputs; ++c) {
        m_outputsWeights.push_back(Connection());
        m_outputsWeights.back().weight = randomWeight();
        //m_outputsWeights.back().deltaWeight = 0.1;
    }

    m_index = index;
}

Neuron::Neuron(unsigned numOutputs, unsigned index, vector<double> &weight_v) {
    for (unsigned c = 0; c < numOutputs; ++c) {
        m_outputsWeights.push_back(Connection());
        m_outputsWeights.back().weight = weight_v[c];
        //m_outputsWeights.back().deltaWeight = 0.1;
    }

    m_index = index;
}

void Neuron::feedForward(Layer &prevLayers) {
    double sum = 0.0;

    //Sum the previous layer's outpup including bias node
    for(unsigned int n = 0; n < prevLayers.size(); ++n) {
        sum += prevLayers[n].getOutputVal() * prevLayers[n].m_outputsWeights[m_index].weight;
    }


    m_outputVal = Neuron::transferFunction(sum);
    //cout << " "<< sum << ">>" << m_outputVal;
}

double Neuron::transferFunction(double x) {
    //using tanh - output_range [-1, 1] could be any function
    return (double) tanh(x);
}

double Neuron::transferFunctionDerivative(double x) {
    //tanh derivative (approximately)
    return 1 - x * x;
}

void Neuron::calcOutputGradients(double targetVal) {
    double delta = targetVal - m_outputVal;
    m_gradient = delta * Neuron::transferFunctionDerivative(m_outputVal);
}

void Neuron::calcHiddenGradients(const Layer &nextLayer) {
    double dow = sumDOW(nextLayer);

    m_gradient = dow * Neuron::transferFunctionDerivative(m_outputVal);
}

double Neuron::sumDOW(const Layer &nextLayer) const {
    double sum = 0.0;

    for(unsigned int n = 0; n < nextLayer.size() - 1; ++n) {
        sum += m_outputsWeights[n].weight * nextLayer[n].m_gradient;
    }
    return sum;
}

void Neuron::updateInputWeights(Layer &prevLayer) {

    //The weights to be updated are in the Connection container
    //in the neurons in preceding layer
    for(unsigned int n = 0; n < prevLayer.size(); ++n) {
        Neuron &neuron = prevLayer[n];

        double oldDeltaWeight = neuron.m_outputsWeights[m_index].deltaWeight;
        double newDeltaWeight =
                //Individual input, magnified by the gradient and train rate:
                eta
                * neuron.getOutputVal()
                * m_gradient
                //+ Monumentum  (fraction of prev delta Weight)
                + alpha
                * oldDeltaWeight;

        neuron.m_outputsWeights[m_index].deltaWeight = newDeltaWeight;
        neuron.m_outputsWeights[m_index].weight += newDeltaWeight;
    }
}

double Neuron::randomWeight() {
    double x =  2* (double) rand() / (double) RAND_MAX * 0.98 - 1; //used to be: (double) rand() / (double) RAND_MAX !!!!MAGIC!!!

    //thin out higher numbers
    if (x > 0.5)
        x = (double) rand() / (double) RAND_MAX;
    if (x > 0.8)
        x = (double) rand() / (double) RAND_MAX;


    return x;
}

//********************** Net ****************************

Net::Net(vector<unsigned int> &topology) {
    unsigned numLayers = (unsigned int) topology.size();

    for(unsigned indexLayer = 0; indexLayer < numLayers; ++indexLayer) {
        m_layers.push_back(Layer());
        unsigned numOutputs = indexLayer == topology.size() -1 ? 0 : topology[indexLayer+1];

        for(unsigned int indexNeuron = 0; indexNeuron <= topology[indexLayer]; ++indexNeuron) {
            m_layers.back().push_back(Neuron(numOutputs, indexNeuron));
        }
        m_layers.back().back().setOutputVal(1.0);
    }
}

Net::Net(vector<unsigned> &topology, vector<vector<vector<double> > > &weight_cube) {
    unsigned numLayers = (unsigned int) topology.size();

    for(unsigned indexLayer = 0; indexLayer < numLayers; ++indexLayer) {
        m_layers.push_back(Layer());
        unsigned numOutputs = indexLayer == topology.size() -1 ? 0 : topology[indexLayer+1];

        for(unsigned int indexNeuron = 0; indexNeuron <= topology[indexLayer]; ++indexNeuron) {
            m_layers.back().push_back(Neuron(numOutputs, indexNeuron, weight_cube[indexLayer][indexNeuron]));
        }
        m_layers.back().back().setOutputVal(1.0);
    }
}

void Net::feedForward(const vector<double> &inputVals) {
    assert(inputVals.size() == m_layers[0].size() - 1);

    for(unsigned int i = 0; i < inputVals.size(); ++i) {
        m_layers[0][i].setOutputVal(inputVals[i]);
    }

    //Forward propagate
    for(unsigned int indexLayer = 1; indexLayer < m_layers.size(); ++indexLayer) {
        Layer &prevLayers = m_layers[indexLayer - 1];
        for(unsigned int n = 0; n < m_layers[indexLayer].size() - 1; ++n) {
            m_layers[indexLayer][n].feedForward(prevLayers);
        }
    }
}

void Net::backProp(const vector<double> &targetVals) {
    //Calculate overall net error (RMS of output neuron errors)
    Layer &outputLayer = m_layers.back();
    m_error  = 0.0;

    for(unsigned int n = 0; n < outputLayer.size() - 1; ++n) {
        double delta = targetVals[n] - outputLayer[n].getOutputVal();
        m_error += delta * delta;
    }

    m_error /= outputLayer.size() - 1;
    m_error = (double) sqrt(m_error); //RMS

    //Give an insight on average error
    m_recentAverageError = (m_recentAverageError * m_recentAverageSmoothingFactor + m_error)/(m_recentAverageSmoothingFactor + 1);

    //Calculate the output layer gradients
    for(unsigned int n = 0; n < outputLayer.size() - 1; ++n) {
        outputLayer[n].calcOutputGradients(targetVals[n]);
    }

    //Calculate gradients on hidden layers
    for(unsigned int indexLayer = (unsigned int) (m_layers.size() - 2); indexLayer > 0; --indexLayer) {
        Layer &hiddenLayer = m_layers[indexLayer];
        Layer &nextLayer = m_layers[indexLayer + 1];

        for(unsigned n = 0; n < hiddenLayer.size(); ++n) {
            hiddenLayer[n].calcHiddenGradients(nextLayer);
        }
    }

    //For all layers from output to first hidden Layer
    //Update conection weight

    for(int indexLayer = (int) (m_layers.size() - 1); indexLayer > 0; --indexLayer) {
        Layer &layer = m_layers[indexLayer];
        Layer &prevLayer = m_layers[indexLayer - 1];

        for(unsigned int n = 0; n < layer.size(); ++n) {
            layer[n].updateInputWeights(prevLayer);
        }
    }
}

void Net::getResults(vector<double> &resultVals) const {
    resultVals.clear();

    for(unsigned int n = 0; n < m_layers.back().size() - 1; ++n) {
        resultVals.push_back(m_layers.back()[n].getOutputVal());
        //cout << m_layers.back()[n].getOutputVal() << endl;
    }
}

/*std::stringstream Net::getNetStructure() const {
    std::stringstream ss_topology, ss_neurons;
    ss_topology << "topology:";
    for(unsigned int i = 0; i < m_layers.size(); ++i) {
        ss_topology << " " << m_layers[i].size() - 1;
        if(i == m_layers.size() - 1) {
            break;
        }
        for(unsigned int j = 0; j < m_layers[i].size(); ++j) {
            ss_neurons << "[" << i << "," << j << "]";
            const Neuron *c_neuron = &m_layers[i][j];
            for(Connection c : c_neuron->m_outputsWeights) {
                ss_neurons << " " << c.weight;
            }
            ss_neurons << endl;
        }
    }
    ss_topology << endl << ss_neurons.str();
    return ss_topology;
}*/


//**************************** MAIN **************************
/*
 * For debug reseasons and experiments mainly
 */

/*
void showVectorVals(string label, vector<double> &v) {
    cout << label;
    for (int i = 0; i < v.size(); ++i) {
        cout << v[i] << " ";
    }
    cout << endl;
}

int main() {
    //srand(42);

    cout << "application is being started" << endl;
    vector<string> trainPathes = {"./trainingData/alpha.txt", "./trainingData/hline.txt", "./trainingData/vline.txt","./trainingData/wedge_up.txt", "./trainingData/epsilon.txt"};
    TrainingData trainData(trainPathes);
    //TrainingData trainData("./dummy.txt");

    cout << "training data loaded successfully" << endl;

    vector<unsigned> topology; // = {2,3,3,1};
    trainData.getTopology(topology);
    Net myNet(topology);

    cout << "neural net created succefully" << endl;

    vector<double> targetVals, resultVals, inputVals;
    int trainingPass = 0;

    cout << "starting training cycle" << endl;
    int cycle = 10000;
    while (trainingPass < cycle) {
        ++trainingPass;
        cout << "\nPass " << trainingPass << endl;

        //inputVals.clear();
        //inputVals = trainData.getNextUncorruptedInputs();
        if(trainData.getNextInputs(inputVals)!= topology[0]) {
            cout << "wrong input size of " << inputVals.size() << endl;
            cout << "should have been " << inputVals.size() << endl;
            cout << "continue"<< endl;

            //break;
            continue;
        }

        cout << "Input from " << trainData.m_trainingDataPaths[trainData.c_inputNum] << endl;
        //showVectorVals("Input: ", inputVals);
        myNet.feedForward(inputVals);

        myNet.getResults(resultVals);
        showVectorVals("Output: ", resultVals);

        trainData.getTargetOutputs(targetVals);
        showVectorVals("Target: ", targetVals);
        assert(targetVals.size() == topology.back());

        myNet.backProp(targetVals);

        cout << "new recent average error: " << myNet.getRecentAverageError() << endl;
    }

    cout << "application finished\n";
    return 0;
}


*/

//************************ External Call *********************

Net *call(vector<unsigned> topology, vector<string> trainPaths, int number_trainingCycles) {
    cout << "neural net is being called" << endl;

    //vector<string> trainPaths = {"./trainingData/alpha.txt", "./trainingData/hline.txt", "./trainingData/vline.txt","./trainingData/wedge_up.txt", "./trainingData/epsilon.txt", "./trainingData/trash.txt"};
    TrainingData trainData(trainPaths);

    cout << "training data loaded successfully" << endl;

    //vector<unsigned> topology; // = {2,3,3,1};
    //trainData.getTopology(topology); //TODO change output_layer if new pattern added
    Net * myNet = new Net(topology);

    cout << "net created successfully" << endl;
    vector<double> inputVals, resultVals, targetVals;

    int trainingPass = 0;
    int trainingMisses = 0;

    cout << "starting training cycle" << endl;
    int cycle = number_trainingCycles; //4

    while (trainingPass < cycle) {
        ++trainingPass;

        if (trainingPass % (number_trainingCycles / 50) == 0) {
            cout << "\rprogress: >";
            for (int i = 0; i < 50; ++i) {
                if (i* number_trainingCycles / 50 < trainingPass)
                    cout << "|";
                else
                    cout << ".";
            }
            cout << "<" << flush;
        }

        if(trainData.getNextInputs(inputVals)!= topology[0]) {
            ++trainingMisses;
            continue;
        }
        myNet->feedForward(inputVals);

        myNet->getResults(resultVals);

        trainData.getTargetOutputs(targetVals);
        assert(targetVals.size() == topology.back());

        myNet->backProp(targetVals);

    }

    cout << endl << "network fed successfully with "<< trainingPass << " training passes (" << trainingMisses << " misses noticed within)" << endl;
    return myNet;
}