#include "CMapRoomGenerator.hpp"

void CMapRoomGenerator::setColorWallBase(Color color) {
	colorWallBase = color;
}

void CMapRoomGenerator::setColorWallAccent(Color color) {
	colorWallAccent = color;
}

void CMapRoomGenerator::setColorFloorBase(Color color) {
	colorFloorBase = color;
}

void CMapRoomGenerator::setColorFloorAccent(Color color) {
	colorFloorAccent = color;
}
