#include "CLineEffect.hpp"
#include "CProjectileHealthBait.hpp"
#include "CSceneManager.hpp"

void CProjectileHealthBait::onHitEnemy(CEnemy* target, float frameTime) {
	if (!isFalling) {
		target->healHealth(target->getMaxHealth() * baitHealPercentage);
		CSceneManager::safeRemoveObject(this);
	}
}

void CProjectileHealthBait::onHitPlayer(CPlayer *player, float frameTime) {
	if(!isFalling) {
		player->healHealth(player->getMaxHealth() * baitHealPercentage);
		CSceneManager::safeRemoveObject(this);
	}
}

void CProjectileHealthBait::tick(float frameTime) {
	CProjectile::tick(frameTime); //Not quite necessary, do refactor

	if(isFalling) {
		float alpha = clockFall.getElapsedTime().asMilliseconds()/fallDuration;

		if(alpha >= 1) {
			alpha = 1;
			isFalling = false;
		}

		graphicBox.setPosOffset(Vector3f(CEngine::lerp(fallOffsetX, 0, alpha), CEngine::lerp(fallOffsetY, 0, alpha), CEngine::lerp(40, 0, alpha)));
		graphicBox.setScaleOffset(CEngine::lerp(0.4f, 1, alpha));
	}

	if(!isFalling) {
		rotate(rotationSpeed * frameTime);

		if(baitTarget == nullptr || !CSceneManager::doesObjectExist(baitTarget)) {
			CCharacter *bestTarget = nullptr;
			for(CEnemy *e : CSceneManager::getEnemies()) {
				float c_distance = CEngine::getDistance(e->getPosition(), getPosition());
				if(c_distance < baitDistance) { //in bait distance -> take into consideration

					if(bestTarget == nullptr || CEngine::getDistance(bestTarget->getPosition(), getPosition()) > c_distance) {
						bestTarget = e;
						clockBait.restart();
					}
				}
			}
			for(CPlayer *p : CSceneManager::getPlayers()) {
				float c_distance = CEngine::getDistance(p->getPosition(), getPosition());
				if(c_distance < baitDistance) { //in bait distance -> take into consideration

					if(bestTarget == nullptr || CEngine::getDistance(bestTarget->getPosition(), getPosition()) > c_distance) {
						bestTarget = p;
						clockBait.restart();
					}
				}
			}
			if(bestTarget != nullptr) {
				acquireTarget(bestTarget);
				baitLine = new CLineEffect(baitTarget, this, COLOR_DARK_GREEN);
				baitLine->setLifetime(lifetime);
				CSceneManager::safeAddObject(baitLine);
			}
		} else {
			if(CEngine::getDistance(baitTarget->getPosition(), getPosition()) > baitMaxDistance) {
				baitTarget = nullptr;
				CSceneManager::safeRemoveObject(baitLine);
			} else {
				if(clockBait.getElapsedTime().asMilliseconds() > baitDelay) {
					pullTarget(frameTime);
				}
			}
		}

	}
}

void CProjectileHealthBait::acquireTarget(CCharacter *target) {
	baitTarget = target;
}

void CProjectileHealthBait::pullTarget(float frameTime) {
	baitTarget->move(baitPullSpeed * CEngine::to2D(CEngine::normalize(getPosition() - baitTarget->getPosition())) * frameTime, false, true);
}

CProjectileHealthBait::~CProjectileHealthBait() {
	CSceneManager::safeRemoveObject(baitLine);
}

bool CProjectileHealthBait::hasTargetAcquired() {
	if(baitTarget == nullptr) {
		return false;
	}
	return CSceneManager::doesObjectExist(baitTarget);
}

CCharacter *CProjectileHealthBait::getTarget() {
	return baitTarget;
}

float CProjectileHealthBait::getBaitExplosionDamage() {
	return baitExplosionDamage;
}
