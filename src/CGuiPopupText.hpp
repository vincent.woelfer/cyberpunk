#ifndef CYBERPUNK_CGUIPOPUPTEXT_HPP
#define CYBERPUNK_CGUIPOPUPTEXT_HPP


#include <SFML/Graphics/Text.hpp>
#include "CGuiElement.hpp"
#include "CClock.hpp"

class CGuiPopupText : public CGuiElement {
private:
	sf::Text text;
	sf::Vector2f pos;
	float lifetime;
	bool fadeOut;	
	CClock lifetimeClock;

	float fadeAfter = 0.8f;
	
public:
	CGuiPopupText(std::string text, unsigned int size, sf::Color color, sf::Vector2f pos, float lifetime, bool fadeOut);
	
	virtual void draw(sf::RenderTarget* renderTarget);

	virtual void tick(float frameTime);

	void setColor(sf::Color color);

};


#endif //CYBERPUNK_CGUIPOPUPTEXT_HPP
