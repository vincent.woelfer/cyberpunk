#include "CGameClock.hpp"
#include "CEngine.hpp"
#include "CDebugHelper.hpp"

CClock CGameClock::gameClock;
float CGameClock::measuredOffset = 0;
float CGameClock::offsetToLocalClock = 0;
int CGameClock::ignorePackets = 0;

void CGameClock::init() {
	gameClock.restart(1000);
	measuredOffset = 0;
	offsetToLocalClock = 0;
}

float CGameClock::getGameTime() {
	return gameClock.getTimeMs() + offsetToLocalClock;
}

void CGameClock::tick(float frameTime) {
	float drift = 0;
	if(measuredOffset > 0) {
		drift = CEngine::clamp(measuredOffset * frameTime, 0, measuredOffset);
	} else if(measuredOffset < 0) {
		drift = CEngine::clamp(measuredOffset * frameTime, measuredOffset, 0);
	}	 
	 
	measuredOffset -= drift;
	offsetToLocalClock += drift;
}

void CGameClock::adjustTowards(float targetTime) {
	if(ignorePackets > 0) {
		ignorePackets--;
		CDebugHelper::print("Ignoring ntp");
		return;
	}
	
	float difference = targetTime - getGameTime();

	if(difference >= 5000) {
		CDebugHelper::print("Reset time to host time which is " + std::to_string(targetTime));
		offsetToLocalClock += difference;
		measuredOffset = 0;
		
		ignorePackets = 5;
	} else {
		measuredOffset = difference;
	}
}

