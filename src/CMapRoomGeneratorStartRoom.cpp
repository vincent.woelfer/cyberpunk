#include "CMapRoomGeneratorStartRoom.hpp"
#include "CMapFloorBlock.hpp"
#include "CUtilityDefines.hpp"

CMapRoomGeneratorStartRoom::CMapRoomGeneratorStartRoom() = default;

CMapRoom* CMapRoomGeneratorStartRoom::generateRoom(CRectangleArea roomArea, Doorway entrance, CDirection exitDirection, int exitSize) {
	std::vector<CMapBlock*> wallBlocks;
	std::vector<CMapBlock*> floorBlocks;
	
	entrance.direction = CDirection::PX;
	entrance.left = VECTOR2D_INVALID;
	entrance.right = VECTOR2D_INVALID;
	
	Doorway exit;
	exit.direction = exitDirection;
	
	std::vector<Vector2f> corners =  getExtendedCorners(roomArea);
	for(int i = 0; i < 4; i++) {
		int j = (i < 3) ? i + 1 : 0;
		Vector2f dir = CEngine::normalize(corners[j] - corners[i]);
		
		// Since corners are in clockwise order
		if(CDirection::fromVector(corners[j] - corners[i]).getLeft() == exit.direction) {
			float dist = CEngine::getDistance(corners[i], corners[j]);
			
			exit.left = corners[i] + dir * ((dist/2.f -exitSize/2.f) + 0.5f);
			exit.right = exit.left + dir * (float)(exitSize-1);
			
			wallBlocks.push_back(new CMapBlock(corners[i], exit.left - dir, 1.f, colorWallBase, colorWallAccent, false));
			wallBlocks.push_back(new CMapBlock(corners[j] - dir, exit.right + dir, 1.f, colorWallBase, colorWallAccent, false));
		} else {
			wallBlocks.push_back(new CMapBlock(corners[i], corners[j] - dir, 1.f, colorWallBase, colorWallAccent, false));
		}
	}
	
	floorBlocks.push_back(new CMapFloorBlock(roomArea, 0.f, 0.5f, colorFloorBase, colorFloorAccent));
	CRectangleArea exitFloorArea(exit.left, exit.right - exit.left + Vector2f(1,1));
	floorBlocks.push_back(new CMapFloorBlock(exitFloorArea, 0.f, 0.5f, colorFloorBase, colorFloorAccent));

	return new CMapRoom(entrance, exit, wallBlocks, floorBlocks);
}

std::vector<Vector2f> CMapRoomGeneratorStartRoom::getExtendedCorners(CRectangleArea area) {
	vector<Vector2f> corners;
	corners.push_back(area.offset + Vector2f(-1.f, -1.f));
	corners.push_back(area.offset + Vector2f(area.size.x, -1.f));
	corners.push_back(area.offset + area.size);
	corners.push_back(area.offset + Vector2f(-1.f, area.size.y));
	return corners;
}



