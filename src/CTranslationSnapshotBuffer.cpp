#include <iostream>
#include "CTranslationSnapshotBuffer.hpp"
#include "CEngine.hpp"
#include "CDebugHelper.hpp"
#include "CGameClock.hpp"
#include "CUtilityDefines.hpp"

void CTranslationSnapshotBuffer::insert(CTranslationSnapshot snapshot) {
	float newPing = CGameClock::getGameTime() - snapshot.gameTime;
	float pingMovingAverageSampleSize = 30.f;
	ping -= ping / pingMovingAverageSampleSize;
	ping += (newPing) / pingMovingAverageSampleSize;
	
	std::vector<CTranslationSnapshot>::iterator iter = buffer.begin();
	
	// Buffer begin (0) is newest entry
	while(iter != buffer.end()) {
		if(snapshot.gameTime > (*iter).gameTime) {
			buffer.insert(iter, snapshot);
			break;
		} else {
			iter++;
		}
	}

	if(iter == buffer.end()) {
		buffer.push_back(snapshot);
	}
	
	while(!buffer.empty() && buffer.back().gameTime <= CGameClock::getGameTime() - (maxEntryAge + ping)) {
		buffer.pop_back();
	}
}

CTranslationSnapshot CTranslationSnapshotBuffer::interpolateForTime(float gameTime) const {
	if(buffer.empty()) {
		CTranslationSnapshot invalid;
		invalid.gameTime = 0; 
		return invalid;
	}

	std::vector<CTranslationSnapshot>::const_iterator iter = buffer.begin();
	while(iter != buffer.end()) {
		// gameTime older than iter
		if(gameTime >= iter->gameTime) {
			if(iter == buffer.begin()) {
				return *iter;				
			} else {
				const CTranslationSnapshot& oldSnapshot = *(iter);
				const CTranslationSnapshot& newSnapshot = *(--iter);
				float alpha = CEngine::clamp((gameTime - oldSnapshot.gameTime) / (newSnapshot.gameTime - oldSnapshot.gameTime), 0.f, 1.f);
				
				CTranslationSnapshot interpolated;
				interpolated.gameTime = gameTime;
				interpolated.pos = CEngine::lerp(oldSnapshot.pos, newSnapshot.pos, alpha);
				interpolated.rot = CEngine::lerp(oldSnapshot.rot, newSnapshot.rot, alpha); // TODO lerp rot 360 cutoff correctly
				
				return interpolated;
			}			
		} else {
			iter++;
		}
	}

	return buffer.back();
}

float CTranslationSnapshotBuffer::getPing() const {
	return ping;
}
