#include "CNavRectangle.hpp"
#include "CRandom.hpp"
#include "CSceneManager.hpp"
#include "CUtilityDefines.hpp"

bool CNavRectangle::isInside(Vector2f pos) const {
	return !(pos.x < negPoint.x || pos.x > posPoint.x ||
			 pos.y < negPoint.y || pos.y > posPoint.y);
}

bool CNavRectangle::isInside(Vector3f pos) const {
	return isInside(Vector2f(pos.x, pos.y));
}

bool CNavRectangle::isInsideExclusiveBorder(Vector2f pos) const {
	return !(pos.x <= negPoint.x || pos.x >= posPoint.x ||
	         pos.y <= negPoint.y || pos.y >= posPoint.y);
}

bool CNavRectangle::isInsideExclusiveBorder(Vector3f pos) const {
	return isInsideExclusiveBorder(Vector2f(pos.x, pos.y));
}

vector<CNavPolygonBorder> CNavRectangle::getBorders() const {
	vector<CNavPolygonBorder> borders;
	borders.push_back(CNavPolygonBorder(negPoint, Vector2f(posPoint.x, negPoint.y)));
	borders.push_back(CNavPolygonBorder(Vector2f(posPoint.x, negPoint.y), posPoint));
	borders.push_back(CNavPolygonBorder(posPoint, Vector2f(negPoint.x, posPoint.y)));
	borders.push_back(CNavPolygonBorder(Vector2f(negPoint.x, posPoint.y), negPoint));

	return borders;
}

CNavPolygonExtend CNavRectangle::getExtend() const {
	CNavPolygonExtend extend;
	extend.minX = negPoint.x;
	extend.maxX = posPoint.x;
	extend.minY = negPoint.y;
	extend.maxY = posPoint.y;
	return extend;
}

float CNavRectangle::getAreaSize() const {
	return (posPoint.x - negPoint.x) * (posPoint.y - negPoint.y);
}

Vector2f CNavRectangle::getCenter() const {
	return negPoint + (posPoint-negPoint)/2.f;
}

Vector2f CNavRectangle::getRandomPointInside() const {
	return Vector2f(CRandom::getInRange(negPoint.x, posPoint.x), CRandom::getInRange(negPoint.y, posPoint.y));
}

void CNavRectangle::addDebugVisuals() {
	Vector3f pos = Vector3f((posPoint.x + negPoint.x) / 2.f, (posPoint.y + negPoint.y) / 2.f, 0.6f);
	Vector3f dim = Vector3f((posPoint.x - negPoint.x) / 2.f, (posPoint.y - negPoint.y) / 2.f, 0.075f);
	
	debugVisuals.push_back(new CMapBlock(pos, dim, Color(150, 0, 0, 30)));
	debugVisuals.back()->getCollisionContainer()->setCollisionChannel(CCollisionChannel());
	CSceneManager::safeAddObject(debugVisuals.back());
	
	for(CNavPolygonEdge* edge : adjacent) {
		for(CPolygonGatewayHole* hole : edge->holes) {
			pos = Vector3f(hole->inward.x, hole->inward.y, 0.6f);
			dim = Vector3f(0.05f, 0.05f, 0.075f);

			CDirection direction = CDirection::fromVector(hole->outward - hole->inward);
			pos += CEngine::toPlanar3D(direction.toVector() * 0.25f);
			
			if(direction == CDirection::PX || direction == CDirection::NX) {
				dim.x *= 4;
			} else {
				dim.y *= 4;
			}

			debugVisuals.push_back(new CMapBlock(pos, dim, Color(200, 0, 0, 50)));
			debugVisuals.back()->getCollisionContainer()->setCollisionChannel(CCollisionChannel());
			CSceneManager::safeAddObject(debugVisuals.back());
		}
	}
}

void CNavRectangle::removeDebugVisuals() {
	for(CObject* visual : debugVisuals) {
		CSceneManager::safeRemoveObject(visual);
	}
	debugVisuals.clear();
}

CObject* CNavRectangle::createLine(Vector2f p1, Vector2f p2) {
	Vector3f pos(((p1+p2)/2.f).x, ((p1+p2)/2.f).y, 0.6f);
	Vector3f dim(0.03, CEngine::getDistance(p1, p2)/2.f, 0.03f);
	Vector2f connectVec = p2 - p1;
	float rot = CEngine::getRotation(connectVec);

	CMapBlock* line = new CMapBlock(pos, dim, Color(255, 0, 0, 75), false);
	line->setRotation(rot);
	line->getCollisionContainer()->setCollisionChannel(CCollisionChannel());

	return line;
}

bool CNavRectangle::intersectsRectangleArea(CRectangleArea rectangleArea) const {
	CRectangleArea ownArea(negPoint, posPoint - negPoint);
	return ownArea.doesIntersect(rectangleArea);
}

bool CNavRectangle::intersectsRectangleAreaExclusiveBorders(CRectangleArea rectangleArea) const {
	CRectangleArea ownArea(negPoint, posPoint - negPoint);
	return ownArea.doesIntersectExclusiveBorder(rectangleArea);
}

// Deletion of this polygon is executed outside of this function
vector<CNavPolygon*> CNavRectangle::splitUp(CRectangleArea rectangleArea) {
	vector<CNavPolygon*> newPolygons;
	
	CNavPolygonExtend extend = getExtend();
	// If polygon is not completely in rectangle
	if(!(rectangleArea.isPointInside(Vector2f(extend.minX, extend.minY)) && rectangleArea.isPointInside(Vector2f(extend.maxX, extend.maxY)))) {
		vector<CRectangleArea> areas = splitIntoRectangles(rectangleArea);

		for(CRectangleArea area : areas) {
			newPolygons.push_back(new CNavRectangle(area));
		}

		for(vector<CNavPolygon*>::iterator it1 = newPolygons.begin(); it1 != newPolygons.end(); ++it1) {
			for(vector<CNavPolygon*>::iterator it2 = it1 + 1; it2 != newPolygons.end(); ++it2) {
				(*it1)->connectTo(*it2, true);
			}
			
			for(CNavPolygonEdge* edge : adjacent) {
				(*it1)->connectTo(edge->target, true);
			}
		}
	}

	if(!debugVisuals.empty()) {
		for(CNavPolygon* polygon : newPolygons) {
			polygon->addDebugVisuals();
		}
	}
	
	return newPolygons;
}

vector<CRectangleArea> CNavRectangle::splitIntoRectangles(const CRectangleArea& rectangleArea) const {
	vector<CRectangleArea> newAreas;
	vector<pair<Vector2f, Vector2f>> cuts = getCuts(rectangleArea);
	CRectangleArea polygonArea(negPoint, posPoint - negPoint);
	
	for(vector<pair<Vector2f, Vector2f>>::iterator it1 = cuts.begin(); it1 != cuts.end(); ++it1) {
		pair<Vector2f, Vector2f> cut = *it1;

		bool bothCutEndpointsOnBorders = polygonArea.isPointOnBorders(cut.first) && polygonArea.isPointOnBorders(cut.second);
		
		if(bothCutEndpointsOnBorders) {
			// Construct both areas and add the one not intersecting the given rectangle
			CRectangleArea rectangleArea1;
			CRectangleArea rectangleArea2;
			
			for(CNavPolygonBorder border : getBorders()) {
				if(!CEngine::areLineSegmentsParallelOrColinear(border.getLeft(), border.getRight(), cut.first, cut.second)) {
					rectangleArea1 = CRectangleArea(cut.first, cut.second, border.getLeft());
					rectangleArea2 = CRectangleArea(cut.first, cut.second, border.getRight());
					break;
				}
			}
			
			CRectangleArea* areaToAdd = rectangleArea2.doesIntersectExclusiveBorder(rectangleArea) ? &rectangleArea1 : &rectangleArea2;
			newAreas.push_back(*areaToAdd);
		}

		for(vector<pair<Vector2f, Vector2f>>::iterator it2 = it1 + 1; it2 != cuts.end(); ++it2) {
			pair<Vector2f, Vector2f> cut2 = *it2;

			Vector2f intersection = CEngine::getLineSegmentIntersection(cut.first, cut.second, cut2.first, cut2.second);
			if(intersection != VECTOR2D_INVALID) {
				// Find two endpoints which are on polygon border
				Vector2f p1 = !polygonArea.isPointInsideExclusiveBorder(cut.first) ? cut.first : cut.second;
				Vector2f p2 = !polygonArea.isPointInsideExclusiveBorder(cut2.first) ? cut2.first : cut2.second;
				
				// Check if one cut has both ends on polygon border
				if(bothCutEndpointsOnBorders) {
					CRectangleArea testArea(p1, p2, intersection);
					if(testArea.doesIntersectExclusiveBorder(rectangleArea)) {
						p1 = (p1 == cut.first) ? cut.second : cut.first;
					}
				} else {
					if(polygonArea.isPointOnBorders(cut2.first) && polygonArea.isPointOnBorders(cut2.second)) {
						CRectangleArea testArea(p1, p2, intersection);
						if(testArea.doesIntersectExclusiveBorder(rectangleArea)) {
							p2 = (p2 == cut2.first) ? cut2.second : cut2.first;
						}	
					}	
				}

				newAreas.push_back(CRectangleArea(p1, p2, intersection));
			}
		}		
	}

	return newAreas;
}

vector<pair<Vector2f, Vector2f>> CNavRectangle::getCuts(const CRectangleArea& rectangleArea) const {
	vector<pair<Vector2f, Vector2f>> cuts;
	vector<Vector2f> rectangleCorners = rectangleArea.getCorners();
	Vector2f rectangleCenter = rectangleArea.getCenter();
	CRectangleArea polygonArea(negPoint, posPoint - negPoint);

	CDirection preferredDirection = CDirection::PX;
	unsigned int insideCorners = 0;
	for(Vector2f corner : rectangleCorners) {
		if(isInsideExclusiveBorder(corner)) {
			insideCorners++;
			
			// Find direction to cut in
			vector<CDirection> outwardDirections = CEngine::getOutwardDirectionsFromCorner(corner, rectangleCenter);
			CDirection outwardDirection;

			outwardDirection = (preferredDirection.isParallel(outwardDirections.at(0))) ? outwardDirections.at(0) : outwardDirections.at(1);
			preferredDirection = outwardDirection.getRight();

			// Find cut endpoint
			Vector2f start;
			for(Vector2f nextCorner : rectangleCorners) {
				if(CEngine::normalize(corner - nextCorner) == outwardDirection.toVector()) {
					if(polygonArea.isPointInside(nextCorner)) {
						start = nextCorner;	
					} else {
						for(CNavPolygonBorder border : getBorders()) {
							Vector2f intersection = CEngine::getLineSegmentIntersection(corner, nextCorner, border.getLeft(), border.getRight());
							
							if(intersection != VECTOR2D_INVALID) {
								start = intersection;
								break;
							}
						}	
					}					
					break;
				}
			}

			Vector2f end;
			for(CNavPolygonBorder border : getBorders()) {
				Vector2f intersection = CEngine::getLineSegmentIntersection(corner, corner + outwardDirection.toVector() * 9999.f, border.getLeft(),
				                                                            border.getRight());

				if(intersection != VECTOR2D_INVALID) {
					end = intersection;
					break;
				}
			}

			// Check if this is an extension of an already existing cut
			bool isExtension = false;
			for(pair<Vector2f, Vector2f>& existingCut : cuts) {
				if((corner == existingCut.first || corner == existingCut.second) &&
				   outwardDirection.isParallel(CDirection::fromVector(existingCut.second - existingCut.first))) {
					if(existingCut.first == corner) {
						existingCut.first = end;
					} else {
						existingCut.second = end;
					}
					isExtension = true;
					break;
				}
			}

			if(!isExtension) {
				cuts.push_back(make_pair(start, end));
			}
		}
	}
	
	// add additional cuts on rectangle edges not cut yet
	unsigned int neededCuts = (insideCorners < 4) ? insideCorners+1 : 4;
	if(cuts.size() < neededCuts) {
		for(unsigned int i = 0; i < 4; i++) {
			unsigned int j = (i + 1 > 3) ? 0 : i+1;

			// If is inside and not part of existing cut
			if(polygonArea.isPointInsideExclusiveBorder(rectangleCorners.at(i)) || polygonArea.isPointInsideExclusiveBorder(rectangleCorners.at(j))) {
				bool isUnique = true;
				for(pair<Vector2f, Vector2f>& existingCut : cuts) {
					if(CEngine::areLineSegmentsParallelOrColinear(existingCut.first, existingCut.second,
					                                              rectangleCorners.at(i), rectangleCorners.at(j)) &&
					   CEngine::doLineSegmentsIntersect(existingCut.first, existingCut.second, rectangleCorners.at(i), rectangleCorners.at(j))) {
						isUnique = false;
						break;
					}
				}

				if(isUnique) {
					// Find cut which lies inside polygon
					Vector2f p1;
					Vector2f p2;
					
					if(!polygonArea.isPointInside(rectangleCorners.at(i)) || !polygonArea.isPointInside(rectangleCorners.at(j))) {
						p1 = polygonArea.isPointInsideExclusiveBorder(rectangleCorners.at(i)) ? rectangleCorners.at(i) : rectangleCorners.at(j);
						
						for(CNavPolygonBorder border : getBorders()) {
							Vector2f intersection = CEngine::getLineSegmentIntersection(rectangleCorners.at(i), rectangleCorners.at(j), border.getLeft(), border.getRight());

							if(intersection != VECTOR2D_INVALID) {
								p2 = intersection;
								break;
							}
						}
					} else {
						p1 = rectangleCorners.at(i);
						p2 = rectangleCorners.at(j);
					}
					
					cuts.push_back(make_pair(p1, p2));
					if(cuts.size() == neededCuts) {
						break;
					}
				}
			}
		}
	}

	// If polygon lies half within rectangle or polygon and rect form a cross (insideCorners == 0)
	if(cuts.empty()) {
		for(CNavPolygonBorder border : getBorders()) {
			for(unsigned int i = 0; i < 4; i++) {
				unsigned int j = (i + 1 > 3) ? 0 : i+1;

				Vector2f startIntersection = CEngine::getLineSegmentIntersection(border.getLeft(), border.getRight(),
				                                                                 rectangleCorners.at(i), rectangleCorners.at(j));
				
				if(startIntersection != VECTOR2D_INVALID &&
						!CEngine::areLineSegmentsParallelOrColinear(border.getLeft(), border.getRight(), rectangleCorners.at(i), rectangleCorners.at(j)) &&
						border.getLeft() != rectangleCorners.at(i) && border.getLeft() != rectangleCorners.at(j) &&
						border.getRight() != rectangleCorners.at(i) && border.getRight() != rectangleCorners.at(j)) {
					
					Vector2f start = startIntersection;

					Vector2f end;
					for(CNavPolygonBorder innerBorder : getBorders()) {
						if(innerBorder != border) {
							Vector2f endIntersection = CEngine::getLineSegmentIntersection(innerBorder.getLeft(), innerBorder.getRight(), rectangleCorners.at(i), rectangleCorners.at(j));
							if(endIntersection != VECTOR2D_INVALID) {
								end = endIntersection;
								break;
							}
						}
					}
					cuts.push_back(make_pair(start, end));
				}				
			}
			if(!cuts.empty()) {
				break;
			}
		}		
	}
	
	return cuts;
}

CNavRectangle::~CNavRectangle() {
	removeDebugVisuals();
}
