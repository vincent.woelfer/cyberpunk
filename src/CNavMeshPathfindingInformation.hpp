#ifndef CYBERPUNK_CNAVMESHPATHFINDINGINFORMATION_HPP
#define CYBERPUNK_CNAVMESHPATHFINDINGINFORMATION_HPP

#include <SFML/System.hpp>
#include <cfloat>
#include <queue>
#include "CNavPolygon.hpp"
#include "CNavPath.hpp"

using namespace sf;

/* Evil c++ typdef mess. Don't touch! */
class CNavMeshPathfindingInformation;
typedef map<Vector2f, CNavMeshPathfindingInformation, CEngine::CompareVector> PathfindingInformationMap;

struct ComparePathfindingInformation;
typedef priority_queue<CNavMeshPathfindingInformation*, vector<CNavMeshPathfindingInformation*>, ComparePathfindingInformation> PriorityPathfindingInformationSet;


/* Main CNavMeshPathfindingInformation class */
class CNavMeshPathfindingInformation {
public:	
	Vector2f pos;
	CNavPolygon* polygon;
	float f;
	float g;
	CNavMeshPathfindingInformation* prev;

	CNavMeshPathfindingInformation(Vector2f pos, CNavPolygon* polygon) :
			pos(pos),
			polygon(polygon),
			f(0),
			g(FLT_MAX),
			prev(nullptr) {}
	
	
	// Explore from prevInfo, add to openSet if new shortest path was found
	void explore(PriorityPathfindingInformationSet* openSet, CNavMeshPathfindingInformation* prevInfo, Vector2f targetPos, bool addToSet);
	
	static CNavMeshPathfindingInformation* getForPoint(PathfindingInformationMap* exploredSet, Vector2f pos, CNavPolygon* polygon);
};

struct ComparePathfindingInformation {
	bool operator()(const CNavMeshPathfindingInformation* lhs, const CNavMeshPathfindingInformation* rhs) const {
		return (lhs->g + lhs->f) > (rhs->g + rhs->f);
	}
};

#endif //CYBERPUNK_CNAVMESHPATHFINDINGINFORMATION_HPP