#include "CSkillForceSweep.hpp"
#include "CInputManager.hpp"
#include "CProjectile.hpp"
#include "CSceneManager.hpp"
#include "CProjectileSweepingWall.hpp"
#include "CRpcHandler.hpp"

void CSkillForceSweep::onButtonPress() {
	Vector2f dir = CInputManager::getDirToMouseInScene(owner->getPosition());
    fire(dir);
}

void CSkillForceSweep::fire(Vector2f dir) {
	createWall(owner, owner->getPosition(), dir);
	
	CRpcHandler::sendPlayerSkillForceSweep(owner, dir, owner->getPosition());
}

void CSkillForceSweep::fireRPC(float gameTime, Vector3f originalPos, Vector2f dir) {
	createWall(owner, owner->getPosition(), dir);
}

void CSkillForceSweep::fireFallback(Vector3f pos, Vector2f dir) {
	createWall(nullptr, pos, dir);
}

void CSkillForceSweep::createWall(CCharacter *initiator, Vector3f pos, Vector2f dir) {
	CSceneManager::safeAddObject(new CProjectileSweepingWall(initiator, pos, dir)); //TODO owner
}
