#include <iostream>
#include "CIconTextBox.hpp"
#include "CGuiManager.hpp"
#include "CGuiTextElement.hpp"
#include "CIcon.hpp"


CIconTextBox::CIconTextBox(Vector2f screenPos, string iconSource, string text, Color color, float iconSize, float textSize) :
	screenPos(screenPos),
	iconSource(iconSource),
	text(text),
	color(color),
	iconSize(iconSize),
	textSize(textSize) {
	
	icon = new CIcon(iconSource, screenPos, Vector2f(iconSize, iconSize));
	icon->setColor(color);
	icon->setKeepAfterFadeout(true);
	CGuiManager::addGuiElement(icon);

	//textElement = new CGuiTextElement(text, (unsigned int) ((int) textSize * CGuiManager::getGuiFontSizeFactor()), color, screenPos + Vector2f(1.5f*iconSize, 0));
	textElement = new CGuiTextElement(text, static_cast<unsigned int>(textSize * CGuiManager::getGuiFontSizeFactor()), color, screenPos);
	textElement->setPosition(screenPos + Vector2f(textElement->getTextBounds().x / 2.f + iconSize / 2.f + iconSize * textYOffsetInRelationToIconSize, 0));
	CGuiManager::addGuiElement(textElement);

}

CIconTextBox::~CIconTextBox() {
	CGuiManager::removeGuiElement(icon);
	CGuiManager::removeGuiElement(textElement);
	delete icon;
	delete textElement;
}

void CIconTextBox::setIsVisibile(bool visible) {
	icon->setIsVisible(visible);
	textElement->setIsVisible(visible);
}

void CIconTextBox::changeIcon(string iconSource) {
	this->iconSource = iconSource;

	CGuiManager::removeGuiElement(icon);
	if(icon != nullptr) {
		delete icon;
	}
	
	icon = new CIcon(iconSource, screenPos, Vector2f(iconSize, iconSize));
	icon->setColor(color);
	icon->setKeepAfterFadeout(true);
	CGuiManager::addGuiElement(icon);
}

void CIconTextBox::updateText(string text) {
	textElement->setText(text);
	textElement->setPosition(screenPos + Vector2f(textElement->getTextBounds().x / 2.f + iconSize / 2.f + iconSize * textYOffsetInRelationToIconSize, 0));
	CGuiManager::removeGuiElement(textElement);
	CGuiManager::addGuiElement(textElement);
}

void CIconTextBox::setColor(Color color) {
	this->color = color;
	icon->setColor(color);
	textElement->setColor(color);
}

void CIconTextBox::setComponentLifetime(float lifetime) {
	icon->setLifetime(lifetime);
	textElement->setLifetime(lifetime);
}
