#include "CGuiTextElement.hpp"
#include "CGuiManager.hpp"
#include "CEngine.hpp"

CGuiTextElement::CGuiTextElement(std::string text, unsigned int size, sf::Color color, sf::Vector2f pos, float lifetime) :
		text(text, CGuiManager::getFontDisplay(), size),
		pos(pos),
		lifetime(lifetime) {
		
	this->text.setOrigin(Vector2f(this->text.getLocalBounds().width, this->text.getLocalBounds().height) * 0.5f);
	this->text.setPosition(pos);
	this->text.setColor(color);
	//this->text.setStyle(sf::Text::Style::Bold);
}

void CGuiTextElement::draw(sf::RenderTarget *renderTarget) {
	if(isVisible) {
		renderTarget->draw(text);
	}
}

void CGuiTextElement::tick(float frameTime) {
	if(isLifetimeLimited && clockLifetime.getTimeMs() >= lifetime) {
		CGuiManager::removeGuiElement(this);
		return;
	}

	if(isFading && isLifetimeLimited && clockLifetime.hasPassed(fadeAfter * lifetime)) {
		float alpha = CEngine::invertAlpha(CEngine::clampAlpha((clockLifetime.getTimeMs() - lifetime * fadeAfter) / (lifetime - lifetime * fadeAfter)));

		sf::Color color = text.getColor();
		color.a = (Uint8) CEngine::lerp(0, 255, alpha);
		text.setColor(color);
	}
}

void CGuiTextElement::setColor(sf::Color color) {
	this->text.setColor(color);
}

void CGuiTextElement::setText(std::string text_str) {
	text.setString(text_str);
	clockLifetime.restart();

	sf::Color color = text.getColor();
	color.a = 255;
	text.setColor(color);
}

void CGuiTextElement::setLifetime(float lifeTime) {
	this->lifetime = lifeTime;
	isLifetimeLimited = true;
	isFading = true;
	clockLifetime.restart();
}

Vector2f CGuiTextElement::getTextBounds() {
	return Vector2f(text.getLocalBounds().width, text.getLocalBounds().height);
}

void CGuiTextElement::setPosition(Vector2f position) {
	text.setPosition(position);
	pos = position;
}
