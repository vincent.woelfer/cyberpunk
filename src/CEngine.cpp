#include "CEngine.hpp"
#include "CSceneManager.hpp"
#include "CObject.hpp"

#include "tinyc2.h"
#include "CUtilityDefines.hpp"

#define TINYC2_IMPL
extern "C" {
	#include "tinyc2.h"
}
#undef TINYC2_IMPL

using namespace sf;

float CEngine::tile_size = 64.f;
float CEngine::angle = 0.5f;
float CEngine::light_angle = 0;

long CEngine::uniqueId = 0;

Vector2f CEngine::toIso(const Vector3f& v) {
    return Vector2f(tile_size * (v.x - v.y), tile_size * ((v.x * angle) + (v.y * angle) - v.z));
}

float CEngine::getDepth(const Vector3f& v) {
    return v.x + v.y + v.z;
}

Vector2f CEngine::normalize(const Vector2f& v) {
	if(v.x != 0 || v.y != 0) {
		float length = getLength(v);
		
		float x = v.x/length;
		x = (abs(x) > EPS) ? x : 0;
		float y = v.y/length;
		y = (abs(y) > EPS) ? y : 0;
		
		return Vector2f(x, y);
	}
	return Vector2f(0, 0);
}

Vector3f CEngine::normalize(const Vector3f& v) {
	if(v.x != 0 || v.y != 0 || v.z != 0) {
		float length = getLength(v);
		return Vector3f(v.x/length, v.y/length, v.z/length);
	}
	return Vector3f(0, 0, 0);
}

float CEngine::getLength(const Vector2f& v) {
    return sqrt(v.x * v.x + v.y * v.y);
}
float CEngine::getLength(const Vector3f& v) {
    return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

float CEngine::getDistance(Vector2f v1, Vector2f v2) {
	return getLength(v1 - v2);
}
float CEngine::getDistance(Vector3f v1, Vector3f v2) {
    return getLength(v1 - v2);
}

Vector3f CEngine::toPlanar3D(const Vector2f& v) {
    return Vector3f(v.x, v.y, 0);
}

Vector3f CEngine::to3DOnPlane(const Vector2f& pos, float z) {
	Vector2f p = pos;
	p = p / (float)tile_size;

	float xt, yt;
	xt = p.y + p.x/2;
	yt = p.y - p.x/2;

	return Vector3f(xt + z, yt + z, z);
}

Vector2f CEngine::to2D(const Vector3f& v) {
	return Vector2f(v.x, v.y);
}
Vector2f CEngine::rotateZ(const Vector2f& v, float angle) {
	angle *= TO_RAD;

	Vector2f r;
	r.x = v.x * cos(angle) - v.y * sin(angle);
	r.y = v.x * sin(angle) + v.y * cos(angle);

	return r;
}

Vector3f CEngine::rotateZ(const Vector3f& v, float angle) {
	angle *= TO_RAD;

	Vector3f r;
	r.x = v.x * cos(angle) - v.y * sin(angle);
	r.y = v.x * sin(angle) + v.y * cos(angle);
	r.z = v.z;

	return r;
}

float CEngine::getLightFactor(float angle) {
	// Over 90 -> Dark(0)
	// 0 -> direct light(0)

	angle = abs(angle);

	float factor;
	if(angle > 90) {
		factor = 0;
	} else {
		factor = cos(angle * TO_RAD);
	}

	return factor;
}

float CEngine::clamp(float input, float min, float max) {
	return fmin(fmax(min, input), max);
}

float CEngine::clampAlpha(float input) {
	return fmin(fmax(0.f, input), 1.f);
}

Color CEngine::getColorFromLightFactor(Color baseColor, float lightFactor) {
	lightFactor = clamp(lightFactor, 0, 1);
	lightFactor = 0.5f + lightFactor * 0.7f;

	return scaleColor(baseColor, lightFactor);	
}

Color CEngine::scaleColor(Color baseColor, float factor) {
	Color color;
	color.r = (Uint8) clamp(baseColor.r * factor, 0, 255);
	color.g = (Uint8) clamp(baseColor.g * factor, 0, 255);
	color.b = (Uint8) clamp(baseColor.b * factor, 0, 255);
	color.a = baseColor.a;

	return color;
}

bool CEngine::doesIntersect(const CBoundingBox& b1, const CBoundingBox& b2) {
	// Check maxDistSquared
	if(getDistanceSquared(b1.getPosition(), b2.getPosition()) > getDimensionSquared(b1.getDimension(), b2.getDimension())) {
		return false;
	}
	
	// Check if separable on z axis
	if((b1.getPosition().z - b1.getDimension().z >= b2.getPosition().z + b2.getDimension().z) ||
	   (b2.getPosition().z - b2.getDimension().z >= b1.getPosition().z + b1.getDimension().z)) {
		return false;
	}

	return (bool) c2PolytoPoly(b1.getPoly(), nullptr, b2.getPoly(), nullptr);
}

bool CEngine::isInFrontOf(const CGraphicBox& b1, const CGraphicBox& b2) {
	Vector3f pos1 = b1.getPosition();
	Vector3f pos2 = b2.getPosition();

	Vector3f dim1 = b1.getDimension();
	Vector3f dim2 = b2.getDimension();

	if(pos1.z - dim1.z >= pos2.z + dim2.z) { return true; }
	else if(pos2.z - dim2.z >= pos1.z + dim1.z) { return false; }

	if(pos1.x - dim1.x >= pos2.x + dim2.x) { return true; }
	else if(pos2.x - dim2.x >= pos1.x + dim1.x) { return false; }

	if(pos1.y - dim1.y >= pos2.y + dim2.y) { return true; }
	else if(pos2.y - dim2.y >= pos1.y + dim1.y) { return false; }

	return b1.getDepth() >= b2.getDepth();
}

bool CEngine::isInFrontOf(const CGraphicBox& b1, const CGraphicArea& a1) {
	return b1.getPosition().z >= a1.getPosition().z;
}

bool CEngine::isInFrontOf(const CGraphicArea& a1, const CGraphicBox& b1) {
	return a1.getPosition().z >= b1.getPosition().z;
}

bool CEngine::isInFrontOf(const CGraphicArea& a1, const CGraphicArea& a2) {
	if(a1.getPosition().z != a2.getPosition().z) {
		return a1.getPosition().z > a2.getPosition().z;
	} else {
		return a1.getRadius() <= a2.getRadius();
	}
}

float CEngine::getRotation(Vector2f& v) {
	if(v.x != 0 || v.y != 0) {
		float d = (atan2(v.y, v.x) * TO_DEG - 90);

		if(d < 0) {
			d+= 360;
		}
		return d;
	}
	return 0;
}

bool CEngine::doesOverlap(const CGraphicContainer& c1, const CGraphicContainer& c2) {
	return !((c1.getRight() < c2.getLeft()) || (c1.getLeft() > c2.getRight())
	         || (c1.getBot() < c2.getTop()) || (c1.getTop() > c2.getBot()));
}

float CEngine::getAngleDifference(Vector2f& v1, Vector2f& v2) {
	float angle = getRotation(v2) - getRotation(v1);

	if(angle > 180) {
		angle = angle - 360;
	} else if( angle < -180) {
		angle = angle + 360;
	}

	return angle;
}

float CEngine::getAngleDifference(float a1, float a2) {
	float angle = a2 - a1;

	if(angle > 180) {
		angle = angle - 360;
	} else if(angle < -180){
		angle = angle + 360;
	}

	return angle;
}

float CEngine::rotate(float rot, float angle) {
	rot += angle;
	if(rot > 360) {
		rot -= 360;
	} else if(rot < 0) {
		rot += 360;
	}

	return rot;
}

c2v CEngine::toC2v(Vector2f& v) {
	return c2V(v.x, v.y);
}

c2v CEngine::toC2v(Vector2f v) {
	return c2V(v.x, v.y);
}

c2v CEngine::toC2v(Vector3f& v) {
	return c2V(v.x, v.y);
}

c2v CEngine::toC2v(Vector3f v) {
	return c2V(v.x, v.y);
}

Vector2f CEngine::toSFVec(c2v v) {
	return Vector2f(v.x, v.y);
}

Vector2f CEngine::getUnitInDirection(float angle) {
	return CEngine::rotateZ(Vector2f(0.f, 1.f), angle);
}

Vector3f CEngine::getUnitInDirection3D(float angle) {
	return CEngine::rotateZ(Vector3f(0.f, 1.f, 0.f), angle);
}

float CEngine::getDistanceSquared(Vector3f v1, Vector3f v2) {
	float x = v1.x - v2.x;
	float y = v1.y - v2.y;
	return x*x + y*y;
}

float CEngine::getDimensionSquared(Vector3f v1, Vector3f v2) {
	return (v1.x + v2.x) * (v1.x + v2.x) + (v1.y + v2.y) * (v1.y + v2.y);
}

float CEngine::lerp(float start, float end, float alpha) {
	return start + (end-start) * alpha;
}

Vector3f CEngine::lerp(const Vector3f& start, const Vector3f& end, float alpha) {
	return start + (end - start) * alpha;
}

Color CEngine::lerp(const Color& start, const Color& end, float alpha) {
	return Color((Uint8) lerp(start.r, end.r, alpha), (Uint8) lerp(start.g, end.g, alpha), (Uint8) lerp(start.b, end.b, alpha),
	             (Uint8) lerp(start.a, end.a, alpha));
}

Vector2f CEngine::roundToInt(Vector2f v) {
	return Vector2f((int) v.x, (int) v.y);
}

float CEngine::getDotProduct(Vector2f& v1, Vector2f& v2) {
	return v1.x*v2.x + v1.y*v2.y;
}

float CEngine::getDotProduct(Vector3f& v1, Vector3f& v2) {
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}

bool CEngine::areInSameDirection(Vector2f v1, Vector2f v2) {
	return fabs(getDotProduct(v1, v2) - (getLength(v1) * getLength(v2))) < EPS;
}

bool CEngine::areInOppositeDirection(Vector2f v1, Vector2f v2) {
	return fabs(getDotProduct(v1, v2) + (getLength(v1) * getLength(v2))) < EPS;
}

bool CEngine::isFurtherInDirection(Vector2f v, Vector2f vReference, Vector2f dir) {
	Vector2f vOtherToV = v - vReference;
	return getDotProduct(vOtherToV, dir) > 0;
}

bool CEngine::areLineSegmentsParallelOrColinear(Vector2f l1Start, Vector2f l1End, Vector2f l2Start, Vector2f l2End) {
	Vector2f v1 = l1End - l1Start;
	Vector2f v2 = l2End - l2Start;
	return abs(v1.x*v2.y - v1.y*v2.x) < EPS;
}

bool CEngine::doLineSegmentsIntersect(Vector2f l1Start, Vector2f l1End, Vector2f l2Start, Vector2f l2End) {
	float s1_x = l1End.x - l1Start.x;
	float s1_y = l1End.y - l1Start.y;
	float s2_x = l2End.x - l2Start.x;
	float s2_y = l2End.y - l2Start.y;
	
	float det = -s2_x * s1_y + s1_x * s2_y;

	// Parallel or colinear
	if(abs(det) < EPS) {
		// Check if parallel
		if(!areLineSegmentsParallelOrColinear(l1Start, l1End, l1Start, l2Start)) {
			return false;
		}
		
		Vector2f dir = l1End - l1Start;
		
		float dotDirDir = getDotProduct(dir, dir);
		float t1 = 0.f;
		float t2 = 1.f;
		Vector2f tol2Start = l2Start - l1Start;
		Vector2f tol2End = l2End - l1Start;
		float t3 = getDotProduct(tol2Start, dir) / dotDirDir;
		float t4 = getDotProduct(tol2End, dir) / dotDirDir;

		if(t4 < t3) {
			float tmp = t3;
			t3 = t4;
			t4 = tmp;
		}

		return (t2 - t3 >= 0 && t4 - t1 >= 0);		
	}	

	float s, t;
	s = (-s1_y * (l1Start.x - l2Start.x) + s1_x * (l1Start.y - l2Start.y)) / det;
	t = ( s2_x * (l1Start.y - l2Start.y) - s2_y * (l1Start.x - l2Start.x)) / det;

	return s >= 0 && s <= 1 && t >= 0 && t <= 1;
}

Vector2f CEngine::getLineSegmentIntersection(Vector2f l1Start, Vector2f l1End, Vector2f l2Start, Vector2f l2End) {
	float s1_x = l1End.x - l1Start.x;
	float s1_y = l1End.y - l1Start.y;
	float s2_x = l2End.x - l2Start.x;
	float s2_y = l2End.y - l2Start.y;

	float det = -s2_x * s1_y + s1_x * s2_y;

	// Parallel
	if(abs(det) < EPS) {
		// Check if parallel
		if(!areLineSegmentsParallelOrColinear(l1Start, l1End, l1Start, l2Start)) {
			return VECTOR2D_INVALID;
		}
		
		Vector2f dir = l1End - l1Start;
		
		float dotDirDir = getDotProduct(dir, dir);
		float t1 = 0.f;
		float t2 = 1.f;
		Vector2f tol2Start = l2Start - l1Start;
		Vector2f tol2End = l2End - l1Start;
		float t3 = getDotProduct(tol2Start, dir) / dotDirDir;
		float t4 = getDotProduct(tol2End, dir) / dotDirDir;

		if(t4 < t3) {
			float tmp = t3;
			t3 = t4;
			t4 = tmp;
		}

		if(t2 - t3 >= 0 && t4 - t1 >= 0) {
			if(t3 >= t1 && t3 <= t2) {
				return l2Start;
			} else if(t4 >= t1 && t4 <= t2) {
				return l2End;
			} else {
				return l1Start;
			}
		} else {
			return VECTOR2D_INVALID;
		}
	}

	float s, t;
	s = (-s1_y * (l1Start.x - l2Start.x) + s1_x * (l1Start.y - l2Start.y)) / det;
	t = ( s2_x * (l1Start.y - l2Start.y) - s2_y * (l1Start.x - l2Start.x)) / det;

	if(s >= 0 && s <= 1 && t >= 0 && t <= 1) {
		// Collision detected         
		return Vector2f(l1Start.x + (t * s1_x), l1Start.y + (t * s1_y));
	}
	//No collision
	return VECTOR2D_INVALID;
}

float CEngine::getAngleToTarget(CObject* object, Vector3f target) {
	Vector2f diff = CEngine::to2D(target -  object->getPosition());
	return CEngine::getAngleDifference(object->getRotation(), CEngine::getRotation(diff));
}

float CEngine::getAngleToTarget(CObject* object, Vector2f target) {
	Vector2f diff = target - CEngine::to2D(object->getPosition());
	return CEngine::getAngleDifference(object->getRotation(), CEngine::getRotation(diff));
}

vector<CDirection> CEngine::getOutwardDirectionsFromCorner(Vector2f corner, Vector2f center) {
	vector<CDirection> directions;
	
	for(CDirection dir : CDirection::allDirections()) {
		if(CEngine::isFurtherInDirection(corner, center, dir.toVector())) {
			directions.push_back(dir);
		}
	}

	return directions;
}

float CEngine::invertAlpha(float alpha) {
	return (alpha * -1.f) + 1.f;
}

long CEngine::getUniqueId() {
	return uniqueId++;
}

Vector2f CEngine::rotateZLeft90(const Vector2f& v) {
	return Vector2f(v.y, -v.x);
}

Vector2f CEngine::rotateZRight90(const Vector2f& v) {
	return Vector2f(-v.y, v.x);
}




