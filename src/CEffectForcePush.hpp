#ifndef CYBERPUNK_CEFFECTFORCEPUSH_HPP
#define CYBERPUNK_CEFFECTFORCEPUSH_HPP

#include "CEffect.hpp"
#include "CCharacter.hpp"

class CEffectForcePush : public CEffect {
private:
	float pushStrength;
	Vector2f direction;

public:
	CEffectForcePush(CCharacter* initiator, float pushStrength, Vector2f direction, float lifetime) :
		CEffect(initiator, lifetime),
	    pushStrength(pushStrength),
	    direction(CEngine::normalize(direction)) {}

	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate);

};


#endif //CYBERPUNK_CEFFECTFORCEPUSH_HPP
