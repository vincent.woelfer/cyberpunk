#include "CEnemy.hpp"
#include "CAIController.hpp"
#include "CRpcHandler.hpp"

void CEnemy::tickAuthority(float frameTime) {
	if(isActive) {
		aiController->tick(frameTime);
	}
}

void CEnemy::tick(float frameTime) {
	if(isActive) {
		CCharacter::tick(frameTime);
		
		healthBar.setPosition(pos);
		healthBar.update(frameTime);
	}
}

bool CEnemy::move(Vector2f desiredMovement, bool restrictMovement = false) {
	return CCharacter::move(desiredMovement * movementSpeed * effectManager.getMovementSpeedModifier(), restrictMovement, false);
}

void CEnemy::rotate(float angle) {
	if(!effectManager.hasStun()) {
		CCharacter::rotate(angle);
	}
}

void CEnemy::setHealth(float health) {
	this->health = CEngine::clamp(health, 0, maxHealth);
	healthBar.setCurrentHealth(this->health);

	if(this->health <= 0) {
		die();
	}
}

void CEnemy::reduceHealth(float damage) {
	health = CEngine::clamp(health - damage, 0, maxHealth);

	// Kill if too low
	if(health/maxHealth <= 0.05 && health/damage <= 0.2) {
		health = 0;
	}

	healthBar.setCurrentHealth(health);

	if(health <= 0) {
		die();
	}
}

void CEnemy::healHealth(float heal) {
	health = CEngine::clamp(health + heal, 0, maxHealth);
	healthBar.setCurrentHealth(health);
}

void CEnemy::die() {
	CRpcHandler::sendCharacterDie(this);
	
	CCharacter::die();
}

CEnemy::~CEnemy() {
	if(isLocal) {
		delete aiController;
	}
}

void CEnemy::setActive(bool active) {
	isActive = active;
}

CTurningEnemyMovementProfile CEnemy::getTurningEnemyMovementProfile() {
	return turningMovementProfile;
}

bool CEnemy::canBePushedByBullet() {
	return true;
}

void CEnemy::setNotLocal(CObjectId id) {
	CObject::setNotLocal(id);
	delete aiController;
}

