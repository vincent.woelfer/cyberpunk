#ifndef CYBERPUNK_CFLATCIRCLE_HPP
#define CYBERPUNK_CFLATCIRCLE_HPP

#include "CObject.hpp"
#include "CGraphicArea.hpp"
#include "CClock.hpp"

/*
 * The world is a flat circle [True Detective]
 */

class CFlatCircle : public CObject {
private:
	CGraphicArea graphicArea;
	CClock lifeClock;
	float radius;
	float lifetime;

public:

	CFlatCircle(Vector3f pos, float radius, Color colorBase, Color colorAccent, float lifetime) :
			CObject(pos, 0),
			graphicArea(Vector3f(pos.x, pos.y, 0.55f), radius, 32, colorBase, colorAccent),
			radius(radius),
			lifetime(lifetime) {}

	void tick(float frameTime);


	virtual CCollisionContainer* getCollisionContainer();
	virtual CGraphicContainer* getGraphicContainer();

};

#endif //CYBERPUNK_CFLATCIRCLE_HPP
