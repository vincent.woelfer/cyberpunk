#ifndef CYBERPUNK_CAICONTROLLERSPECTER_HPP
#define CYBERPUNK_CAICONTROLLERSPECTER_HPP


#include <SFML/System.hpp>
#include "CAIController.hpp"


using namespace sf;

class CEnemySpecter;

class CAIControllerSpecter : public CAIController {
private:
	CEnemySpecter* host;

	Vector2f wanderTarget = Vector2f(-1, -1);
	Vector2f lastPosition;

	void pickNewWanderTarget();

public:
	CAIControllerSpecter(CEnemySpecter* host) :
		host(host) {}

	void tick(float frameTime);

};


#endif //CYBERPUNK_CAICONTROLLERSPECTER_HPP
