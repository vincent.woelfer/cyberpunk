#ifndef CYBERPUNK_CEFFECTMARK_HPP
#define CYBERPUNK_CEFFECTMARK_HPP

#include <unordered_set>
#include "CEffect.hpp"
#include "CTextureManager.hpp"

class CCharacter;

class CEffectMark : public CEffect {
public:
	enum class EffectMarkType{Lightning};
	
private:
	class CEffectMarkEntry;
	std::map<EffectMarkType, CEffectMarkEntry> effectMarkEntries;

	int maxMarkCount = 3;

public:
	CEffectMark(CCharacter* initiator, EffectMarkType markType, int markCount = 1);
	
	virtual bool tick(float frameTime, CEffectAggregate& effectAggregate);

	bool canMergeEffect(CEffect* otherEffect) override;
	
	virtual void onStart();
	virtual void onEnd();

	virtual std::vector<const sf::Texture*> getVisuals();
	virtual int getVisualPriority();
	
	void removeEffectMark(EffectMarkType markType, int markCount);

private:
	void addEffectMarkEntry(CEffectMarkEntry newEntry);
	
	static std::string getMarkTypeName(EffectMarkType markType);
	
	/* Inner private class */
	class CEffectMarkEntry {
		
	public:
		EffectMarkType markType;
		int markCount;
		CClock lifetimeClock;
		std::unordered_set<CCharacter*> initiators;
		const sf::Texture* texture;
		
		float lifetime = 20000;

	public:
		CEffectMarkEntry(CCharacter* initiator, EffectMarkType markType, int markCount = 1) :
				markType(markType),
				markCount(markCount),
				initiators(std::unordered_set<CCharacter*>({initiator})),
				texture(&CTextureManager::getTexture(getMarkTypeName(markType))) {}
		
		bool hasExpiredLifetime() const;

		struct CompareEffectMarkEntries {
			bool operator()(const CEffectMarkEntry lhs, const CEffectMarkEntry rhs) const {
				if(lhs.markCount != rhs.markCount) {
					return lhs.markCount > rhs.markCount;
				} else {
					// TODO add further mark types
					return true;
				}
			}
		};
	};
};


#endif //CYBERPUNK_CEFFECTMARK_HPP
