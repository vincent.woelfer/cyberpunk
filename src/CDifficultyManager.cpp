#include "CDifficultyManager.hpp"

CDifficulty CDifficultyManager::difficulty;
int CDifficultyManager::playerCount;
int CDifficultyManager::additionalPlayerCount;


void CDifficultyManager::setDifficulty(CDifficulty difficultyUpdate) {
	difficulty = difficultyUpdate;
}

void CDifficultyManager::setNumberOfPlayers(int numOfPlayersUpdate) {
	playerCount = numOfPlayersUpdate;
	additionalPlayerCount = numOfPlayersUpdate - 1;
}

float CDifficultyManager::linearScale(float intercept, float increase, float x) {
	return intercept + increase * x;
}

float CDifficultyManager::squaredScale(float intercept, float increase, float x) {
	return intercept + increase * x * x;
}

float CDifficultyManager::polynomScale(float intercept, float linearIncrease, float x, float squaredIncrease,
                                       float x_squared) {
	return intercept + linearIncrease * x + squaredIncrease * x_squared * x_squared;
}

float CDifficultyManager::getEnemyHealthModifier() {

	float challengeTerm = linearScale(0.f, 0.1f, (float) difficulty);
	float playerTerm = polynomScale(1.f, 0.05f, additionalPlayerCount, 0.05, additionalPlayerCount);

	return challengeTerm + playerTerm;
}

float CDifficultyManager::getSpawnBudgetModifier() {
	float challengeTerm = linearScale(0.f, 0.15f, (float) difficulty);
	float playerTerm = polynomScale(1.f, 0.1f, additionalPlayerCount, 0.05, additionalPlayerCount);

	return challengeTerm + playerTerm;
}

float CDifficultyManager::getIntensityModifier() {
	float challengeTerm = linearScale(0.f, 0.1f, (int) difficulty);
	float playerTerm = linearScale(1.f, 0.1f, additionalPlayerCount);

	return challengeTerm + playerTerm;
}
