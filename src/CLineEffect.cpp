#include "CLineEffect.hpp"
#include "CSceneManager.hpp"

CLineEffect::CLineEffect(Vector3f p1, Vector3f p2, Color lineColor, float width) :
		CObject(p1, 0),
		graphicBox(p1, Vector3f(0.02f, 1, 0.02f), lineColor), width(width) {
	updateDimensions(p1, p2);
}

CLineEffect::CLineEffect(CObject *o1, CObject *o2, Color lineColor, float width) :
		CObject(o1->getPosition(), 0),
		graphicBox(o2->getPosition(), Vector3f(0.02f, 1, 0.02f), lineColor), width(width) {

	this->o1 = o1;
	this->o2 = o2;

	updateDimensions(o1->getPosition(), o2->getPosition());
}

void CLineEffect::tick(float frameTime) {
	if(o1 != nullptr && o2 != nullptr && CSceneManager::doesObjectExist(o1) && CSceneManager::doesObjectExist(o2)) {// check if objects are set
		updateDimensions(o1->getPosition(), o2->getPosition());
	}
	if(clock.getElapsedTime().asMilliseconds() >= lifetime) {
		CSceneManager::safeRemoveObject(this);
	}
}


CCollisionContainer* CLineEffect::getCollisionContainer() {
	return nullptr;
}

CGraphicContainer* CLineEffect::getGraphicContainer() {
	return &graphicBox;
}

void CLineEffect::updateDimensions(Vector3f p1, Vector3f p2) {
	Vector3f connectVec = p2 - p1;
	Vector3f origin = p1 + connectVec * 0.5f;
	Vector2f dir = CEngine::to2D(connectVec);
	float rotation = CEngine::getRotation(dir);

	graphicBox.setPosition(origin);
	graphicBox.setRotation(rotation);
	graphicBox.setDimension(Vector3f(width, CEngine::getLength(connectVec)/2.f, width));
}

void CLineEffect::setLifetime(float lifetime) {
	this->lifetime = lifetime;
}
