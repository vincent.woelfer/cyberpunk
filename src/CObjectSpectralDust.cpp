#include "CObjectSpectralDust.hpp"
#include "CSceneManager.hpp"
#include "CEnemyGhost.hpp"
#include "CMap.hpp"
#include "CEnemyLich.hpp"

CCollisionContainer* CObjectSpectralDust::getCollisionContainer() {
    return &boundingBox;
}

CGraphicContainer *CObjectSpectralDust::getGraphicContainer() {
    return &graphicBox;
}

void CObjectSpectralDust::tick(float frameTime) {
	// Call ghosts
	if(survivalClock.getElapsedTime().asMilliseconds() >= callInterval * 500) {
		callInterval++;
		callGhosts();
	}

	// Consume ghosts
	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		CEnemyGhost* enemyGhost = dynamic_cast<CEnemyGhost*>(enemy);
		
		if(enemyGhost != nullptr) {
			if(CEngine::getDistance(getPosition(), enemyGhost->getPosition()) <= 1.2f) {
				consumedHealth += enemyGhost->getHealth();
				CSceneManager::safeRemoveObject(enemyGhost);
			}
		}
	}

	// Revive
	if(consumedHealth >= minHealth) {
		Vector3f spawnPos = getPosition();
		spawnPos.z = 1.2f;
		CEnemyLich* spectral = new CEnemyLich(spawnPos, 0);
		spectral->setHealth(consumedHealth);
		CSceneManager::safeAddObject(spectral);
		CSceneManager::safeRemoveObject(this);
	}
	
    if(survivalClock.getElapsedTime().asMilliseconds() > lifetime) {
        CSceneManager::safeRemoveObject(this);
    }
}

void CObjectSpectralDust::setLifetime(float lifetime) {
    this->lifetime = lifetime;
}

struct GhostWithRating{
	CEnemyGhost* ghost;
	float rating;

	// Lower is better
	bool operator < (const GhostWithRating& other) const {
		return (rating < other.rating);
	}
};

void CObjectSpectralDust::callGhosts() {
	vector<GhostWithRating> ghostsToCall;
	
	// Call Ghosts
	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		CEnemyGhost* enemyGhost = dynamic_cast<CEnemyGhost*>(enemy);
		
		if(enemyGhost != nullptr) {
			GhostWithRating ghostWithRating;
			
			CNavPath* p = CSceneManager::getMap()->findPath(CEngine::to2D(getPosition()), CEngine::to2D(enemyGhost->getPosition()), enemyGhost->getAgentHalfWidth());
			float distToDust = p->getLength(CEngine::to2D(getPosition()));
			
			CPlayer* player = CSceneManager::getPlayers().at(0);
			p = CSceneManager::getMap()->findPath(CEngine::to2D(player->getPosition()), CEngine::to2D(enemyGhost->getPosition()), enemyGhost->getAgentHalfWidth());
			float distToPlayer = p->getLength(CEngine::to2D(player->getPosition()));
			
			float rating = distToDust - distToPlayer * 0.8f;
			
			// Skip if rating is too bad or ghost wont reach dust anyway
			bool wouldReachDust = (enemyGhost->getHealth() > distToDust/ enemyGhost->getMovementSpeed() * 7); // 7hp per sec lost
			
			if(rating > 3 || !wouldReachDust) {
				continue;
			}

			ghostWithRating.ghost = enemyGhost;
			ghostWithRating.rating = rating;
			ghostsToCall.push_back(ghostWithRating);
		}
	}
	
	std::sort(ghostsToCall.begin(), ghostsToCall.end());
	
	ghostsToCall.resize(4);
	
	for(GhostWithRating ghostWithRating : ghostsToCall) {
		if(ghostWithRating.ghost == nullptr) {
			break;
		}
		ghostWithRating.ghost->callToDust(this);
	}
}
