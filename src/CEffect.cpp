#include "CEffect.hpp"


bool CEffect::hasExpiredLifetime() const {
	return lifetime > 0 && lifetimeClock.getTimeMs() > lifetime;
}

bool CEffect::canMergeEffect(CEffect* otherEffect) {
	return false;
}

void CEffect::setOwner(CCharacter* owner) {
	this->owner = owner;
}

std::vector<const sf::Texture*> CEffect::getVisuals() {
	return std::vector<const sf::Texture*>();
}

int CEffect::getVisualPriority() {
	return 0;
}

std::string CEffect::getId() const {
	return id;
}

long CEffect::getIdAsLong() const {
	return std::stol(id);
}

void CEffect::setId(long id) {
	this->id = std::to_string(id);
}

void CEffect::setId(std::string id) {
	this->id = id;
}

void CEffect::restartLifetime() {
	lifetimeClock.restart();
}
