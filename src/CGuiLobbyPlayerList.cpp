#include <SFML/Graphics/Text.hpp>
#include <iostream>
#include "CGuiLobbyPlayerList.hpp"
#include "CGuiManager.hpp"
#include "CColorDefines.hpp"
#include "CLobby.hpp"
#include "CNetworkManager.hpp"

CGuiLobbyPlayerList::CGuiLobbyPlayerList(CLobby* lobby) :
		lobby(lobby) {

	ownNameSize = static_cast<unsigned int>(ownNameSize * CGuiManager::getGuiFontSizeFactor());
	otherNameSize = static_cast<unsigned int>(otherNameSize * CGuiManager::getGuiFontSizeFactor());
	
	ownText = sf::Text("", CGuiManager::getFontDisplay(), ownNameSize);
	ownText.setStyle(sf::Text::Style::Bold);

}

CGuiLobbyPlayerList::~CGuiLobbyPlayerList() {
	CGuiManager::removeGuiElement(this);
}

void CGuiLobbyPlayerList::tick(float frameTime) {
	ownText.setString(lobby->getName());
	ownText.setColor(getClassColor(lobby->getClassName()));
	ownText.setOrigin(Vector2f(ownText.getLocalBounds().width, ownText.getLocalBounds().height) * 0.5f);
	ownText.setPosition(CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.175f)));
	
	otherTexts.clear();
	
	int i = 0;
	for(CConnection connection : CNetworkManager::getConnections()) {
		sf::Text text(connection.name, CGuiManager::getFontDisplay(), otherNameSize);
		text.setColor(getClassColor(connection.className));
		text.setOrigin(Vector2f(text.getLocalBounds().width, text.getLocalBounds().height) * 0.5f);
		text.setPosition(CGuiManager::getRelativeToGuiSize(Vector2f(0.5f, 0.325f + i * 0.08f)));

		if(connection.isHost && markHostConnection) {
			text.setStyle(text.getStyle() | sf::Text::Style::Underlined);
		}
		
		if(connection.isAliveClock.hasPassed(isAliveTimeout)) {
			text.setStyle(text.getStyle() | sf::Text::Style::StrikeThrough);
			text.setColor(text.getColor() - sf::Color(0, 0, 0, 50));
		} else {
			text.setStyle(text.getStyle() | sf::Text::Style::Bold);
		}

		otherTexts.push_back(text);		
		i++;
	}
}

void CGuiLobbyPlayerList::draw(sf::RenderTarget* renderTarget) {
	if(isVisible) {
		renderTarget->draw(ownText);
		
		for(sf::Text otherText : otherTexts) {
			renderTarget->draw(otherText);
		}
	}
}

sf::Color CGuiLobbyPlayerList::getClassColor(CClassName className) const {
	sf::Color color;
	
	switch (className) {
		case CClassName::MEDIATOR:
			color = COLOR_PINK;
			break;
		case CClassName::SNIPER:
			color = COLOR_DARK_GREEN;
			break;
		case CClassName::SHOTGUNNER:
			color = COLOR_ORANGE;
			break;
	}

	return color;
}

