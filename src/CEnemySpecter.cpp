#include "CEnemySpecter.hpp"
#include "CProjectileSpecterPull.hpp"
#include "CProjectileSweepingWall.hpp"
#include "CEnemyGhost.hpp"
#include "CEffectMovementSlow.hpp"


void CEnemySpecter::tick(float frameTime) {
	if(isActive) {
		if(specterAura == nullptr) {
			specterAura = new CSpecterAura(Vector3f(pos.x, pos.y, 0.55f), auraRadius, Color(40, 40, 90, 100), Color(70, 70, 150, 255));
			CSceneManager::safeAddObject(specterAura);
		}
		
		CEnemy::tick(frameTime);
		healHealth(healthRegeneration * frameTime);
		
		specterAura->updatePosition(pos);
		induceAuraEffect(frameTime);

		updateIsPulling();
	}
}

void CEnemySpecter::die() {
	CSceneManager::safeRemoveObject(specterAura);
    CEnemy::die();
}

void CEnemySpecter::induceAuraEffect(float frameTime) {
	CPlayer* player = CSceneManager::getPlayers()[0];

	for(CEnemy* enemy : CSceneManager::getEnemies()) {
		if(CEngine::getDistance(enemy->getPosition(), getPosition()) <= auraRadius) {
			if(dynamic_cast<CEnemyGhost*>(enemy) != nullptr) {
				enemy->healHealth(auraHealthRegeneration * frameTime);
			} else if(dynamic_cast<CEnemySpecter*>(enemy) != nullptr && enemy != this) { //!isBeingDevoured){
				if(devourHierarchy > dynamic_cast<CEnemySpecter*>(enemy)->devourHierarchy) {
					devourMergeTarget(dynamic_cast<CEnemySpecter*>(enemy));
				}
				else if (devourHierarchy == dynamic_cast<CEnemySpecter*>(enemy)->devourHierarchy && CEngine::getDistance(enemy->getPosition(), getPosition()) <= devourRadius && CEngine::getDistance(getPosition(), player->getPosition()) < CEngine::getDistance(enemy->getPosition(), player->getPosition())) {
					devourMergeTarget(dynamic_cast<CEnemySpecter*>(enemy));
				}
			}
		}
	}

	float distToPlayer = CEngine::getDistance(player->getPosition(), getPosition());
	if(distToPlayer <= auraRadius) {
		distToPlayer = CEngine::clamp(distToPlayer, auraMinSlowRadius, auraRadius);
		float distAlpha = CEngine::invertAlpha(CEngine::clampAlpha((distToPlayer - auraMinSlowRadius)/(auraRadius - auraMinSlowRadius)));
		float slowModifier = CEngine::lerp(auraSlowModifierMin, auraSlowModifierMax, distAlpha);
		
		CEffectMovementSlow* slowEffect = dynamic_cast<CEffectMovementSlow*>(player->getEffectManager()->getEffectWithId(slowEffectId));

		if(slowEffect != nullptr) {
			slowEffect->restartLifetime();
			slowEffect->setMovementSpeedModifier(slowModifier);
		} else {
			slowEffect = new CEffectMovementSlow(this, slowModifier, 200);
			slowEffect->setId(slowEffectId);
			player->addEffect(slowEffect);
		}
		
		player->reduceHealth(auraDamage * frameTime);
	}
}

void CEnemySpecter::devourMergeTarget(CEnemySpecter* target) {
	++devourHierarchy;

	//increase own stats
	maxHealth += target->getMaxHealth();
	healthBar.setMaxHealth(maxHealth);
	healHealth(target->getHealth());
	
	effectManager.addEffect(new CEffectMark(nullptr, CEffectMark::EffectMarkType::Lightning, target->getEffectManager()->getMarkCount(CEffectMark::EffectMarkType::Lightning))); 

	auraRadius *= 1.25f;
	auraDamage *= 1.5f;

	auraHealthRegeneration *= 1.5f;
	
	pullStunDuration *= 1.5f;
	pullDuration *= 1.5f;
	pullStrength *= 1.5;
	pullMaxRange *= 1.25f;
	pullStandingDuration *= 1.25f;
	
	float zIncrease = ((CGraphicBox*)target->getGraphicContainer())->getDimension().z;
	graphicBox.setDimension(graphicBox.getDimension() + Vector3f(0, 0, zIncrease));
	graphicBox.setPosition(graphicBox.getPosition() + Vector3f(0, 0, 2 * zIncrease));

	CSceneManager::safeRemoveObject(specterAura);
	specterAura = new CSpecterAura(Vector3f(pos.x, pos.y, 0.55f), auraRadius, Color(40, 40, 90, 100), Color(70, 70, 150, 255));
	CSceneManager::safeAddObject(specterAura);
	target->die();
}

int CEnemySpecter::getDevourHierarchy() {
	return devourHierarchy;
}

bool CEnemySpecter::isPlayerInSight(CPlayer* target) {
	float dist = CEngine::getDistance(getPosition(), target->getPosition());
	if(dist > pullMaxRange * 0.9f) {
		return false;
	}
	
	Vector3f pos = (getPosition() + target->getPosition()) * 0.5f;
	Vector3f dim = Vector3f(0.25f, dist / 2.f, 0.25f);
	Vector2f dirToTarget = CEngine::to2D(target->getPosition() - getPosition());
	float rot = CEngine::getRotation(dirToTarget);

	CBoundingBox box(pos, dim, rot);
	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC,
	                                    CCollisionChannel::Channel::PROJECTILE});

	for(CObject* colliding : CSceneManager::getIntersecting(&box, collisionChannel)) {
		if(colliding->getCollisionContainer()->getCollisionChannel()->isSet({CCollisionChannel::Channel::STATIC})) {
			return false;
		} else {
			// Other projectiles are ok, they dont block
			if(dynamic_cast<CProjectileSweepingWall*>(colliding) != nullptr) {
				return false;
			}
		}
	}
	return true;
}

void CEnemySpecter::updateIsPulling() {
	if(isPulling) {
		if(!hasFiredProjectile) {
			if(clockPullDuration.getTimeMs() >= pullTelegraphDuration) {
				hasFiredProjectile = true;
				clockPullDuration.restart();
				
				graphicBox.setColorBase(colorNormal);

				CPlayer* target = findPullTarget();

				if(target != nullptr) {
					pull(target);
				}
			} else {
				float alpha = CEngine::clamp(clockPullDuration.getTimeMs() / pullTelegraphDuration, 0.25f, 1);
				graphicBox.setColorBase(CEngine::lerp(colorNormal, colorPullTelegraph, alpha));
			}
		} else {
			float waitingDuration = hasHitPull ? pullDuration + pullStunDuration : pullStandingDuration;
			
			if(clockPullDuration.getTimeMs() >= waitingDuration) {
				isPulling = false;
				hasHitPull = false;
				clockCooldownPull.restart();
			}
		}
	} else {
		if(isPullReady() && findPullTarget() != nullptr) {
			startPullTelegraph();
		}
	}
}

void CEnemySpecter::pull(CPlayer* target) {
	if(effectManager.hasStun()) {
		return;
	}

	Vector2f targetPos = CEngine::to2D(target->getPosition());
	float targetPosOffset = CEngine::getDistance(CEngine::to2D(getPosition()), targetPos) / pullProjectileSpeed * target->getMovementSpeed();
	targetPos += CEngine::normalize(target->getMovementDirection()) * targetPosOffset;

	Vector2f dir = CEngine::normalize(targetPos - CEngine::to2D(this->getPosition()));

	Vector3f projectileStartPos = this->getPosition();
	projectileStartPos.z = 1.3f;
	CProjectileSpecterPull* p = new CProjectileSpecterPull(this, projectileStartPos, dir, this,
	                                                       pullProjectileSpeed, pullMaxRange, pullStunDuration, pullDuration, pullStrength);
	p->setRotation(CEngine::getRotation(dir));
	p->move(dir * 0.75f);
	CSceneManager::safeAddObject(p);
}

void CEnemySpecter::notifyPullProjectileHit() {
	clockPullDuration.restart();
	hasHitPull = true;
}

void CEnemySpecter::startPullTelegraph() {
	if(effectManager.hasStun()) {
		return;
	}
	
	clockPullDuration.restart();
	isPulling = true;	
	hasFiredProjectile = false;
}

bool CEnemySpecter::isPullReady() {
	return !isPulling && clockCooldownPull.getTimeMs() >= cooldownPull && !effectManager.hasStun();
}

CPlayer* CEnemySpecter::findPullTarget() {
	CPlayer* target = nullptr;

	for(CPlayer* player : CSceneManager::getPlayers()) {
		if(isPlayerInSight(player)) {
			target = player;
			break;
		}
	}

	return target;
}

bool CEnemySpecter::getIsPulling() const {
	return isPulling;
}

