#include "CCollisionContainer.hpp"
#include "CEngine.hpp"
#include "CCollisionChannel.hpp"

void CCollisionContainer::setPosition(Vector3f pos) {
	this->pos = pos;
}

void CCollisionContainer::setRotation(float rot) {
	this->rot = CEngine::clamp(rot, 0, 360);
}

void CCollisionContainer::move(Vector3f movement) {
	pos += movement;
}

void CCollisionContainer::move(Vector2f movement) {
	pos += CEngine::toPlanar3D(movement);
}

void CCollisionContainer::rotate(float angle) {
	rot = CEngine::rotate(rot, angle);
}

Vector3f CCollisionContainer::getPosition() const {
	return pos;
}

float CCollisionContainer::getRotation() const {
	return rot;
}

CCollisionChannel* CCollisionContainer::getCollisionChannel() {
	return &collisionChannel;
}

void CCollisionContainer::setCollisionChannel(CCollisionChannel collisionChannel) {
	this->collisionChannel = collisionChannel;
}

bool CCollisionContainer::matchCollisionChannel(CCollisionChannel channel) const {
	return collisionChannel.match(channel);
}
