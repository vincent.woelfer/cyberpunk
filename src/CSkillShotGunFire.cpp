#include "CSkillShotGunFire.hpp"
#include "CInputManager.hpp"
#include "CSceneManager.hpp"
#include "CGui.hpp"
#include "CRpcHandler.hpp"
#include "CObjectProjectileHitPopUpBox.hpp"
#include "CEffectVisible.hpp"
#include "CEffectForcePush.hpp"
#include "CEffectMovementSlow.hpp"
#include "CDebugHelper.hpp"
#include "CLineEffect.hpp"
#include "CClassShotgunner.hpp"

void CSkillShotGunFire::onButtonPress() {
	Vector2f dir = CInputManager::getDirToMouseInScene(owner->getPosition());
	fire(dir);
	
	remainingBullets--;

	if(remainingBullets <= 0) {
		reload();
	}
}

void CSkillShotGunFire::tick(float frameTime) {
	if(isReloading && clockCooldown.hasPassed(reloadCooldown)) {
		stopReloading(false);
	}
}

void CSkillShotGunFire::quickDraw() {
	if(isReloading) {
		stopReloading(true);

		CRpcHandler::sendPlayerQuickDraw(owner);
	}
}

void CSkillShotGunFire::quickDrawRPC() {
	stopReloading(true);
}

void CSkillShotGunFire::reload() {
	if(cooldown != reloadCooldown) {
		isReloading = true;
		remainingBullets = maxBullets;
		cooldown = reloadCooldown;
		clockCooldown.restart();
		
		Color col = owner->getGraphicContainer()->getColorBase();
		col.a = 130;
		owner->getGraphicContainer()->setColorBase(col);

		if(owner->getIsLocal()) {
			CGui::updateLocalPlayerReloadStatus(CReloadStatus::RELOADING);
		}

		CRpcHandler::sendPlayerReload(owner);
	}
}

void CSkillShotGunFire::reloadRPC(float gameTime) {
	isReloading = true;
	remainingBullets = maxBullets;
	cooldown = reloadCooldown;
	clockCooldown.restartAtGameTime(gameTime);
	
	Color col = owner->getGraphicContainer()->getColorBase();
	col.a = 130;
	owner->getGraphicContainer()->setColorBase(col);
}

void CSkillShotGunFire::fire(Vector2f dir) {
	std::vector<CSkillShotGunFireRaycastResult> raycastResults = performRaycast(dir);
	std::vector<CSkillShotGunFireHitResult> hitResults = getHitResults(raycastResults); 
	
	hitEnemies(hitResults);
	spawnFirePopupBox(dir);
	spawnPopupBoxesLocal(raycastResults);
	
	CRpcHandler::sendPlayerShotgunnerPrimaryFire(owner, dir, hitResults, getPopupBoxesNetwork(raycastResults));
}

void CSkillShotGunFire::fireRPC(Vector2f dir, std::vector<CSkillShotGunFireHitResult> hitResults, std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxes) {
	convertIdsToPointers(hitResults);
	hitEnemies(hitResults);
	
	spawnFirePopupBox(dir);
	spawnFireLinesNetwork(dir);
	spawnPopupBoxesNetwork(popupBoxes);	
}

void CSkillShotGunFire::stopReloading(bool wasQuickdraw) {
	isReloading = false;
	cooldown = fireCooldown;
	owner->getGraphicContainer()->setColorBase(owner->getCharacterClass()->getBaseColor());

	if(owner->getIsLocal()) {
		if(wasQuickdraw) {
			CGui::updateLocalPlayerReloadStatus(CReloadStatus::QUICKDRAW);
		} else {
			CGui::updateLocalPlayerReloadStatus(CReloadStatus::NONE);
		}
	}
}

std::vector<CSkillShotGunFireRaycastResult> CSkillShotGunFire::performRaycast(Vector2f dir) {
	std::vector<CSkillShotGunFireRaycastResult> results;

	float angleOffset = bulletSpreadAngle / (bulletCountPerShot - 1);
	float angleStart = CEngine::getRotation(dir) - bulletSpreadAngle/2.f;
	
	Vector3f lateralOffset = CEngine::toPlanar3D(CEngine::rotateZ(dir, 90) * firePositionLateralOffsetRange);
	Vector3f firePosition = owner->getPosition() + CEngine::toPlanar3D(dir) * firePositionOffsetLength - lateralOffset/2.f;

	CCollisionChannel collisionChannel({CCollisionChannel::Channel::STATIC, CCollisionChannel::Channel::CHARACTER});
	
	for(int i = 0; i < bulletCountPerShot; i++) {
		float angle = angleStart + (i * angleOffset);
		Vector2f actualDir = CEngine::getUnitInDirection(angle);
		Vector3f actualFirePosition = firePosition + lateralOffset * ((float)i / bulletCountPerShot);

		Vector3f fireLineEndPos = actualFirePosition + CEngine::toPlanar3D(actualDir) * (bulletRange - 0.2f);
		
		CHitResult hit = CSceneManager::raycast(actualFirePosition, actualDir, collisionChannel);

		if(hit.target != nullptr && hit.hitDistance <= bulletRange) {
			CSkillShotGunFireRaycastResult raycastResult;
			
			if(dynamic_cast<CCharacter*>(hit.target) != nullptr) {
				raycastResult.isCharacter = true;
				raycastResult.target = dynamic_cast<CCharacter*>(hit.target);
				raycastResult.dir = actualDir;
				raycastResult.hitPosition = hit.hitPos;
			} else {
				raycastResult.isCharacter = false;
				raycastResult.hitPosition = hit.hitPos;
			}
			
			fireLineEndPos = raycastResult.hitPosition;

			results.push_back(raycastResult);
		}

		spawnFireLine(actualFirePosition, fireLineEndPos);				
	}

	return results;
}

std::vector<CSkillShotGunFirePopupBoxNetwork> CSkillShotGunFire::getPopupBoxesNetwork(std::vector<CSkillShotGunFireRaycastResult> raycastResults) {
	std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxes;
	
	for(CSkillShotGunFireRaycastResult raycastResult : raycastResults) {
		CSkillShotGunFirePopupBoxNetwork popupBox;
		if(raycastResult.isCharacter) {
			popupBox.target = raycastResult.target->getId();
			popupBox.relativePos = raycastResult.hitPosition - raycastResult.target->getPosition();
			popupBoxes.push_back(popupBox);
		}
	}

	return popupBoxes;
}

std::vector<CSkillShotGunFireHitResult> CSkillShotGunFire::getHitResults(std::vector<CSkillShotGunFireRaycastResult> raycastResults) {
	std::vector<CSkillShotGunFireHitResult> hitResults;

	for(CSkillShotGunFireRaycastResult raycastResult : raycastResults) {
		if(raycastResult.isCharacter) {
			bool foundExistingResult = false;
			
			for(CSkillShotGunFireHitResult& existingHitResult : hitResults) {
				if(existingHitResult.target == raycastResult.target) {
					foundExistingResult = true;
					existingHitResult.bulletCount++;
					existingHitResult.dir += raycastResult.dir;

					break;					
				}
			}

			if(!foundExistingResult) {
				CSkillShotGunFireHitResult newHitResult;
				newHitResult.dir = raycastResult.dir;
				newHitResult.bulletCount = 1;
				newHitResult.target = raycastResult.target;
				newHitResult.targetId = raycastResult.target->getId();
				hitResults.push_back(newHitResult);
			}
		}
	}
	
	for(CSkillShotGunFireHitResult& hitResult : hitResults) {
		hitResult.dir = CEngine::normalize(hitResult.dir);
	}

	return hitResults;
}

void CSkillShotGunFire::hitEnemies(std::vector<CSkillShotGunFireHitResult> hitResults) {
	for(CSkillShotGunFireHitResult hitResult : hitResults) {
		float damage = damagePerBullet * hitResult.bulletCount;
		float pushStrength = pushStrengthBasis + pushStrengthPerAdditionalBullet * (hitResult.bulletCount - 1);
		float pushDuration = pushDurationBasis + pushDurationPerAdditionalBullet * (hitResult.bulletCount - 1);
		
		hitResult.target->reduceHealth(damage);

		if(hitResult.target->getHealth() <= 0) {
			hitResult.target->push(hitResult.dir * 0.75f);
			dynamic_cast<CClassShotgunner*>(owner->getCharacterClass())->spawnGem(hitResult.target->getPosition());
		}
		CPlayer::checkKillPotential(hitResult.target, owner);

		hitResult.target->addEffect(new CEffectForcePush(nullptr, pushStrength, hitResult.dir, pushDuration));
		hitResult.target->addEffect(new CEffectMovementSlow(nullptr, 0.15f, 700));

		hitResult.target->addEffect(new CEffectVisible(nullptr, 100, 500));
	}
}

void CSkillShotGunFire::spawnPopupBoxesLocal(std::vector<CSkillShotGunFireRaycastResult> raycastResults) {
	for(CSkillShotGunFireRaycastResult raycastResult : raycastResults) {
		spawnPopupBox(raycastResult.hitPosition);
	}
}

void CSkillShotGunFire::spawnPopupBox(Vector3f pos) const {
	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(pos, Vector3f(0.1f, 0.1f, 0.1f), COLOR_ORANGE - Color(0, 0, 0, 50), 35));
}

void CSkillShotGunFire::convertIdsToPointers(std::vector<CSkillShotGunFireHitResult>& hitResults) {
	for(auto iter = hitResults.begin(); iter != hitResults.end(); iter++) {
		(*iter).target = dynamic_cast<CCharacter*>(CSceneManager::getObject((*iter).targetId));
		if((*iter).target == nullptr) {
			hitResults.erase(iter);
			iter--;
		}
	}
}

void CSkillShotGunFire::spawnFirePopupBox(Vector2f dir) const {
	Vector3f firePosition = owner->getPosition() + CEngine::toPlanar3D(dir) * firePositionOffsetLength; 
	CSceneManager::safeAddObject(new CObjectProjectileHitPopUpBox(firePosition, Vector3f(0.2f, 0.2f, 0.2f), COLOR_ORANGE - Color(0, 0, 0, 50), 50));
}

void CSkillShotGunFire::spawnFireLine(Vector3f startPos, Vector3f endPos) const {
	CLineEffect* lineEffect = new CLineEffect(startPos, endPos, Color(150, 30, 30, 60), 0.12f);
	lineEffect->setLifetime(70);
	CSceneManager::safeAddObject(lineEffect);
}

void CSkillShotGunFire::spawnFireLinesNetwork(Vector2f dir) {
	float angleOffset = bulletSpreadAngle / (bulletCountPerShot - 1);
	float angleStart = CEngine::getRotation(dir) - bulletSpreadAngle / 2.f;

	Vector3f lateralOffset = CEngine::toPlanar3D(CEngine::rotateZ(dir, 90) * firePositionLateralOffsetRange);
	Vector3f firePosition =
			owner->getPosition() + CEngine::toPlanar3D(dir) * firePositionOffsetLength - lateralOffset / 2.f;

	for(int i = 0; i < bulletCountPerShot; i++) {
		float angle = angleStart + (i * angleOffset);
		Vector2f actualDir = CEngine::getUnitInDirection(angle);
		Vector3f actualFirePosition = firePosition + lateralOffset * ((float) i / bulletCountPerShot);

		Vector3f fireLineEndPos = actualFirePosition + CEngine::toPlanar3D(actualDir) * (bulletRange - 0.2f);

		CHitResult hit = CSceneManager::raycast(actualFirePosition, actualDir, CCollisionChannel::Channel::STATIC);
		if(hit.target != nullptr && hit.hitDistance <= bulletRange) {
			fireLineEndPos = hit.hitPos;
		}

		spawnFireLine(actualFirePosition, fireLineEndPos);
	}
}

void CSkillShotGunFire::spawnPopupBoxesNetwork(std::vector<CSkillShotGunFirePopupBoxNetwork> popupBoxesNetwork) {
	for(CSkillShotGunFirePopupBoxNetwork popupBoxNetwork : popupBoxesNetwork) {
		auto* target = dynamic_cast<CCharacter*>(CSceneManager::getObject(popupBoxNetwork.target));

		if(target != nullptr) {
			spawnPopupBox(target->getPosition() + popupBoxNetwork.relativePos);
		}
	}
}


