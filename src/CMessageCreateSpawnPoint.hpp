#ifndef CYBERPUNK_CMESSAGECREATESPAWNPOINT_HPP
#define CYBERPUNK_CMESSAGECREATESPAWNPOINT_HPP

#include <SFML/System.hpp>
#include "CMessage.hpp"
#include "CObjectIdManager.hpp"

class CMessageCreateSpawnPoint : public CMessage {
public:
	CObjectId objectId;
	sf::Vector2f pos;

	CMessageCreateSpawnPoint() :
		CMessage(CMessageType::CREATE_SPAWN_POINT) {}

	void serializeInto(sf::Packet& packet) override;
	void deserializeFrom(sf::Packet& packet) override;
	void process() override;

};


#endif //CYBERPUNK_CMESSAGECREATESPAWNPOINT_HPP
